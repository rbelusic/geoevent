package org.fm.app.geoevent.dm.event;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "messages")
public interface DmMessage extends DmObject {

    public String getId(String def);

    public void setId(String def);

    public String getUserId(String def);

    public void setUserId(String def);

    public String getTitle(String def);

    public void setTitle(String def);

    @Column(name = "message")
    public String getMessageBody(String def);

    @Column(name = "message")
    public void setMessageBody(String def);

    @Column(name = "context_type")
    public String getContextType(String def);

    @Column(name = "context_type")
    public void setContextType(String def);

    @Column(name = "tstamp")
    public Date getTimestamp(Date def);

    @Column(name = "tstamp")
    public void setTimestamp(Date def);

    @Transient
    public DmUser getUser(DmUser def);

    @Transient
    public void setUser(DmUser def);

}
