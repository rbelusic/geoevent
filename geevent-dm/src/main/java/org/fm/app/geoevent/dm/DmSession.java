package org.fm.app.geoevent.dm;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "sessions")
public interface DmSession extends DmObject {
    public String getId(String def);
    public void setId(String def);

    public String getUserId(String def);
    public void setUserId(String def);

    @Transient
    public DmUser getUser(DmUser def);
    
    @Transient
    public void setUser(DmUser def);

    
    public String getIp(String def);
    public void setIp(String def);

    @Column(name = "tstamp")
    public Date getTimestamp(Date def);
    
    @Column(name = "tstamp")
    public void setTimestamp(Date def);

    public Date getClosedAt(Date def);
    public void setClosedAt(Date def);
}
