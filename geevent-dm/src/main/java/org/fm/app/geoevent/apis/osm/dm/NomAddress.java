package org.fm.app.geoevent.apis.osm.dm;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import org.fm.dm.DmObject;


public interface NomAddress extends DmObject {
    @JsonProperty ("house_number")    
    public String getHouseNumber(String def);
    
    @JsonProperty ("house_number")    
    public void setHouseNumber(String def);    
    
    public String getRoad(String def);
    public void setRoad(String def);    
    
    public String getSuburb(String def);
    public void setSuburb(String def);    
    
    public String getHamlet(String def);
    public void setHamlet(String def);    
    
    public String getVillage(String def);
    public void setVillage(String def);    
    
    public String getCity(String def);
    public void setCity(String def);    
    
    public String getTown(String def);
    public void setTown(String def);    
    
    @JsonProperty("state_district")
    public String getStateDistrict(String def);
    
    @JsonProperty("state_district")    
    public void setStateDistrict(String def);    
    
    public String getState(String def);
    public void setState(String def);    
    
    public String getPostcode(String def);
    public void setPostcode(String def);    
    
    public String getCounty(String def);
    public void setCounty(String def);    
    
    public String getCountry(String def);
    public void setCountry(String def);    
    
    public String getLocality(String def);
    public void setLocality(String def);    
    
    @JsonProperty("country_code")
    public String getCountryCode(String def);
    
    @JsonProperty("country_code")    
    public void setCountryCode(String def);  
}
