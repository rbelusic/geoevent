package org.fm.app.geoevent.apis.osm.dm;


public interface OsmNode extends OsmElement {
    public Double getLat(Double def);
    public void setLat(Double def);
    
    public Double getLon(Double def);
    public void setLon(Double def);
}
