package org.fm.app.geoevent.apis.osm.dm;

import java.util.List;
import org.fm.dm.DmObject;

public interface NomPolygon extends DmObject {
    public String getType(String def);
    public void setType(String def);    
    
    public List<List<List<Double>>> getPoints(List<List<List<Double>>> def);
    public void setPoints(List<List<List<Double>>> def);    
}
