package org.fm.app.geoevent.apis.osm.dm;

import java.util.List;

public interface OsmWay extends OsmElement {   
    public List<OsmNode> getNodes(List<OsmNode> def);
    public void setNodes(List<OsmNode> def);    
    
    public Boolean isClosed(Boolean def);
    public Boolean setClosed(Boolean def);
    
    public Boolean isArea(Boolean def);
    public Boolean setArea(Boolean def);
}
