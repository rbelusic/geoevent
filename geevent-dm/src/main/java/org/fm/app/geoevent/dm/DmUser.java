package org.fm.app.geoevent.dm;

import java.util.List;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "users")
public interface DmUser extends DmObject {
    public String getId(String def);
    public void setId(String def);

    public String getTitle(String def);
    public void setTitle(String def);

    public String getDescription(String def);
    public void setDescription(String def);

    public String getPassword(String def);
    public void setPassword(String def);
    
    @Transient
    public List<DmRole> getRoles(List<DmRole> def);
    
    @Transient
    public void setRoles(List<DmRole> def);
}
