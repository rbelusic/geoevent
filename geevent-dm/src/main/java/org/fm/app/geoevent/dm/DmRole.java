package org.fm.app.geoevent.dm;

import javax.persistence.Table;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "roles")
public interface DmRole extends DmObject {
    public String getId(String def);
    public void setId(String def);

    public String getDescription(String def);
    public void setDescription(String def);    
}
