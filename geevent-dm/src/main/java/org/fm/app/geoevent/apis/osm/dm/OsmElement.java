package org.fm.app.geoevent.apis.osm.dm;

import java.util.Date;
import java.util.List;
import org.fm.dm.DmObject;

public interface OsmElement extends DmObject {
    public Long getOsmId(Long def);
    public void setOsmId(Long def);    
    
    public Long getUid(Long def);
    public void setUid(Long def);    
    
    public String getUser(String def);
    public void setUser(String def);    
    
    public Date getTimestamp(Date def);
    public void setTimestamp(Date def);    
    
    public Boolean IsVisible(Boolean def);
    public void setVisible(Boolean def);    

    public Integer getVersion(Integer def);
    public void setVersion(Integer def);    
    
    public Integer getChangeset(Integer def);
    public void setChangeset(Integer def);        
    
    public List<OsmTag> getTags(List<OsmTag> def);
    public void setTags(List<OsmTag> def);    
    
    public String getElementType(String def);
    public String setElementType(String def);
}
