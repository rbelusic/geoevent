package org.fm.app.geoevent.dm.event;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmAttributeDef;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "locations")
public interface DmLocation extends DmObject {

    public String getId(String def);

    public void setId(String def);

    public String getUserId(String def);

    public void setUserId(String def);

    public Double getLongitude(Double def);

    public void setLongitude(Double def);

    public Double getLatitude(Double def);
    
    public void setLatitude(Double def);

    public Double getZpos(Double def);

    public void setZpos(Double def);
    
    @DmAttributeDef(getter = "org.fm.app.geoevent.dm.DmUtil.getGeindex")
    public String getGeindex(String def);

    public void setGeindex(String def);
    
   
    @Column(name = "tstamp")
    public Date getTimestamp(Date def);

    @Column(name = "tstamp")
    public void setTimestamp(Date def);

    @Transient
    public DmUser getUser(DmUser def);

    @Transient
    public void setUser(DmUser def);

    @Transient
    @DmAttributeDef(getter = "org.fm.app.geoevent.dm.DmUtil.getDistanceTo")
    public Double getDistanceTo(DmLocation dmTo);    

    @Transient
    @DmAttributeDef(getter = "org.fm.app.geoevent.dm.DmUtil.getDistancesTo")
    public List<Double> getDistancesTo(List<DmLocation> dmTo);    
}
