package org.fm.app.geoevent.apis.osm.dm;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Map;
import org.fm.dm.DmObject;


public interface NomPlace extends DmObject {
    @JsonProperty ("place_id")
    public Long getPlaceId(Long def);
    
    @JsonProperty ("place_id")
    public void setPlaceId(Long def);    
    
    public Double getLat(Double def);
    
    public void setLat(Double def);
    
    public Double getLon(Double def);
    
    public void setLon(Double def);
    
    @JsonProperty ("display_name")
    public String getDisplayName(String def);
    
    @JsonProperty ("display_name")
    public void setDisplayName(String def);    
    
    public String getClass(String def);
    
    public void setClass(String def);    
    
    public String getType(String def);
    
    public void setType(String def);    

    public Double getImportance(Double def);
    
    public void setImportance(Double def);
    
    @JsonProperty ("boundingbox")
    public List<String> getBoundingBox(List<String> def);
    
    @JsonProperty ("boundingbox")
    public void setBoundingBox(List<String> def);
    
    public NomAddress getAddress(NomAddress def);    
    public void setAddress(NomAddress def);
    
    public GeoJsonElement getGeojson(GeoJsonElement def);    
    public void setGeojson(GeoJsonElement def);
    
    
    public String getLicence(String def);
    public void setLicence(String def);    
    
    @JsonProperty ("osm_type")
    public String getOsmType(String def);
    
    @JsonProperty ("osm_type")
    public void setOsmType(String def);    
    
    @JsonProperty ("osm_id")
    public Long getOsmId(Long def);
    
    @JsonProperty ("osm_id")
    public void setOsmId(Long def);    
    
    public Map<String, Object> getExtratags(Map<String, Object> def);
    public void setExtratags(Map<String, Object> def);
    
    public Map<String, Object> getNamedetails(Map<String, String> def);
    public void setNamedetails(Map<String, String> def);
    
    @JsonProperty("icon")
    public String getIconUrl(String def);
    @JsonProperty("icon")
    public void setIconUrl(String def);
    
    
    
}
