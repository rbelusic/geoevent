package org.fm.app.geoevent.util.rs;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import org.fm.application.FmContext;
import org.fm.dm.DmObject;

public class GeObjectMapper extends ObjectMapper {    
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Class defaultDeserializationClass;
    
    public GeObjectMapper() {
        super();
        setDateFormat(DATE_FORMAT);
        enable(SerializationFeature.INDENT_OUTPUT);
        
        DmSerializer ser = new DmSerializer(this);
        DmDeserializer deser = new DmDeserializer(this);
        SimpleModule module = new SimpleModule(getClass().getName(), new Version(1, 0, 0, "", "", ""));
        for (Class<DmObject> classType : FmContext.getDmClassTypes()) {
            module.addSerializer(classType, ser);
            module.addDeserializer(classType, deser);
        }   

        module.addDeserializer(Object.class, deser);
        registerModule(module);
    }
    
    public <T extends Object> List<T> readListOfValues(
            String content, Class<T> valueType
    ) throws IOException, JsonParseException, JsonMappingException {
        setDefaultDeserializationClass(valueType);
        return super.readValue(content, List.class);        
    }

    public <T extends Object> T readValue(
            String content, Class<T> valueType
    ) throws IOException, JsonParseException, JsonMappingException {
        setDefaultDeserializationClass(valueType);        
        return super.readValue(content, valueType);        
    }

    public Class getDefaultDeserializationClass() {
        return defaultDeserializationClass == null ? Map.class : defaultDeserializationClass;
    }

    public void setDefaultDeserializationClass(Class defaultDeserializationClass) {
        this.defaultDeserializationClass = defaultDeserializationClass;
    }
}
