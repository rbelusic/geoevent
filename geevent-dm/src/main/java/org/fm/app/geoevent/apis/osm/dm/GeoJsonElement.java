package org.fm.app.geoevent.apis.osm.dm;

import java.util.List;
import org.fm.dm.DmObject;

public interface GeoJsonElement extends DmObject {
    public String getType(String def);
    public void setType(String def);
    
    public List<Object> getCoordinates(List<Object> def);
    public void setCoordinates(List<Object> def);    
    
}
