package org.fm.app.geoevent.dm;

import javax.persistence.Table;
import org.fm.dm.DmObject;

@Table(name = "user_roles")
public interface DmUserRole extends DmObject {
    public String getUserId(String def);
    public void setUserId(String def);

    public String getRole(String def);
    public void setRole(String def);    
}
