package org.fm.app.geoevent.util;

public class GeoUtil {

    public static String geoGenerateTileIndexValue(Double value, Double min, Double max, int nl) {
        String sv = "";
        for (int i = 0; i < nl; i++) {
            double cen = min + (max - min) / 2.0;
            boolean pos = value >= cen;

            sv += pos ? "1" : "0";
            min = pos ? cen : min;
            max = pos ? max : cen;
        }
        return sv;
    }

    public static String geoGenerateTileIndex(Double x, Double y, int nl) {
        while (x > 180) {
            x -= 90;
        }
        while (x < -180) {
            x += 90;
        }

        String tileXstr = geoGenerateTileIndexValue(x, -180.0, 180.0, nl);
        String[] tileX = tileXstr.split("(?!^)");

        String tileYstr = geoGenerateTileIndexValue(y, -90.0, 90.0, nl);
        String[] tileY = tileYstr.split("(?!^)");

        String qkey = "";
        for (int i = 0; i < nl; i++) {
            qkey += tileX[i] + ("1".equals(tileY[i]) ? "0" : "1");
        }

        return (qkey);
    }

    public static String geoGenerateTileRange(String t1, String t2, int nl) {
        String trange = "";
        int maxi = t1.length() < t2.length() ? t1.length() : t2.length();
        maxi = maxi < nl * 2 ? maxi : nl * 2;

        String[] t1arr = t1.split("(?!^)");
        String[] t2arr = t2.split("(?!^)");

        for (int i = 0; i < maxi; i++) {
            if (!t1arr[i].equals(t2arr[i])) { // first dif, return
                return trange;
            } else {
                trange += t1arr[i];
            }
        }
        return trange;
    }

    public static String stripTileIndex(String oi) {
        String[] oiarr = oi.split("(?!^)");
        for (int i = oiarr.length - 1; i >= 0; i--) {
            if (!"0".equals(oiarr[i])) {
                return oi.substring(0, i + 1);
            }
        }

        return "11";
    }

    /*
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     * 
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     * @returns Distance in Meters
     */
    public static double distanceHaversine(
            double lat1, double lon1, double el1,
            double lat2, double lon2, double el2) {

        final int R = 6371; // Radius of the earth

        Double latDistance = Math.toRadians(lat2 - lat1);
        Double lonDistance = Math.toRadians(lon2 - lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
}
