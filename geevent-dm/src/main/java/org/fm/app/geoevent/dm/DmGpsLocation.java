package org.fm.app.geoevent.dm;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.xml.ws.FaultAction;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@Table(name = "gps_locations")
@DmDataIdAttribute(name = "id")
public interface DmGpsLocation extends DmObject {
    public String getId(String def);
    public void  setId(String def);
    
    public Double getAccuracy(Double def);
    public void  setAccuracy(Double def);
    
    public Double getAltitude(Double def);
    public void  setAltitude(Double def);
    
    public Double getBearing(Double def);
    public void  setBearing(Double def);

    public Double getSpeed(Double def); // m/s
    public void  setSpeed(Double def);

    public String getActivity(String def); 
    public void  setActivity(String def);

    public Double getLatitude(Double def);
    public void  setLatitude(Double def);

    public Double getLongitude(Double def);
    public void  setLongitude(Double def);

    public String getProvider(String def);
    public void  setProvider(String def);

    @Column(name = "received_at")
    public Date getReceivedAt(Date def);
    @Column(name = "received_at")
    public void  setReceivedAt(Date def);    
    
    @Column(name = "tstamp")
    public Date getTimestamp(Date def);
    @Column(name = "tstamp")
    public void  setTimestamp(Date def);    

}
