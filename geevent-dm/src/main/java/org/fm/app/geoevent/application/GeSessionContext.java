package org.fm.app.geoevent.application;

import java.util.UUID;
import java.util.logging.Logger;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.DmSysInfo;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmFactory;

/**
 *
 * @author RobertoB
 */
public class GeSessionContext  extends FmSessionContext {
    private static Logger L = FmContext.getLogger(GeSessionContext.class);
    private DmSession sessionInfo = null;
    
    /**
     * @return the sessionInfo
     */
    public DmSession getSessionInfo() {
        return sessionInfo;
    }

    /**
     * @param sessionInfo the sessionInfo to set
     */
    public void setSessionInfo(DmSession ses) {
        this.sessionInfo = ses;
    }

    public static String generateRequestToken() {
        return (UUID.randomUUID().toString());
    }

    public boolean isSessionAuthenticated() {
        return (sessionInfo == null);
    }

    public String getUserId() {
        return getSessionInfo() == null ? "" : getSessionInfo().getUserId("");
    }

    public static DmSysInfo getBuildInfo() {
        FmApplication app = FmApplication.instance();
        DmSysInfo info = DmFactory.create(DmSysInfo.class);
        if (app == null) {
            return info;
        }

        info.setName(app.getBuildProperty("build.project.name"));
        info.setVersion(app.getBuildProperty("build.project.version"));
        info.setBuildTime(app.getBuildProperty("build.project.time"));

        return (info);
    }
    
}
