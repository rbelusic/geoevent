package org.fm.app.geoevent.dm.event;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
@Table(name = "events")
public interface DmEvent extends DmObject {
    public static final int VISIBILITY_USER = 0;
    public static final int VISIBILITY_PUBLIC = 1;
    
    public String getId(String def);

    public void setId(String def);

    public String getUserId(String def);

    public void setUserId(String def);

    public String getLocationId(String def);

    public void setLocationId(String def);

    public String getMessageId(String def);

    public void setMessageId(String def);

    public Integer getVisibility(Integer def);

    public void setVisibility(Integer def);

    @Column(name = "row_number")
    public Integer getRowNumber(Integer def);

    @Column(name = "row_number")
    public void setRowNumber(Integer def);
    
    @Column(name = "tstamp")
    public Date getTimestamp(Date def);
    
    @Column(name = "tstamp")
    public void setTimestamp(Date def);

    // --
    @Transient
    public DmUser getUser(DmUser def);

    @Transient
    public void setUser(DmUser def);

    @Transient
    public DmLocation getLocation(DmLocation def);

    @Transient
    public void setLocation(DmLocation def);

    @Transient
    public DmMessage getMessage(DmMessage def);

    @Transient
    public void setMessage(DmMessage def);
    
    // --    
    public Double getDistance(Double def);
    
    @Transient
    public void setDistance(Double def);
    
}
