package org.fm.app.geoevent.apis.osm.dm;

import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */

public interface OsmUser extends OsmElement {
    public String getDisplayName(String def);
    
    public void setDisplayName(String def);    
    
    public Date getAccountCreated(Date def);
    
    public void setAccountCreated(Date def);    
    
    public String getProfilePicture(Date def);
    
    public void setProfilePicture(Date def);    
    
    public List<String> getLanguages(List<String> def);
    
    public void setLanguages(List<String> def);        
}
