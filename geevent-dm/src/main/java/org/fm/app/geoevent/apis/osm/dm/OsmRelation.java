package org.fm.app.geoevent.apis.osm.dm;

import java.util.List;

public interface OsmRelation extends OsmElement {
    public List<OsmElement> getElements(List<OsmElement> def);    
    public void setElements(List<OsmElement> def);
}
