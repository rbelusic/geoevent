/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fm.app.geoevent.dm;

import java.util.ArrayList;
import java.util.List;
import org.fm.app.geoevent.dm.event.DmLocation;
import org.fm.app.geoevent.util.GeoUtil;
import org.fm.dm.DmFactory;

/**
 *
 * @author user
 */
public class DmUtil {

    // -- DmLocation -----------------------------------------------------------
    public final static int GEINDEX_LEVELS = 32;

    public static String getGeindex(DmLocation dm, String dmdef) {
        if (dm.getAttr("geindex", "").isEmpty()) {
            dm.setAttr(
                    "geindex",
                    GeoUtil.geoGenerateTileIndex(dm.getLongitude(0D), dm.getLatitude(0D), GEINDEX_LEVELS),
                    false
            );
        }

        return dm.getAttr("geindex", "");
    }

    public static Double getDistanceTo(DmLocation dm, DmLocation dmTo) {
        return dmTo == null ? 0D
                : GeoUtil.distanceHaversine(
                        dm.getLatitude(0D), dm.getLongitude(0D), dm.getZpos(0D),
                        dmTo.getLatitude(0D), dmTo.getLongitude(0D), dmTo.getZpos(0D)
                );
    }

    public static List<Double> getDistancesTo(DmLocation dm, List<DmLocation> dmToList) {
        List<Double> distances = new ArrayList<Double>();
        if (dmToList == null) {
            return null;
        }

        for (DmLocation dmTo : dmToList) {
            distances.add(
                    GeoUtil.distanceHaversine(
                        dm.getLatitude(0D), dm.getLongitude(0D), dm.getZpos(0D),
                        dmTo.getLatitude(0D), dmTo.getLongitude(0D), dmTo.getZpos(0D)
                    )
            );
        }

        return distances;
    }

    public static DmLocation getPointOnDistance(DmLocation dm, Double dist, Double brng) {
        dist = dist / 6371.;
        brng = Math.toRadians(brng);

        Double lat1 = Math.toRadians(dm.getLatitude(0D));
        Double lon1 = Math.toRadians(dm.getLongitude(0D));

        Double lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist)
                + Math.cos(lat1) * Math.sin(dist) * Math.cos(brng));

        Double lon2 = lon1 + Math.atan2(Math.sin(brng) * Math.sin(dist)
                * Math.cos(lat1),
                Math.cos(dist) - Math.sin(lat1)
                * Math.sin(lat2));

        if (lat2.isNaN() || lon2.isNaN()) {
            return null;
        }

        DmLocation dmRet = DmFactory.create(DmLocation.class);
        dmRet.setLatitude(Math.toDegrees(lat2));
        dmRet.setLongitude(Math.toDegrees(lon2));

        return dmRet;
    }
}
