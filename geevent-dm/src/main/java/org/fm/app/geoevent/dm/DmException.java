package org.fm.app.geoevent.dm;

import java.util.Date;
import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "id")
public interface DmException extends DmObject {
    public String getId(String def);
    public void setId(String def);

    public String getDescription(String def);
    public void setDescription(String def);    
    
    public Date getTimestamp(Date def);
    public void setTimestamp(Date def);    
    
    public String getRequestPath(String def);
    public void setRequestPath(String def);   
    
    public DmSysInfo getBuildInfo(DmSysInfo def);
    public void setBuildInfo(DmSysInfo def);
}
