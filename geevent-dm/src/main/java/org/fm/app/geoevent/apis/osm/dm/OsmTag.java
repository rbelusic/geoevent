package org.fm.app.geoevent.apis.osm.dm;

import org.fm.dm.DmObject;

public interface OsmTag  extends DmObject {
    public String getKey(String def);
    public void setKey(String key);
    public String getValue(String def);
    public void setValue(String def);
}
