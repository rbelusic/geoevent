package org.fm.app.geoevent.util.rs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.dm.DmAttribute;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;


/**
 *
 * @author RobertoB
 */
public class DmDeserializer extends JsonDeserializer {    
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static Logger L = FmContext.getLogger(DmDeserializer.class);
    private final GeObjectMapper objectMapper;

    class ParserStepInfo {
        ParserStepInfo parentParserInfo;
        public Class resultPropertyClass;
        public String resultPropertyName;        
        public Map<String, Object> properties = new HashMap<String, Object>();
        public List<Object> values = new ArrayList<Object>();                

        public ParserStepInfo(ParserStepInfo parent,Class rsltPropClass,String rsltPropName) {
            parentParserInfo = parent;
            resultPropertyClass = rsltPropClass;
            resultPropertyName = rsltPropName;            
        }
                
        public void add(String name, Object value) {
            if(List.class.isAssignableFrom(resultPropertyClass)) {
                values.add(value);
            } else {
                properties.put(name, value);
            }
        }
    }
    
    public DmDeserializer(GeObjectMapper m) {
        super();
        objectMapper = m;
    }

    @Override
    public Object deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {        
        List<ParserStepInfo> parserStepsInfo = new ArrayList<ParserStepInfo>();
        
        JsonToken currentToken;        
        while ((currentToken = jp.nextValue()) != null) {
            ParserStepInfo currentContext = parserStepsInfo.size() > 0 ? parserStepsInfo.get(parserStepsInfo.size()-1) : null;
            ParserStepInfo parentContext = parserStepsInfo.size() > 1 ? parserStepsInfo.get(parserStepsInfo.size()-2) : null;
            ParserStepInfo newContext;
            
            if(currentContext == null) {
                currentContext = new ParserStepInfo(parentContext,jp.getCurrentName() == null ? ArrayList.class : Map.class,"$");
                parserStepsInfo.add(currentContext);
            }
            
            switch (currentToken) {                
                case START_ARRAY:
                    // kreiraj novi propinfo
                    newContext = new ParserStepInfo(currentContext,ArrayList.class,currentContext == null ? "$" : getCurrentName(jp,currentContext));
                    parserStepsInfo.add(newContext);
                    break;
                    
                case END_ARRAY:
                    parserStepsInfo.remove(parserStepsInfo.size()-1);
                    
                    if(parentContext == null) { // zadnji
                        return currentContext.values;
                    }
                    parentContext.add(currentContext.resultPropertyName, currentContext.values);
                    break;                    
                case START_OBJECT:
                    // kreiraj novi propinfo
                    newContext = new ParserStepInfo(currentContext,Map.class,currentContext == null ? "$" : getCurrentName(jp,currentContext));
                    parserStepsInfo.add(newContext);
                    break;
                case END_OBJECT:
                    parserStepsInfo.remove(parserStepsInfo.size()-1);
                    Object propValue = null;
                    String dmKind = (String) currentContext.properties.get("@dmKind");
                    Class dmKindClass = null;
                    if (dmKind == null || dmKind.isEmpty()) {
                        if(parentContext == null) {
                            dmKindClass = objectMapper.getDefaultDeserializationClass();
                        } else {
                            dmKindClass = parentContext.resultPropertyClass == null ?
                                    objectMapper.getDefaultDeserializationClass() : 
                                    parentContext.resultPropertyClass;
                        }                        
                    } else {
                        dmKindClass = FmApplication.instance()== null || FmContext.getDmClassOfType(dmKind) == null ?
                                Map.class:
                                FmContext.getDmClassOfType(dmKind);
                    }                    
                    if (dmKindClass == Map.class) {
                        propValue = currentContext.properties;
                    } else {
                        if(DmObject.class.isAssignableFrom(dmKindClass)) {                                
                            propValue = createDm((Class<DmObject>)dmKindClass, currentContext.properties);
                        } else {
                            propValue = currentContext.properties; // nepoznato!!
                        }
                    }
                    
                    if(parentContext == null) {
                        return propValue;
                    }
                    parentContext.add(currentContext.resultPropertyName, propValue);                    
                    break;          
                case VALUE_STRING:
                    currentContext.add(getCurrentName(jp,currentContext), jp.getValueAsString());
                    break;
                    
                case VALUE_NUMBER_INT:
                    currentContext.add(getCurrentName(jp,currentContext), jp.getValueAsLong());
                    break;
                    
                case VALUE_NUMBER_FLOAT:
                    currentContext.add(getCurrentName(jp,currentContext), jp.getValueAsDouble());
                    break;
                    
                case VALUE_FALSE:
                    currentContext.add(getCurrentName(jp,currentContext), Boolean.FALSE);
                    break;
                    
                case VALUE_TRUE:
                    currentContext.add(getCurrentName(jp,currentContext), Boolean.TRUE);
                    break;
                    
                case VALUE_NULL:
                    currentContext.add(getCurrentName(jp,currentContext), null);
                    break;
                    
                default:
                    currentContext.add(getCurrentName(jp,currentContext), jp.getText());
                    L.info("Unknown token type:" + currentToken.asString()); 
                ;
            }
        }
        
        // must be only one map in list
        return null;
   }
    
    private String getCurrentName(JsonParser jp, ParserStepInfo stepInfo) throws IOException {
        if(stepInfo == null) return "$";
        if(stepInfo.resultPropertyClass != null && List.class.isAssignableFrom(stepInfo.resultPropertyClass)) {
            return String.valueOf(stepInfo.values.size());
        } 
        return jp.getCurrentName();
    }   
    
    private <T extends DmObject> T createDm(Class<T> cls, Map<String, Object> map) {
        T dm = DmFactory.create(cls);
        
        for(String prop: DmFactory.getDmClassAttributes(cls).keySet()) {
            DmAttribute meta = DmFactory.getDmAttributeMetadata(cls, prop);
            String jsonName = meta.getMethodAnnotation("set" + meta.getName(), JsonProperty.class) == null ?
                    prop :                    
                    ((JsonProperty)meta.getMethodAnnotation("set" + meta.getName(), JsonProperty.class)).value();
            Object propValue = map.get(jsonName);

            if(meta != null && propValue != null) {
                Object dmPropValue = null;
                if(meta.getType().isAssignableFrom(propValue.getClass())) {
                    dmPropValue = propValue;
                } else if(DmObject.class.isAssignableFrom(meta.getType())) {
                    DmObject dmAttr = createDm(meta.getType(), (Map<String, Object>) propValue);
                    dmPropValue = dmAttr;
                } else {
                    dmPropValue = convert(propValue,meta.getType()); 
                }
                dm.setAttr(prop, dmPropValue); 
            }            
        }
        
        return dm;
    }
    
    private <T> T convert(Object value, Class<T> cls) {
        Object convValue = null;
        Constructor<T> ctor;
        try {
            if(Date.class.isAssignableFrom(cls)) {
                if(value instanceof Number) {
                    convValue = new Date(((Number)value).longValue());
                } else {
                    convValue = DATE_FORMAT.parse(value.toString());
                }
            } else {
                ctor = cls.getConstructor(String.class);
                convValue = ctor.newInstance(new Object[] { value.toString() });             
            }
        } catch (Exception ex) {
            L.log(Level.WARNING, "Error converting " + value + " to " + cls.getSimpleName(),ex);
            
        }
        return (T)convValue;
    }

}
  
