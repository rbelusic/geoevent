package org.fm.app.geoevent.util.rs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.application.FmContext;
import org.fm.dm.DmAttribute;

import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;


/**
 *
 * @author RobertoB
 */
public class DmSerializer extends JsonSerializer {
    private static Logger L = FmContext.getLogger(DmSerializer.class);
    private final ObjectMapper objectMapper;
    
    public DmSerializer(ObjectMapper m) {
        super();
        objectMapper = m;
    }
    @Override
    public void serialize(Object ob, JsonGenerator jg, SerializerProvider sp) throws IOException, JsonProcessingException {
        //L.log(Level.INFO, "Start" );
        if(ob == null) {
            return;
            //jg.writeNull();
        } else  if(ob instanceof List) {
            List lst = (List)ob;
            jg.writeStartArray();
            for(Object lo: lst) {
                jg.writeObject(lo);
            }
            jg.writeEndArray();
        } else if(ob instanceof DmObject) {
            DmObject dm = (DmObject)ob;        
            jg.writeStartObject();
            for(String key: dm.getAttributeNames()) {
                Object value = dm.getAttr(key,null);
                if(value == null) continue;
                DmAttribute meta = 
                        DmFactory.getDmAttributeMetadata(((DmObject)ob).getSubClass(), key);
            
                String jsonName = meta.getMethodAnnotation("get" + meta.getName(), JsonProperty.class) == null ?
                        key :           
                        ((JsonProperty)meta.getMethodAnnotation("get" + meta.getName(), JsonProperty.class)).value();
                
                
                jg.writeFieldName( jsonName );
                if(value == null) {
                    jg.writeNull();
                } else {
                    jg.writeObject( value );
                }                
            }
            jg.writeFieldName("@dmKind");
            jg.writeString(DmFactory.getDmKind(dm.getSubClass()));

            jg.writeEndObject();
            
        }
        //L.log(Level.INFO, "End" );
    }

}
