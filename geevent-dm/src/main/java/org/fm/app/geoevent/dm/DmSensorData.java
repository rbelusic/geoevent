package org.fm.app.geoevent.dm;

import java.util.List;
import org.fm.client.sensors.FmSensor;
import org.fm.dm.DmObject;

/**
 *
 * @author robertob
 */
public interface DmSensorData extends DmObject {
    public FmSensor.SENSOR_TYPE getSensor(FmSensor.SENSOR_TYPE def);
    public void  setSensor(FmSensor.SENSOR_TYPE def);
    
    public List<Double> getValues(List<Double> def);
    public void setValues(List<Double> def);
    
    public Integer getAccuracy(Integer def);
    public void  setAccuracy(Integer def);
    
    public Long getTimestamp(Long def);
    public void  setTimestamp(Long def);    
    
}
