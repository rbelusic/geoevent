package org.fm.app.geoevent.dm;

import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

@DmDataIdAttribute(name = "name")
public interface DmSysInfo extends DmObject {
    public String getName(String def);
    public void setName(String def);

    public String getVersion(String def);
    public void setVersion(String def);

    public String getBuildTime(String def);
    public void setBuildTime(String def);
}
