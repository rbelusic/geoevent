package org.fm.app.geoevent.apis.osm.dm;

import org.fm.dm.DmObject;

public interface OsmImg extends DmObject {
    public String getHref(String def);
    public String setHref(String def);
}
