package org.fm.http.client.impl;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.fm.io.IoUtils;
import org.fm.http.client.FmHttpBodyParam;
import org.fm.http.client.FmHttpClient;
import org.fm.http.client.FmHttpFileParam;
import org.fm.http.client.FmHttpParam;
import org.fm.http.client.FmHttpParamList;
import org.fm.http.client.FmHttpResponse;
import org.fm.utils.Base64;

/**
 *
 * @author RobertoB
 */
public class FmApacheHttpClient implements FmHttpClient{

    private static final Logger LOG = Logger.getLogger(FmHttpClient.class.getName());

    private final static Map<FmHttpClient.REQUEST_TYPE, Class> requestTypes = new HashMap<FmHttpClient.REQUEST_TYPE, Class>() {{
            put(FmHttpClient.REQUEST_TYPE.GET, HttpGet.class);
            put(FmHttpClient.REQUEST_TYPE.PUT, HttpPut.class);
            put(FmHttpClient.REQUEST_TYPE.POST, HttpPost.class);
            put(FmHttpClient.REQUEST_TYPE.DELETE, HttpDelete.class);
            put(FmHttpClient.REQUEST_TYPE.HEAD, HttpHead.class);
            put(FmHttpClient.REQUEST_TYPE.OPTIONS, HttpOptions.class);
        }};

    
    
    public FmHttpResponse invoke(FmHttpClient.REQUEST_TYPE requestType, // GET, POST, PUT,DELETE,OPTIONS, HEAD
            String endpoint, // http://.../rest/session/login
            String contentType, // application/json
            FmHttpParamList args, // username=..&password=..
            Map<String, String> headers // nasaSessionId=..			
    ) throws Exception {
        return invoke(requestType, endpoint, contentType, args, headers, null);
    }

    public FmHttpResponse invoke(
            FmHttpClient.REQUEST_TYPE requestType, String endpoint, String contentType,
            FmHttpParamList args,
            Map<String, String> headers, Map<String, Object> cookies
    ) throws Exception {
        boolean isMultipart = contentType != null && contentType.startsWith("multipart/form-data");
        
        // -- kreiraj request --------------------------------------------------
        Class<HttpRequestBase> reqClass = requestTypes.get(requestType);
        if (reqClass == null) {
            throw new Exception("Request of type [" + requestType
                    + "] is not supported.");
        }
        HttpRequestBase request = reqClass.newInstance();
        
        
        // kreiraj url
        URIBuilder uriBuilder = new URIBuilder(endpoint);
        if (FmHttpClient.REQUEST_TYPE.GET.equals(requestType)) { // akoje GET zakaci parametre na url
            for (FmHttpParam param : args) {
                String content = 
                    param.getContent() == null ? "" : (
                        param.isByteArray() ? new String((byte[]) param.getContent()) : param.getContent().toString()
                    );
                
                uriBuilder.addParameter(param.getName(), content);
            }
        }        
        URI uri = uriBuilder.build();
        request.setURI(uri);

        // kreiraj headere
        if (contentType != null && !contentType.isEmpty() && !isMultipart) {
            request.addHeader("Content-Type", contentType);
        }
        if (headers != null) {
            for (Map.Entry<String, String> e : headers.entrySet()) {
                request.addHeader(e.getKey(), (e.getValue() == null ? "" : e
                        .getValue()).toString());
            }
        }

        // POST & PUT parametri
        if ((request instanceof HttpPost) || (request instanceof HttpPut)) {                    
            // -- multipart ----------------------------------------------------
            if(isMultipart) {
                MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
                for (FmHttpParam param : args) {
                    if (param instanceof FmHttpBodyParam) {
                        multipartEntityBuilder
                            .addTextBody(param.getName(), param.getContent().toString(), ContentType.TEXT_PLAIN);
                             
                    } else if(param instanceof FmHttpFileParam) {
                        FmHttpFileParam fp = (FmHttpFileParam) param;
                        multipartEntityBuilder
                            .addBinaryBody(fp.getName(), (File)fp.getContent(), ContentType.APPLICATION_OCTET_STREAM, fp.getFileName());
    
                    } else { // simple param
                        multipartEntityBuilder
                            .addTextBody(param.getName(), param.getContent().toString(), ContentType.TEXT_PLAIN);
                    }
                }
                HttpEntity reqEntity = multipartEntityBuilder.build();
                if (request instanceof HttpPost) {
                    ((HttpPost) request).setEntity(reqEntity);
                } else if (request instanceof HttpPut) {
                    ((HttpPut) request).setEntity(reqEntity);
                }
                
            } 
            // -- not multipart ------------------------------------------------
            else {
                List<NameValuePair> argsList = new ArrayList<NameValuePair>();

                for (FmHttpParam param : args) {
                    if (param instanceof FmHttpBodyParam) {
                        String bodyStr = param.getContent().toString();
                        StringEntity ent = new StringEntity(bodyStr, "UTF-8");
                        ent.setContentType(param.getContentType());
                        if (request instanceof HttpPost) {
                            ((HttpPost) request).setEntity(ent);
                        } else if (request instanceof HttpPut) {
                            ((HttpPut) request).setEntity(ent);
                        }
                    } else if (param instanceof FmHttpFileParam) {
                        // only multipart                
                    } else {
                        // normalan name & value
                        argsList.add(
                            new BasicNameValuePair(
                                param.getName(), 
                                (param.getContent() == null ? "" : param.toString())
                            )
                        );
                    }
                }
                
                if (argsList.size() > 0) {
                    if (request instanceof HttpPost) {
                        ((HttpPost) request).setEntity(new UrlEncodedFormEntity(
                                argsList, "UTF-8"));
                    } else if (request instanceof HttpPut) {
                        ((HttpPut) request).setEntity(new UrlEncodedFormEntity(
                                argsList, "UTF-8"));
                    }
                }                            
            }
        }           

        // http(s) client
        BasicCookieStore cookieStore = new BasicCookieStore();
        if (cookies != null) {
            for (Map.Entry<String, Object> e : cookies.entrySet()) {
                cookieStore.addCookie((Cookie) e.getValue());
            }
        }
        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                builder.build());
        CloseableHttpClient cli = HttpClients.custom().setSSLSocketFactory(
                sslsf).setDefaultCookieStore(cookieStore).build();

        // call
        CloseableHttpResponse response = null;
        InputStream is = null;
        try {
            dumpRequest(request);
            response = cli.execute(request);
            dumpResponse(response);
            int statusCode = response.getStatusLine().getStatusCode();

            try {
                is = response.getEntity().getContent();
            } catch (Exception e1) {
            }

            List<Cookie> clist = cookieStore.getCookies();
            Map<String, Object> cmap = new HashMap<String, Object>();

            for (Cookie cookie : clist) {
                cmap.put(cookie.getName(), cookie);
            }
            return new FmHttpResponse(statusCode, IoUtils.getInputStreamAsByteArray(is), cmap);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (Exception e1) {
            }

            try {
                if (response != null) {
                    response.close();
                }
            } catch (Exception e1) {
            }
            try {
                if (cli != null) {
                    cli.close();
                }
            } catch (Exception e1) {
            }
        }
    }


    public <P extends FmHttpParam> FmHttpResponse put(String endpoint,
            String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception {
        return invoke(FmHttpClient.REQUEST_TYPE.PUT, endpoint, contentType, args, headers);
    }

    public <P extends FmHttpParam> FmHttpResponse get(String endpoint,
            String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception {
        return invoke(FmHttpClient.REQUEST_TYPE.GET, endpoint, contentType, args, headers);
    }

    public <P extends FmHttpParam> FmHttpResponse post(
            String endpoint, String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception {
        return invoke(FmHttpClient.REQUEST_TYPE.POST, endpoint, contentType, args, headers);
    }

    public <P extends FmHttpParam> FmHttpResponse delete(
            String endpoint, String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception {
        return invoke(FmHttpClient.REQUEST_TYPE.DELETE, endpoint, contentType, args, headers);
    }

    public <P extends FmHttpParam> FmHttpResponse options(
            String endpoint, String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception {
        return invoke(FmHttpClient.REQUEST_TYPE.OPTIONS, endpoint, contentType, args, headers);
    }

    public <P extends FmHttpParam> FmHttpResponse head(
            String endpoint, String contentType, FmHttpParamList args,
            Map<String, String> headers) throws Exception {
        return invoke(FmHttpClient.REQUEST_TYPE.HEAD, endpoint, contentType, args, headers);
    }


    // private
    private Map<String, String> addBasicAuthHeader(String username, String password,Map<String, String> headers) {
        //Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
        if(headers == null) {
            headers = new HashMap<String, String>();
        }
        headers.put("Authorization", "Basic " + Base64.encodeBytes((username + ":" + password).getBytes(),Base64.DONT_BREAK_LINES));
        return headers;
    }
    
    private static void dumpRequest(HttpRequestBase request) {
        LOG.info("REST REQUEST:" + request);
    }

    private static void dumpResponse(CloseableHttpResponse response) {
        LOG.info("REST RESPONSE:" + response);
    }
    

}
