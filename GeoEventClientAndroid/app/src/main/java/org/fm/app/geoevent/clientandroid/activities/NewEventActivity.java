package org.fm.app.geoevent.clientandroid.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.app.geoevent.client.GeRestClient;
import org.fm.app.geoevent.clientandroid.Config;
import org.fm.app.geoevent.clientandroid.R;
import org.fm.app.geoevent.clientandroid.RestAsyncTask;
import org.fm.app.geoevent.clientandroid.impl.FmEventServiceMsgsDispatcher;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.app.geoevent.clientandroid.services.events.EventsService;
import org.fm.app.geoevent.clientandroid.services.events.EventsServiceUtils;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.http.client.impl.FmApacheHttpClient;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A login screen that offers login via email/password.
 */
public class NewEventActivity extends FmActivity {
    private final static Logger LOG = FmContext.getLogger(NewEventActivity.class);
    private FmEventServiceMsgsDispatcher eventsDispatcher = null;
    private RestAsyncTask mAuthTask = null;

    // UI references.
    private EditText mTitleView;
    private EditText mMessageContentView;
    private View mProgressView;
    private View mPostEventFormView;
    private ImageView mImageView;
    private Button mPostButton;
    private EditText mAddress;
    private DmGpsLocation lastLocation=null;
    private DmGpsLocation formerLocation = null;
    private NomPlace lastPlace = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // build ui
        buildUI();

        // start service
        startService(new Intent(getBaseContext(), EventsService.class));

        // create events dispatcher
        eventsDispatcher = new FmEventServiceMsgsDispatcher(this);
    }

    private void buildUI() {
        // Set up the post form
        setContentView(R.layout.activity_new_event);

        mTitleView = (EditText) findViewById(R.id.title);
        mMessageContentView = (EditText) findViewById(R.id.message_content);
        mMessageContentView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.post || id == EditorInfo.IME_NULL) {
                    attemptPost();
                    return true;
                }
                return false;
            }
        });

        mImageView = (ImageView) findViewById(R.id.event_photo);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeEventPhoto();

            }
        });

        mAddress = (EditText) findViewById(R.id.address);

        mPostButton = (Button) findViewById(R.id.post_event_button);
        mPostButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptPost();
            }
        });

        mPostEventFormView = findViewById(R.id.post_form);
        mProgressView = findViewById(R.id.post_event_progress);
    }

    private void takeEventPhoto() {
        getEventsServiceUtils().startPhotoChooseActivity();
    }


    @Override
    public void onResume() {
        super.onResume();
        if(eventsDispatcher.isListener(this)) {
            eventsDispatcher.resume();
            eventsDispatcher.requestUpdate();
        } else {
            eventsDispatcher.addListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        eventsDispatcher.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();  // Always call the superclass method first
        eventsDispatcher.removeAllListeners();
    }

    private void updateViews() {
        mPostButton.setEnabled(!Config.getConfiguration().getToken("").isEmpty());

        String curAddr = lastPlace == null ? "" : lastPlace.getDisplayName("");
        if(!curAddr.equals(mAddress.getText())) {
            mAddress.setText(curAddr);
        }



    }

    private void updateImageView(Uri imgUri) {
        if(imgUri != null) mImageView.setImageURI(imgUri);
    }

    // -- fm callbacks ---
    public void onConfigurationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New configuration received.");
        DmConfiguration cfg = (DmConfiguration) data;
        Config.setConfiguration(cfg);

        updateViews();
    }

    /**
     * Attempts to sign in to the account specified by the login form.
     * If there are form errors (missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void onRestError(Object sender, Object data) {
        mAuthTask = null;
        showProgress(false);

        Exception e =  (Exception) ((Object[])data)[2];
        ((TextView)findViewById(R.id.status)).setText(((Exception)data).getMessage());
        mMessageContentView.setError(getString(R.string.error_event_post));
        mTitleView.requestFocus();
    }

    public void onRestResponse(Object sender, Object data) {
        showProgress(false);

        GeRestClient.Endpoint endPoint = (GeRestClient.Endpoint) ((Object[])data)[0];
        if(endPoint.equals(GeRestClient.Endpoint.EVENTS_ADD)) {
            mAuthTask = null;
            ((TextView)findViewById(R.id.status)).setText(R.string.post_done_status);
            returnPostStatus(true);
        }
    }

    public void onAddressChangedChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New address received.");
        lastPlace = (NomPlace) data;
        LOG.log(Level.INFO, "Current address is now " + lastPlace + ".");
        updateViews();
        LOG.log(Level.INFO, "Handling new address event is done.");
    }

    public void onBestLocationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New location received.");
        formerLocation = lastLocation;
        lastLocation = (DmGpsLocation) data;
        LOG.log(Level.INFO, "Current location is now " + lastLocation + ".");
        updateViews();
        LOG.log(Level.INFO, "Handling new location event is done.");
    }


    @Override
    public void onBackPressed() {
        returnPostStatus(false);
    }

    private void returnPostStatus(boolean postStatus) {

        // Create intent to deliver some kind of result data
        Intent result = new Intent("POST_EVENT_ACTION");
        setResult(postStatus ? Activity.RESULT_OK : Activity.RESULT_CANCELED, result);

        finish();
    }

    private void attemptPost() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mTitleView.setError(null);
        mMessageContentView.setError(null);


        // Store values at the time of the login attempt.
        final String title = mTitleView.getText().toString().trim();
        final String msgtext = mMessageContentView.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        if(Config.getConfiguration().getToken("").isEmpty()) {
            mMessageContentView.setError(getString(R.string.post_error_login_required));
            focusView = mMessageContentView;
            cancel = true;
        }

        if(lastLocation == null) {
            mMessageContentView.setError(getString(R.string.post_error_location_required));
            focusView = mMessageContentView;
            cancel = true;
        }

        // Check for a valid password.
        if (TextUtils.isEmpty(msgtext)) {
            mMessageContentView.setError(getString(R.string.post_error_field_required));
            focusView = mMessageContentView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(title)) {
            mTitleView.setError(getString(R.string.post_error_field_required));
            focusView = mTitleView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt post and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            final DmEvent event = getEventsServiceUtils().createEvent(title,msgtext,lastLocation, lastPlace);

            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask =
                (RestAsyncTask) new RestAsyncTask(this,new GeRestClient(new FmApacheHttpClient(), Config.SERVER_URL))
                    .execute(GeRestClient.Endpoint.EVENTS_ADD, new HashMap<String, Object>() {{
                        put("$events", new DmEvent[] {event});
                    }});
        }
    }



    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mPostEventFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mPostEventFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mPostEventFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mPostEventFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == EventsServiceUtils.CAMERA_DIALOG_REQUEST_ID) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Uri selectedImageUri = null;
                if (isCamera) {
                    selectedImageUri = Uri.fromFile(getEventsServiceUtils().getOutputMediaFile());
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }
                if(selectedImageUri != null) {
                    updateImageView(selectedImageUri);
                }
            }
        }
    }

}

