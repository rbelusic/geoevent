package org.fm.app.geoevent.clientandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.library21.custom.SwipeRefreshLayoutBottom;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.fm.app.geoevent.clientandroid.Config;
import org.fm.app.geoevent.clientandroid.R;
import org.fm.app.geoevent.clientandroid.adapters.LocationsAdapter;
import org.fm.app.geoevent.clientandroid.dialogs.GeDialog;
import org.fm.app.geoevent.clientandroid.impl.FmEventServiceMsgsDispatcher;
import org.fm.app.geoevent.clientandroid.impl.FmLocationDispatcher;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.app.geoevent.clientandroid.services.events.EventsService;
import org.fm.app.geoevent.clientandroid.services.events.EventsServiceUtils;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.application.FmContext;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GpsLocationsListActivity extends FmActivity implements  SwipeRefreshLayoutBottom.OnRefreshListener {
    private final static Logger LOG = FmContext.getLogger(GpsLocationsListActivity.class);

    private List<DmGpsLocation> locations = new ArrayList<>();

    private FmLocationDispatcher dispatcher;
    private LocationsAdapter locationAdapter = null;
    private FmEventServiceMsgsDispatcher eventsDispatcher;
    private SwipeRefreshLayoutBottom swpBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // -- android ui --
        buildUI();

        // start service
        startService(new Intent(getBaseContext(), EventsService.class));


        // kreiraj dispatcher
        eventsDispatcher = new FmEventServiceMsgsDispatcher(this);
    }

        // -- private --
        private void buildUI() {
            // main ui
            setContentView(R.layout.activity_scrolling_list);
            Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
            setSupportActionBar(toolbar);

            // swype
            swpBottom = (SwipeRefreshLayoutBottom) findViewById(R.id.swiperefresh);
            swpBottom.setOnRefreshListener(this);

            // list and adapter
            final ListView listView = (ListView) findViewById(R.id.list);

            locationAdapter = new LocationsAdapter(this, new ArrayList<DmGpsLocation>());
            listView.setAdapter(locationAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    try {
                        getEventsServiceUtils().startMapActivity(null, (DmGpsLocation) listView.getAdapter().getItem(position));
                    } catch (Exception e) {
                        showLongInfo("Show map err:" + e.getMessage());
                    }
                }
            });

    }



    @Override
    public void onResume() {
        super.onResume();
        if(eventsDispatcher.isListener(this)) {
            eventsDispatcher.resume();
            eventsDispatcher.requestUpdate();
        } else {
            eventsDispatcher.addListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        eventsDispatcher.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();  // Always call the superclass method first
        eventsDispatcher.removeAllListeners();
    }



    private MenuItem loginMenuItem;

    private void updateLoginMenuItem() {
        loginMenuItem.setIcon(Config.getConfiguration().getToken("").isEmpty() ? R.drawable.ic_action_login : R.drawable.ic_action_logout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling_list, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(GpsLocationsListActivity.this, "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_LONG).show();
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });

        loginMenuItem = menu.findItem(R.id.action_login);
        updateLoginMenuItem();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
        } else if(id == R.id.action_showmap) {
            getEventsServiceUtils().startMapActivity(null, locations.size() > 0 ? locations.get(0) : null);
        } else if(id == R.id.action_new_event) {
            getEventsServiceUtils().startNewEventActivity();
        } else if(id == R.id.action_gpslist) {
            getEventsServiceUtils().startLocationsListActivity();;
        } else if(id == R.id.action_login) {
            DmConfiguration config = Config.getConfiguration();
            if(config.getToken("").isEmpty()) {
                getEventsServiceUtils().startLoginActivity();
            } else {
                config.setToken("");
                eventsDispatcher.requestReloadConfiguration(config);
            }
        } else {
            return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == EventsServiceUtils.LOGIN_DIALOG_REQUEST_ID) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

            }
        }
    }
    // -- fm calllbacks ----------------------------------------------------------------------------
    public void onServiceConnected(Object sender, Object params) {
        try {
            locations = getEventsServiceUtils().loadGpsLocations(0,50);
            locationAdapter.addAll(locations);

        } catch(Exception e) {
            LOG.log(Level.INFO, "Location load error:", e);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            GeDialog.showDialog(GpsLocationsListActivity.this,e.getMessage(),sw.toString());
        }
    }


    public void onConfigurationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New configuration received.");
        DmConfiguration cfg = (DmConfiguration) data;
        Config.setConfiguration(cfg);
        updateLoginMenuItem();
    }

    public void onBestLocationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New location received.");
        DmGpsLocation lastLocation = (DmGpsLocation) data;

        LOG.log(Level.INFO, "Current location is now " + lastLocation + ".");
        locations.add(0, lastLocation);
        locationAdapter.refreshList(locations);
        LOG.log(Level.INFO, "Handling new location event is done.");
    }


    @Override
    public void onRefresh() {

        swpBottom.setRefreshing(true);

        try {
            setProgressBarIndeterminateVisibility(true);
            List<DmGpsLocation> newLocations = getEventsServiceUtils().loadGpsLocations(locationAdapter.getCount(), 50);
            setProgressBarIndeterminateVisibility(false);
            swpBottom.setRefreshing(false);
            locations.addAll(newLocations);
            locationAdapter.addAll(newLocations);
        } catch(Exception e) {
            LOG.log(Level.INFO, "Location load error:", e);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            GeDialog.showDialog(GpsLocationsListActivity.this, e.getMessage(), sw.toString());
            setProgressBarIndeterminateVisibility(false);
            swpBottom.setRefreshing(false);
        }
    }
}
