package org.fm.app.geoevent.clientandroid;

import android.os.Parcel;
import android.os.Parcelable;

import org.fm.app.geoevent.util.rs.DmSerializer;
import org.fm.application.FmContext;
import org.fm.dm.DmAttribute;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ParcelDm implements Parcelable {
    private DmObject dmObject;

    // You can include parcel data types
    private int mData;
    private String mName;

    // We can also include child Parcelable objects. Assume MySubParcel is such a Parcelable:
    private ParcelDm mInfo;

    // This is where you write the values you want to save to the `Parcel`.
    // The `Parcel` class has methods defined to help you save all of your values.
    // Note that there are only methods defined for simple values, lists, and other Parcelable objects.
    // You may need to make several classes Parcelable to send the data you want.
    @Override
    public void writeToParcel(Parcel out, int flags) {
        if(getDmObject() == null) {
            out.writeValue(null);
            return;
        }

        Map<String, Object> dmMap = dmToMap(getDmObject());
        out.writeMap(dmMap);
    }

    private DmObject mapToDm(Map<String, Object> map) {
        DmObject dm = null;
        try {
            dm = mapToDm(map,getClass().getClassLoader());
        } catch (ClassNotFoundException e) {
            dm = DmFactory.create();
        }
        return(dm);
    }

    private Map<String, Object> dmToMap(DmObject dm) {
        Map<String, Object> map = dmToMap(dm,getClass().getClassLoader());
        return(map);
    }

    // Using the `in` variable, we can retrieve the values that
    // we originally wrote into the `Parcel`.  This constructor is usually
    // private so that only the `CREATOR` field can access.
    private ParcelDm(Parcel in) {
        Map<String, Object> map = in.readHashMap(null);
        DmObject dm = mapToDm(map);

        map = dm.getAttr();
        String className = in.readString();
        int numAttrs = in.readInt();
        Class dmCls = null;
        try {
            dmCls = getClass().getClassLoader().loadClass(className);
        } catch (ClassNotFoundException e) {
            dmCls = DmObject.class;
        }
        DmObject dmObject = DmFactory.create(dmCls);

        int pos=1;
        while(pos < numAttrs) {
            String attr = in.readString();
            DmAttribute attrdef = DmFactory.getDmAttributeMetadata(dmCls, attr);
            Object value;
            ParcelDm parcValue;
            if(DmObject.class.isAssignableFrom(attrdef.getType())) {
                parcValue = in.readParcelable(attrdef.getType().getClassLoader());
                value = parcValue.getDmObject();
            } else {
                value = in.readValue(attrdef.getType().getClassLoader());
            }
            dmObject.setAttr(attr,value);
        }
    }

    public ParcelDm(DmObject dm) {
        dmObject = dm;
    }

    // In the vast majority of cases you can simply return 0 for this.
    // There are cases where you need to use the constant `CONTENTS_FILE_DESCRIPTOR`
    // But this is out of scope of this tutorial
    @Override
    public int describeContents() {
        return 0;
    }

    // After implementing the `Parcelable` interface, we need to create the
    // `Parcelable.Creator<MyParcelable> CREATOR` constant for our class;
    // Notice how it has our class specified as its type.
    public static final Parcelable.Creator<ParcelDm> CREATOR
            = new Parcelable.Creator<ParcelDm>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @Override
        public ParcelDm createFromParcel(Parcel in) {
            return new ParcelDm(in);
        }

        // We just need to copy this and change the type to match our class.
        @Override
        public ParcelDm[] newArray(int size) {
            return new ParcelDm[size];
        }
    };

    public DmObject getDmObject() {
        return dmObject;
    }



    public static DmObject mapToDm(Map<String, Object> map, ClassLoader classLoader) throws ClassNotFoundException {
        DmObject dm = null;
        Class<DmObject> cls = (Class<DmObject>) classLoader.loadClass((String) map.get("@dmKindLong"));
        dm = DmFactory.create(cls);

        for(String attr: map.keySet()) {
            Object value = map.get(attr);
            if(
                value != null &&
                        Map.class.isAssignableFrom(value.getClass()) &&
                        ((Map)value).get("@dmKindLong") != null
            ) {
                dm.setAttr(attr, mapToDm((Map) value, classLoader));
            } else {
                dm.setAttr(attr,value);
            }
        }
        return dm;
    }

    public static HashMap<String, Object> dmToMap(DmObject dm, ClassLoader classLoader) {
        HashMap<String, Object> map = new HashMap<>();
        map.putAll(dm.getAttr());

        for(String attr: map.keySet()) {
            Object value = map.get(attr);
            if(value != null && DmObject.class.isAssignableFrom(value.getClass())) {
                map.put(attr,dmToMap((DmObject)value,classLoader));
            } else {
                map.put(attr, value);
            }
        }
        map.put("@dmKindLong",DmFactory.getDmKindLong(dm.getSubClass()));
        return(map);
    }


}
