package org.fm.app.geoevent.clientandroid.adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;


import com.google.android.gms.location.DetectedActivity;

import org.fm.FmEventsDispatcher;
import org.fm.app.geoevent.clientandroid.R;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.dm.DmAttribute;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;
import org.fm.utils.TimeUtil;

import java.util.Date;
import java.util.List;


public class LocationsAdapter extends FmAdapter<DmGpsLocation>  {
    private FmEventsDispatcher dispatcher = new FmEventsDispatcher(this);

    public LocationsAdapter(Context context, List<DmGpsLocation> values) {
        super(context, R.layout.location_layout, values);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = super.getView(position, convertView, parent);

        // Change the icon
        DmGpsLocation loc = getValues().get(position);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        if(imageView != null) imageView.setImageResource(getUserAactivityIconId(loc.getActivity("unkonown")));

        return rowView;
    }

    @Override
    public String formatValue(DmObject dm, String attr) {
        if (attr.equalsIgnoreCase("timestamp")) {
            Date d = dm.getAttr(attr,null);
            return DateUtils.formatDateTime(getContext(), d.getTime(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE);
        } else if (attr.equalsIgnoreCase("accuracy") || attr.equalsIgnoreCase("altitude")) {
            return formatter2.format(dm.getAttr(attr,0F));
        }
        return super.formatValue(dm, attr);
    }

    public static int getUserAactivityIconId(String activity) {
        if (
                activity == null || activity.isEmpty() ||
                        activity.equalsIgnoreCase("still") ||
                        activity.equalsIgnoreCase("tilting") ||
                        activity.equalsIgnoreCase("unknown")
                ) {
            return R.drawable.ic_activity_still;
        } else if (activity.equalsIgnoreCase("running")) {
            return R.drawable.ic_activity_running;
        } else if (activity.equalsIgnoreCase("biking")) {
            return R.drawable.ic_activity_biking;
        } else if (activity.equalsIgnoreCase("driving")) {
            return R.drawable.ic_activity_driving;
        }
        return R.drawable.ic_activity_still;
    }
}
