package org.fm.app.geoevent.clientandroid.impl.dao;

import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.dao.DaoObject;
import org.fm.dm.DmObject;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by RobertoB on 7.3.2016..
 */
public interface DaoInternal extends DaoObject{
    public <T extends DmObject> List<T> getBy(Class<T> dmObjectClass,String attrName, Object attrValue); // from DaoObjectImpl.class
    public <T extends DmObject> T get(Class<T> dmObjectClass,String id);
    public <T extends DmObject> List<T> get(Class<T> dmObjectClass,String [] ids);
    public <T extends DmObject> void put(T [] dms);

    public <T extends DmObject> void update(T [] dms, Set<String> columns);
    public <T extends DmObject> List<T> find(Class<T> dmObjectClass,String sql,Map<String, Object> params);
    public void execute(String sql,Map<String, Object> params);


    public  List<DmGpsLocation> getRecentGpsLocations(int fromrow, int nrows);
}
