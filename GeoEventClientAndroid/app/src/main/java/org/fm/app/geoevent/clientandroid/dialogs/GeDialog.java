package org.fm.app.geoevent.clientandroid.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by RobertoB on 15.3.2016..
 */
public class GeDialog {
    public static void showDialog(Context ctx,String title, String msg) {
        new AlertDialog.Builder(ctx)
                .setTitle(title)
                .setMessage(msg)
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {}
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
