package org.fm.app.geoevent.clientandroid.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.fm.app.geoevent.client.GeUtils;
import org.fm.app.geoevent.clientandroid.Config;
import org.fm.app.geoevent.clientandroid.R;
import org.fm.app.geoevent.clientandroid.impl.FmEventServiceMsgsDispatcher;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.app.geoevent.clientandroid.services.events.EventsService;
import org.fm.app.geoevent.clientandroid.services.events.EventsServiceUtils;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.application.FmContext;
import org.fm.dm.DmUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventMapActivity extends FmActivity implements OnMapReadyCallback {
    private final static Logger LOG = FmContext.getLogger(EventMapActivity.class);

    private final List<DmEvent> listOfEvents = new ArrayList<>();
    private DmGpsLocation lastLocation = null;
    private DmEvent eventToDisplay  = null;

    private GoogleMap googleMap = null;

    private FmEventServiceMsgsDispatcher eventsDispatcher = null;
    private Marker userLocationMarker = null;
    private Circle userLocationAccCircle = null;
    private Circle notifRadiusCircle = null;
    private Circle notifRadiusCircleDismiss = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if(intent != null) {
            Bundle b = intent.getExtras();
            if(b != null) {
                try {
                    lastLocation = DmUtil.mapToDm((Map<String, Object>) b.getSerializable("location"), getClassLoader());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    eventToDisplay = DmUtil.mapToDm((Map<String, Object>) b.getSerializable("event"), getClassLoader());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        buildUI();

        // start service
        startService(new Intent(getBaseContext(), EventsService.class));
        eventsDispatcher = new FmEventServiceMsgsDispatcher(this);

    }


    @Override
    public void onResume() {
        super.onResume();
        if(eventsDispatcher.isListener(this)) {
            eventsDispatcher.resume();
            eventsDispatcher.requestUpdate();
        } else {
            eventsDispatcher.addListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        eventsDispatcher.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();  // Always call the superclass method first
        eventsDispatcher.removeAllListeners();
    }

    private MenuItem loginMenuItem;

    private void updateLoginMenuItem() {
        loginMenuItem.setIcon(Config.getConfiguration().getToken("").isEmpty() ? R.drawable.ic_action_login : R.drawable.ic_action_logout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_event_map, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(EventMapActivity.this, "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_LONG).show();
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });

        loginMenuItem = menu.findItem(R.id.action_login);
        updateLoginMenuItem();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if(id == R.id.action_current_location) {
            zoomToLocation(lastLocation,googleMap.getCameraPosition().zoom);
        } else if(id == R.id.action_gpslist) {
            getEventsServiceUtils().startLocationsListActivity();
        } else if(id == R.id.action_login) {
            DmConfiguration config = Config.getConfiguration();
            if(config.getToken("").isEmpty()) {
                getEventsServiceUtils().startLoginActivity();
            } else {
                config.setToken("");
                eventsDispatcher.requestReloadConfiguration(config);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == EventsServiceUtils.LOGIN_DIALOG_REQUEST_ID) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

            }
        }
    }

    // -- map ---
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap gm) {
        googleMap = gm;
        setupMap();
    }

    private MarkerOptions createEventMarker(DmEvent ev) {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(
                GeUtils.checkImportance(lastLocation, ev, Config.NOTIF_RADIUS.doubleValue(), Config.NOTIF_RADIUS_DISMISS.doubleValue()) ?
                        R.drawable.marker_event_important : R.drawable.marker_event
        );


        Double lat = ev.getLocation(null).getLatitude(0D);
        Double lon = ev.getLocation(null).getLongitude(0D);
        LatLng ll = new LatLng(lat, lon);
        MarkerOptions marker = new MarkerOptions()
                .position(ll)
                .title(ev.getMessage(null).getTitle(""))
                .snippet(ev.getMessage(null).getMessageBody(""))
                .icon(icon);

        return marker;
    }

    private MarkerOptions createGpsLocationMarker(DmGpsLocation loc) {
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(
                R.drawable.marker_user_location
        );

        Double lat = loc.getLatitude(0D);
        Double lon = loc.getLongitude(0D);
        LatLng ll = new LatLng(lat, lon);
        String [] locationInfo = EventsServiceUtils.formatGpsLocationInfo(this,loc);
        MarkerOptions marker = new MarkerOptions()
                .position(ll)
                .title(Config.getConfiguration().getUsername("My location") )
                .snippet(locationInfo[0] + "\n" + locationInfo[1])
                .icon(icon);

        return marker;
    }

    private void updateLocationMapMarker() {
        if(googleMap == null || lastLocation == null) return;

        if(userLocationMarker != null) {
            userLocationMarker.remove();
            userLocationMarker = null;
        }

        if(userLocationAccCircle != null) {
            userLocationAccCircle.remove();
            userLocationAccCircle = null;
        }

        if(notifRadiusCircle != null) {
            notifRadiusCircle.remove();
            notifRadiusCircle = null;
        }

        if(notifRadiusCircleDismiss != null) {
            notifRadiusCircleDismiss.remove();
            notifRadiusCircleDismiss = null;
        }


        userLocationMarker = googleMap.addMarker(createGpsLocationMarker(lastLocation));
        Double lat = lastLocation.getLatitude(0D);
        Double lon = lastLocation.getLongitude(0D);
        LatLng latlng = new LatLng(lat, lon);

        CircleOptions accCircle = (lastLocation.getAccuracy(null) != null) ?
                new CircleOptions()
                    .center(latlng)
                    .fillColor(Color.argb(160, 80, 0, 60))
                    .strokeColor(Color.rgb(80, 0, 60))
                    .strokeWidth(1f)
                    .radius(lastLocation.getAccuracy(10D))
                : null;
        CircleOptions notifCircle = new CircleOptions()
                .center(latlng)
                .fillColor(Color.argb(160, 255,239,213))
                .strokeColor(Color.rgb(255,239,213))
                .strokeWidth(1f)
                .radius(Config.NOTIF_RADIUS * 1000D);
        CircleOptions notifCircleDismiss = new CircleOptions()
                .center(latlng)
                .fillColor(Color.argb(60, 152,251,152))
                .strokeColor(Color.rgb(152,251,152))
                .strokeWidth(1f)
                .radius(Config.NOTIF_RADIUS_DISMISS * 1000D);

        notifRadiusCircleDismiss = googleMap.addCircle(notifCircleDismiss);
        notifRadiusCircle = googleMap.addCircle(notifCircle);
        userLocationAccCircle = accCircle == null || lastLocation.getAccuracy(null) > Config.NOTIF_RADIUS *1000D ? null : googleMap.addCircle(accCircle); // accr
    }

    private Map<String,Marker> eventMarkers = new HashMap<>();

    private void updateEventMapMarker(DmEvent dm) {
        Marker marker;
        if(eventMarkers.get(dm.getId("")) != null) {
            marker = eventMarkers.get(dm.getId(""));
            marker.remove();
        }
        marker = googleMap.addMarker(createEventMarker(dm));
        eventMarkers.put(dm.getId(""),marker);
    }

    private void updateEventsMapMarkers(List<DmEvent> updatedEvents) {
        if(googleMap == null) return;

        // events
        if(updatedEvents != null) { // display and update only requested event
            for(DmEvent ev: updatedEvents) {
                if(eventToDisplay == null) {
                    updateEventMapMarker(ev);
                } else if(ev.getId("").equals(eventToDisplay.getId(""))) {
                    eventToDisplay = ev;
                    updateEventMapMarker(ev);
                }
            }
        } else { // display only event to display
            if(eventToDisplay != null) {
                updateEventMapMarker(eventToDisplay);
            }
        }
    }

    // -- fm callbacks ---
    public void onConfigurationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New configuration received.");
        DmConfiguration cfg = (DmConfiguration) data;
        Config.setConfiguration(cfg);
        updateLoginMenuItem();
    }

    public void onEventsReceived(Object sender, Object data) {
        LOG.log(Level.INFO, "Events received.");
        List<DmEvent> rcvEvents =  ((List<DmEvent>) data);
        LOG.log(Level.INFO, "Received events list has " + rcvEvents.size() + " events.");

        // new events
        List<DmEvent> diffEvents = DmUtil.listNotIn(rcvEvents, listOfEvents);
        LOG.log(Level.INFO, "There is " + diffEvents.size() + " new events in received list of events.");

        // map of old important events
        Map<String, DmEvent> importantEventsMap = new HashMap<>();
        for(DmEvent ev: listOfEvents) {
            if(ev.getInsAttr("important", Boolean.FALSE)) {
                importantEventsMap.put(ev.getId(""), ev);
            }
        }
        LOG.log(Level.INFO, "Old events list had " + importantEventsMap.size() + " events.");

        // create new list
        List<DmEvent> allEvents = DmUtil.listNotIn(listOfEvents, rcvEvents);
        allEvents.addAll(rcvEvents);

        List<DmEvent> newListOfEvents =  GeUtils.optimizeSortedEventsList(
                GeUtils.recalculateEventDistancesAndSort(
                        lastLocation, allEvents, Config.NOTIF_RADIUS.doubleValue(), Config.NOTIF_RADIUS_DISMISS.doubleValue()
                ),
                Config.MAX_CACHED_EVENTS_LIST_SIZE, Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT
        );
        LOG.log(Level.INFO, "New events list has " + newListOfEvents.size() + " events.");

        // create list for nortif
        List<DmEvent> notifEvList = new ArrayList<>();
        for(DmEvent ev: newListOfEvents) {
            if(ev.getInsAttr("important",Boolean.FALSE) && !importantEventsMap.containsKey(ev.getId(""))) {
                notifEvList.add(ev);
            }
        }
        LOG.log(Level.INFO, "List of events with changed importance has " + notifEvList.size() + " events.");

        LOG.log(Level.INFO, "Updating list of cached events ...");
        listOfEvents.clear();
        listOfEvents.addAll(newListOfEvents);

        LOG.log(Level.INFO, "Updating markers on map ...");
        updateEventsMapMarkers(listOfEvents);
        LOG.log(Level.INFO, "Handling new events is done.");
    }


    public void onBestLocationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New location received.");
        DmGpsLocation formerLocation = lastLocation;
        lastLocation = (DmGpsLocation) data;
        lastLocation.setInsAttr("marker",formerLocation == null ? null : formerLocation.getInsAttr("marker",null));

        LOG.log(Level.INFO, "Current location is now " + lastLocation + ".");
        List<DmEvent> newEvents =  GeUtils.optimizeSortedEventsList(
                GeUtils.recalculateEventDistancesAndSort(
                        lastLocation, listOfEvents, Config.NOTIF_RADIUS.doubleValue(), Config.NOTIF_RADIUS_DISMISS.doubleValue()
                ),
                Config.MAX_CACHED_EVENTS_LIST_SIZE, Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT
        );
        listOfEvents.clear();
        listOfEvents.addAll(newEvents);

        LOG.log(Level.INFO, "Updating location marker on map ...");
        updateLocationMapMarker();
        LOG.log(Level.INFO, "Updating events markers on map ...");
        updateEventsMapMarkers(listOfEvents);
        LOG.log(Level.INFO, "Handling new location event is done.");
    }

    // -- private ---
    private void buildUI() {
        setContentView(R.layout.activity_event_map);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setupMap() {
        UiSettings settings = googleMap.getUiSettings();
        settings.setMapToolbarEnabled(true);

        // add marker to event
        if(eventToDisplay != null) {
            updateEventsMapMarkers(null);
            Double lat = eventToDisplay.getLocation(null).getLatitude(0D);
            Double lon = eventToDisplay.getLocation(null).getLongitude(0D);
            LatLng latlng = new LatLng(lat, lon);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 18f));
        }
        if(lastLocation != null) {
            updateLocationMapMarker();
            if(eventToDisplay == null) {
                zoomToLocation(lastLocation,16f);
            }
        }

        if(lastLocation == null && eventToDisplay == null) {
            Double lat = (0D);
            Double lon = (0D);
            LatLng latlng = new LatLng(lat, lon);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 16f));
        }

    }

    private void zoomToLocation(DmGpsLocation loc, float zoom) {
        if(loc != null) {
            Double lat = loc.getLatitude(0D);
            Double lon = loc.getLongitude(0D);
            LatLng latlng = new LatLng(lat, lon);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng,zoom));
        }
    }
}
