package org.fm.app.geoevent.clientandroid.impl.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.Toast;

import org.fm.app.geoevent.dm.DmSensorData;
import org.fm.client.sensors.FmSensor;
import org.fm.dm.DmFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.WINDOW_SERVICE;

/**
 * @author rbelusic
 */
public class FmSensorAndroid extends FmSensor implements SensorEventListener {
    private Context ctx;
    private Display display;
    private Sensor androidSensor;
    private Integer sensorTypeAndroid;

    private int currentAccuracy = SensorManager.SENSOR_STATUS_UNRELIABLE;

    private static Map<SENSOR_TYPE, Integer> sensorLinkTable = new HashMap<SENSOR_TYPE, Integer>() {{
        put(FmSensor.SENSOR_TYPE.MAGNETIC_FIELD, Sensor.TYPE_MAGNETIC_FIELD);
        put(FmSensor.SENSOR_TYPE.ACCELEROMETER, Sensor.TYPE_LINEAR_ACCELERATION);
    }};

    public FmSensorAndroid(Object owner, Context context, FmSensor.SENSOR_TYPE stype) {
        super(owner, stype);
        ctx = context;
        sensorTypeAndroid = sensorLinkTable.get(getSensorType());
        display = ((WindowManager) ctx.getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
    }

    @Override
    public void start() {
        SensorManager sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);
        if (androidSensor == null && sensorTypeAndroid != null) {
            androidSensor = ((SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE)).
                    getDefaultSensor(sensorTypeAndroid);
        }
        if (androidSensor != null) {
            sensorManager.registerListener(this, androidSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void stop() {
        if (androidSensor != null) {
            SensorManager sensorManager = (SensorManager) ctx.getSystemService(Context.SENSOR_SERVICE);
            sensorManager.unregisterListener(this, androidSensor);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent se) {
        DmSensorData evdata = sensorEventToDmSensorData(se);
        fireEvent(FmSensor.EVENT, evdata);
    }

    private final static Map<Integer, Integer> accuracyConvTable = new HashMap<Integer, Integer>() {{
        put(SensorManager.SENSOR_STATUS_UNRELIABLE, FmSensor.SENSOR_STATUS_UNRELIABLE);
        put(SensorManager.SENSOR_STATUS_ACCURACY_LOW, FmSensor.SENSOR_STATUS_ACCURACY_LOW);
        put(SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM, FmSensor.SENSOR_STATUS_ACCURACY_MEDIUM);
        put(SensorManager.SENSOR_STATUS_ACCURACY_HIGH, FmSensor.SENSOR_STATUS_ACCURACY_HIGH);
    }};

    private Integer convAccuracy(Integer andracc) {
        return accuracyConvTable.get(andracc);        // same constants
    }

    private DmSensorData sensorEventToDmSensorData(SensorEvent se) {
        DmSensorData sdata = DmFactory.create(DmSensorData.class);
        sdata.setSensor(getSensorType());
        sdata.setTimestamp(se.timestamp/1000000L);
        sdata.setAccuracy(convAccuracy(se.accuracy));
        List<Double> vals = new ArrayList<Double>();
        double[] dVals = new double[se.values.length];
        for (int i = 0; i < se.values.length; i++) {
            dVals[i] = (double) se.values[i];
        }
        for (double f :  fixSensorEventRotation(filterSensorValues(dVals))) {
            vals.add(f);
        }
        sdata.setValues(vals);
        return sdata;
    }

    private String exToStr(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));

        String exceptionAsString = sw.toString();
        return exceptionAsString.length() > 128 ? exceptionAsString.substring(0,128) : exceptionAsString;
    }


    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
        currentAccuracy = convAccuracy(arg1);
    }

    private double[] fixSensorEventRotation(double[] values) {
        if (values.length < 2) return values;
        double vt;
        switch (display.getRotation() /* getOrientation() */) {
            case Surface.ROTATION_90:
                vt = values[1];
                values[1] = values[0];
                values[0] = -1D * vt;
                break;
            case Surface.ROTATION_180:
                values[0] *= -1D;
                values[1] *= -1D;
                break;
            case Surface.ROTATION_270:
                vt = values[1];
                values[1] = -1D * values[0];
                values[0] = vt;
                break;
        }

        return values;
    }

    private Toast lastInfo = null;

    private void info(String s) {
        if (lastInfo != null) lastInfo.cancel();
        lastInfo = Toast.makeText(ctx.getApplicationContext(), s, Toast.LENGTH_SHORT);
        lastInfo.show();

    }


}
