package org.fm.app.geoevent.clientandroid.impl;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;


import org.fm.FmDispatcher;
import org.fm.FmEventsDispatcher;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.dm.DmFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;



/**
 * Created by robertob on 5.2.2016..
 */


public class FmLocationDispatcher implements LocationListener, FmDispatcher {
    private static final String TAG = FmLocationDispatcher.class.getName();
    private static final int MAX_LOCATION_AGE = 120000;

    private final FmDispatcher dp = new FmEventsDispatcher(this);

    private final LocationManager locationManager;
    private Location bestLocation = null;
    private Location lastLocation = null;

    public FmLocationDispatcher(LocationManager locManager) {
        locationManager = locManager;
    }

    public DmGpsLocation getLastKnownLocation(String provider) {
        DmGpsLocation location = null;
        try {
            location = locationToDmGpsLocation(locationManager.getLastKnownLocation(provider));
        } catch(SecurityException e) {
            e.printStackTrace();
        }
        return location;
    }

    public DmGpsLocation getLastLocation() {
        return locationToDmGpsLocation(lastLocation);
    }

    public DmGpsLocation getBestLocation() {
        return locationToDmGpsLocation(bestLocation == null ? lastLocation : bestLocation);
    }


    public void requestUpdatesFromProvider(String provider,int minTime, int minDistance) {
        try {
            Log.e(TAG, "Requesting location updates from [" + provider + ", " + minDistance + "m, " + minTime + "ms]");
            if(locationManager.isProviderEnabled(provider)) {
                locationManager.requestLocationUpdates(provider, minTime, minDistance, this);
            } else {
                Log.e(TAG, "Error requesting location updates from [" + provider + ", " + minDistance + "m, " + minTime + "ms]. Provider is not enabled.");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void stopLocationUpdates() {
        try {
            Log.e(TAG, "Stopping location updates");
            locationManager.removeUpdates(this);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        boolean isBetter = isBetterLocation(location,bestLocation);
        DmGpsLocation dmLoc = locationToDmGpsLocation(location);

        lastLocation = location;
        if(isBetter) {
            bestLocation = location;
        }
        Log.e(TAG, "Location changed: " + dmLoc);
        fireEvent("onLocationChanged", dmLoc);
        if(isBetter) {
            Log.e(TAG, "Best location changed: " + dmLoc);
            fireEvent("onBestLocationChanged",dmLoc);
        }
    }

    @Override
    public void onStatusChanged(final String provider, final int status, final Bundle extras) {
        Log.e(TAG, "Status of privider changed: " + provider + "=" + status);
        fireEvent("onLocationProviderStatusChanged",new HashMap() {{
            put("provider",provider);
            put("status",status);
            put("extras",extras);
        }});
    }

    public boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > MAX_LOCATION_AGE;
        boolean isSignificantlyOlder = timeDelta < -MAX_LOCATION_AGE;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = location.getProvider() == null ?
                currentBestLocation.getProvider() == null :
                location.getProvider().equals(currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    private DmGpsLocation locationToDmGpsLocation(Location l) {
        if(l == null) return null;
        DmGpsLocation dm = DmFactory.create(DmGpsLocation.class);

        dm.setAccuracy(Double.valueOf(l.getAccuracy()));
        dm.setAltitude(Double.valueOf(l.getAltitude()));
        dm.setBearing(Double.valueOf(l.getBearing()));
        dm.setLatitude(Double.valueOf(l.getLatitude()));
        dm.setLongitude(Double.valueOf(l.getLongitude()));
        dm.setSpeed(Double.valueOf(l.getSpeed()));
        dm.setProvider(l.getProvider());
        dm.setTimestamp(new Date(l.getTime()));

        return dm;
    }

    @Override
    public void onProviderEnabled(String provider) {
        fireEvent("onLocationProviderEnabled",provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        fireEvent("onLocationProviderDisabled",provider);
    }

    @Override
    public <T> boolean addListener(T t) {
        return dp.addListener(t);
    }

    @Override
    public <T> void fireEvent(String s, T t) {
        dp.fireEvent(s,t);
    }

    @Override
    public void removeAllListeners() {
        dp.removeAllListeners();
    }

    @Override
    public <T> boolean removeListener(T t) {
        return dp.removeListener(t);
    }

    @Override
    public int getListenersCount() {
        return dp.getListenersCount();
    }

    @Override
    public <T> List<T> getListeners() {
        return dp.getListeners();
    }

    @Override
    public <T> boolean isListener(T t) {
        return dp.isListener(t);
    }

    @Override
    public String getID() {
        return dp.getID();
    }
}
