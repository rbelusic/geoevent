package org.fm.app.geoevent.clientandroid.impl;

import android.os.AsyncTask;
import android.os.Handler;

import org.fm.FmDispatcher;
import org.fm.app.geoevent.client.GeRestClient;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.http.client.impl.FmApacheHttpClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by robertob on 17.2.2016..
 */
public class FmTimerDispatcher implements FmDispatcher {
    private static final String TAG = FmTimerDispatcher.class.getName();
    private final FmDispatcher dp = new org.fm.FmEventsDispatcher(this);
    private long timerDelay;
    private long timerPeriod;
    Timer timer = null;
    TimerTask timerTask = null;
    final Handler handler = new Handler();

    public void startTimer(long delay, long period) {
        // after the first Xms the TimerTask will run every Yms
        timerDelay = delay;
        timerPeriod = period;
        resumeTimer();
    }

    public void resumeTimer() {
        stopTimer();
        initializeTimerTask();
        timer = new Timer();
        timer.schedule(timerTask, timerDelay, timerPeriod);
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if(timerTask != null) {
            timerTask.cancel();
        }
    }

    private void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        fireEvent("onTimer",System.currentTimeMillis());
                    }
                });
            }
        };
    }

    @Override
    public <T> boolean addListener(T t) {
        return dp.addListener(t);
    }

    @Override
    public <T> void fireEvent(String s, T t) {
        dp.fireEvent(s,t);
    }

    @Override
    public void removeAllListeners() {
        dp.removeAllListeners();
    }

    @Override
    public <T> boolean removeListener(T t) {
        return dp.removeListener(t);
    }

    @Override
    public int getListenersCount() {
        return dp.getListenersCount();
    }

    @Override
    public <T> List<T> getListeners() {
        return dp.getListeners();
    }

    @Override
    public <T> boolean isListener(T t) {
        return dp.isListener(t);
    }

    @Override
    public String getID() {
        return dp.getID();
    }

}
