package org.fm.app.geoevent.clientandroid.services.locationservice;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class LocationService extends Service {
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    private static final int MAX_LOCATION_AGE = 120000;
    private static final int MAX_HISTORY_SIZE = 50;

    private final List<LocationServiceListener> listeners = new ArrayList<>();
    private static final String TAG = LocationService.class.getName();

    private LocationManager mLocationManager = null;
    private Location bestLocation = null;
    private Location lastLocation = null;

    private final List<Location> locations = new ArrayList<Location>();
    public LocationService() {
    }

    private class LocationChangeListener implements LocationListener {
        private String provider;
        public LocationChangeListener(String mProvider) {
            provider = mProvider;
            Log.e(TAG, "LocationListener " + mProvider);
        }

        public String getProvider() {
            return provider;
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            addLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }
        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationChangeListener[] mLocationListeners = new LocationChangeListener[] {
            new LocationChangeListener(LocationManager.GPS_PROVIDER),
            new LocationChangeListener(LocationManager.NETWORK_PROVIDER)
    };


    public void addListener(LocationServiceListener l) {
        if(!listeners.contains(l)) listeners.add(l);
    }

    public void removeListener(LocationServiceListener l) {
        listeners.remove(l);
    }


    public void addLocation(Location l) {
        Log.e(TAG, "Location added: " + l);
        locations.add(l);
        lastLocation = l;
        for(LocationServiceListener listener: listeners) {
            listener.onLocation(l);
        }

        if(isBetterLocation(l,bestLocation)) {
            Log.e(TAG, "Best location changed: " + l);
            bestLocation = l;
            for(LocationServiceListener listener: listeners) {
                listener.onBestLocation(l);
            }
        }

        while(locations.size() > MAX_HISTORY_SIZE) {
            Log.e(TAG, "Old Location removed from log: " + locations.get(0));
            locations.remove(0);
        }
    }


    public Location getLastLocation() {
        return lastLocation;
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > MAX_LOCATION_AGE;
        boolean isSignificantlyOlder = timeDelta < -MAX_LOCATION_AGE;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = location.getProvider() == null ?
                currentBestLocation.getProvider() == null :
                location.getProvider().equals(currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }


    public Location getBestLocation() {
        return bestLocation;
    }


    @Override
    public IBinder onBind(Intent intent) {
        IBinder binder = new Binder() {
            public LocationService getService() {
                return LocationService.this;
            }
        };

        if(intent instanceof  LocationServiceListener) {
            addListener((LocationServiceListener)intent);
        }
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        if(intent instanceof  LocationServiceListener) {
            addListener((LocationServiceListener)intent);
        }
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        if (mLocationManager == null) {
            Log.e(TAG, "initializeLocationManager");
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }

        for(LocationChangeListener mLocListener: mLocationListeners) {
            try {
                mLocationManager.requestLocationUpdates(
                        mLocListener.getProvider(),
                        LOCATION_INTERVAL, LOCATION_DISTANCE,
                        mLocListener);
                addLocation(mLocationManager.getLastKnownLocation(mLocListener.getProvider()));
            } catch (java.lang.SecurityException ex) {
                Log.i(TAG, "fail to request location update, ignore", ex);
            } catch (IllegalArgumentException ex) {
                Log.d(TAG, "network provider does not exist, " + ex.getMessage());
            }
        }
    }


    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        for(LocationServiceListener listener: listeners) {
            listener.onQuit();
        }
        listeners.clear();

        super.onDestroy();

        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (SecurityException ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }
}
