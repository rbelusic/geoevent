package org.fm.app.geoevent.clientandroid;

import android.os.AsyncTask;

import org.fm.FmDispatcher;
import org.fm.FmException;
import org.fm.app.geoevent.client.GeRestClient;

import java.net.InetAddress;
import java.util.Map;

public class RestAsyncTask extends AsyncTask {
    private final FmDispatcher dp = new org.fm.FmEventsDispatcher(this);
    private Map<String, Object> httpArgs = null;
    private GeRestClient.Endpoint endpoint = null;
    private GeRestClient.EndpointConfig endpointcfg = null;
    private Exception lastException = null;
    private final GeRestClient restClient;


    public RestAsyncTask(Object lstnr, GeRestClient cli) {
        restClient = cli;
        dp.addListener(lstnr);
    }

    public boolean isInternetAvailable() {
        //if(!isNetworkConnected()) return false;

        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name

            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            return false;
        }

    }

    public GeRestClient getRestClient() {
        return restClient;
    }

    @Override
    public Object doInBackground(Object[] params) { // first param is ENDPOINT, second is Map<String, Object> restaParams
        if(!isInternetAvailable()) {
            return new FmException("Internet is not available. Check your internet connection");
        }
        endpoint = (GeRestClient.Endpoint) params[0];
        endpointcfg = endpoint != null ? GeRestClient.getEndpointConfig(endpoint) : null;
        httpArgs = params.length > 1 ? (Map<String, Object>) params[1] : null;

        if (endpointcfg != null) {
            if (endpointcfg.httpMethod.equalsIgnoreCase("GET")) {
                if (endpointcfg.returnsCollection) {
                    try {

                        return getRestClient().listGet(endpoint, endpointcfg.responseClass, httpArgs);
                    } catch (Exception e) {
                        return e;
                    }
                } else {
                    try {
                        return getRestClient().get(endpoint, endpointcfg.responseClass, httpArgs);
                    } catch (Exception e) {
                        return e;
                    }
                }
            } else if (endpointcfg.httpMethod.equalsIgnoreCase("POST")) {
                try {
                    return getRestClient().post(endpoint, endpointcfg.responseClass, httpArgs);
                } catch (Exception e) {
                    return e;
                }
            }
        }
        return new FmException("Unknown method");
    }

    @Override
    public void onPostExecute(Object response) {
        if(Exception.class.isAssignableFrom(response.getClass())) {
            dp.fireEvent("onRestError", new Object[]{endpoint, httpArgs,response});
        } else {
            dp.fireEvent("onRestResponse", new Object[]{endpoint, httpArgs, response});
        }
    }

}
