package org.fm.app.geoevent.clientandroid.activities;

import android.os.Bundle;
import android.support.library21.custom.SwipeRefreshLayoutBottom;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.fm.app.geoevent.apis.osm.dm.NomAddress;
import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.app.geoevent.apis.osm.dm.NomPolygon;
import org.fm.app.geoevent.client.GeClientSessionContext;
import org.fm.app.geoevent.clientandroid.impl.dao.DaoInternal;
import org.fm.app.geoevent.clientandroid.impl.dao.DaoInternalImpl;
import org.fm.app.geoevent.clientandroid.services.events.EventsServiceUtils;
import org.fm.app.geoevent.dm.DmException;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.DmSysInfo;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.app.geoevent.dm.event.DmLocation;
import org.fm.app.geoevent.dm.event.DmMessage;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;


public class FmActivity extends AppCompatActivity  {
    private EventsServiceUtils eventsServiceUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fmContextLoader();
        eventsServiceUtils = new EventsServiceUtils(this);
    }


    public GeClientSessionContext getGeSession() {
        return (GeClientSessionContext) FmContext.getSessionBean(FmSessionContext.class);
    }

    public EventsServiceUtils getEventsServiceUtils() {
        return eventsServiceUtils;
    }

    public void fmContextLoader() {
        try {
            FmApplication.instance(FmApplication.class,"GeoEvent Client");
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        FmContext.addSessionBean(FmSessionContext.class, new GeClientSessionContext());

        FmContext.addDmClassType(DmUser.class.getSimpleName(), DmUser.class);
        FmContext.addDmClassType(DmException.class.getSimpleName(), DmException.class);
        FmContext.addDmClassType(DmGpsLocation.class.getSimpleName(), DmGpsLocation.class);
        FmContext.addDmClassType(DmSession.class.getSimpleName(), DmSession.class);
        FmContext.addDmClassType(DmSysInfo.class.getSimpleName(), DmSysInfo.class);
        FmContext.addDmClassType(DmEvent.class.getSimpleName(),DmEvent.class);
        FmContext.addDmClassType(DmLocation.class.getSimpleName(),DmLocation.class);
        FmContext.addDmClassType(DmMessage.class.getSimpleName(), DmMessage.class);

        FmContext.addDmClassType(NomPlace.class.getSimpleName(), NomPlace.class);
        FmContext.addDmClassType(NomAddress.class.getSimpleName(), NomAddress.class);

        FmContext.addDmClassType(NomPolygon.class.getSimpleName(), NomPolygon.class);

        // dao
        FmApplication.instance().setDao(DaoInternal.class,new DaoInternalImpl());
    }


    private Toast lastInfo = null;
    public void showShortInfo(String s) {
        //if(true) return;
        if(lastInfo != null) lastInfo.cancel();
        lastInfo = Toast.makeText(getApplicationContext(),s, Toast.LENGTH_SHORT);
        lastInfo.show();

    }

    public void showLongInfo(String s) {
        //if(true) return;
        if(lastInfo != null) lastInfo.cancel();
        lastInfo = Toast.makeText(getApplicationContext(),
                s, Toast.LENGTH_LONG);
        lastInfo.show();
    }

}
