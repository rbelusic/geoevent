package org.fm.app.geoevent.clientandroid.impl;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;

import org.fm.FmDispatcher;
import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.app.geoevent.clientandroid.ParcelDm;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.app.geoevent.clientandroid.services.events.EventsService;
import org.fm.app.geoevent.clientandroid.services.events.EventsServiceUtils;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.app.geoevent.util.rs.GeObjectMapper;
import org.fm.application.FmContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class FmEventServiceMsgsDispatcher implements FmDispatcher {
    private final static Logger LOG = FmContext.getLogger(FmEventServiceMsgsDispatcher.class);

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    private final FmDispatcher dp = new org.fm.FmEventsDispatcher(this);
    private final Context context;
    /** Messenger for communicating with service. */
    Messenger mService = null;
    /** Flag indicating whether we have called bind on the service. */
    boolean mIsBound;
    /** Some text view we are using to show state information. */
    private Exception lastException = null;
    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  We are communicating with our
            // service through an IDL interface, so get a client-side
            // representation of that from the raw service object.
            mService = new Messenger(service);
            // We want to monitor the service for as long as we are
            // connected to it.
            try {
                Message msg = Message.obtain(null, EventsServiceUtils.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            } catch (RemoteException e) {
                lastException = e;
                fireEvent("onEventsFetchError",lastException);
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }

            // As part of the sample, tell the user what happened.
            LOG.info("Connected to Events service.");
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mService = null;

            // As part of the sample, tell the user what happened.
            LOG.info( "Events service is disconnected!");
        }
    };

    public FmEventServiceMsgsDispatcher(Context ctx) {
        context = ctx;
    }

    public void requestUpdate() {
        if(mService == null) return;
        Message msg = Message.obtain(null, EventsServiceUtils.MSG_REQUEST_UPDATE);
        msg.replyTo = mMessenger;
        try {
            mService.send(msg);
        } catch (RemoteException e) {
            lastException = e;
            fireEvent("onEventsFetchError", lastException);
        }
    }

    public void requestReloadConfiguration(DmConfiguration config) {
        if(mService == null) return;
        Message message = Message.obtain(null,EventsServiceUtils.MSG_REQUEST_RELOAD_CONFIG);
        Bundle responseData = new Bundle();
        HashMap<String, Object> map = ParcelDm.dmToMap(config, context.getClassLoader());
        responseData.putSerializable("configuration", map);
        message.setData(responseData);
        message.replyTo = mMessenger;
        try {
            mService.send(message);
        } catch (RemoteException e) {
            lastException = e;
            fireEvent("onEventsFetchError", lastException);
        }
    }

    public void pause() {
        if(mService == null) return;
        Message message = Message.obtain(null,EventsServiceUtils.MSG_PAUSE_CLIENT);
        message.replyTo = mMessenger;
        try {
            mService.send(message);
        } catch (RemoteException e) {
            lastException = e;
            fireEvent("onEventsFetchError", lastException);
        }
    }

    public void resume() {
        if(mService == null) return;
        Message message = Message.obtain(null,EventsServiceUtils.MSG_RESUME_CLIENT);
        message.replyTo = mMessenger;
        try {
            mService.send(message);
        } catch (RemoteException e) {
            lastException = e;
            fireEvent("onEventsFetchError", lastException);
        }
    }


    @Override
    public <T> boolean addListener(T t) {
        if(!mIsBound) {
            doBindService();
        }
        boolean resp = dp.addListener(t);
        if(mIsBound) fireEvent("onServiceConnected",this);
        return resp;
    }

    @Override
    public <T> void fireEvent(String s, T t) {
        dp.fireEvent(s,t);
    }

    @Override
    public void removeAllListeners() {
        dp.removeAllListeners();
        doUnbindService();
    }

    @Override
    public <T> boolean removeListener(T t) {
        boolean retc = dp.removeListener(t);
        if(dp.getListenersCount() == 0) {
            doUnbindService();
        }
        return retc;
    }

    @Override
    public int getListenersCount() {
        return dp.getListenersCount();
    }

    @Override
    public <T> List<T> getListeners() {
        return dp.getListeners();
    }

    @Override
    public <T> boolean isListener(T t) {
        return dp.isListener(t);
    }

    @Override
    public String getID() {
        return dp.getID();
    }

    private void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        mIsBound = context.bindService(new Intent(context, EventsService.class), mConnection, Context.BIND_AUTO_CREATE);

    }

    private void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, EventsServiceUtils.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    lastException = e;
                    fireEvent("onEventsFetchError", lastException);

                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }

            // Detach our existing connection.
            context.unbindService(mConnection);
            mIsBound = false;
        }
    }

    /**
     * Handler of incoming messages from service.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            try {
                Bundle data = msg.getData();
                if(data == null) return;

                HashMap<String, Object> map;
                switch (msg.what) {
                    case EventsServiceUtils.MSG_RESPONSE_CONFIG:
                        map = (HashMap<String, Object>) data.getSerializable("configuration");
                        if (map != null) {
                            try {
                                DmConfiguration dm = (DmConfiguration) ParcelDm.mapToDm(map, getClass().getClassLoader());
                                if(dm != null) fireEvent("onConfigurationChanged", dm);
                            } catch (Exception e) {
                                lastException = e;
                                fireEvent("onEventsFetchError", e);
                            }
                        }
                        break;
                    case EventsServiceUtils.MSG_RESPONSE_LOCATION:
                        map = (HashMap<String, Object>) data.getSerializable("location");
                        if (map != null) {
                            try {
                                DmGpsLocation loc = (DmGpsLocation) ParcelDm.mapToDm(map, getClass().getClassLoader());
                                if(loc != null) fireEvent("onBestLocationChanged", loc);
                            } catch (Exception e) {
                                lastException = e;
                                fireEvent("onEventsFetchError", e);
                            }
                        }
                        break;
                    case EventsServiceUtils.MSG_RESPONSE_ADDRESS:
                        map = (HashMap<String, Object>) data.getSerializable("place");
                        if (map != null) {
                            try {
                                NomPlace place = (NomPlace) ParcelDm.mapToDm(map, getClass().getClassLoader());
                                if(place != null) fireEvent("onAddressChangedChanged", place);
                            } catch (Exception e) {
                                lastException = e;
                                fireEvent("onEventsFetchError", e);
                            }
                        }
                        break;
                    case EventsServiceUtils.MSG_RESPONSE_EVENTS:
                        ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) data.getSerializable("events");
                        List<DmEvent> events = new ArrayList<>();
                        for(HashMap mmap: list) {
                            try {
                                DmEvent event = (DmEvent) ParcelDm.mapToDm(mmap, getClass().getClassLoader());
                                events.add(event);
                            } catch (Exception e) {
                                lastException = e;
                                fireEvent("onEventsFetchError", e);
                            }
                        }
                        if(events.size() > 0) fireEvent("onEventsReceived", events);
                        break;
                    default:
                        super.handleMessage(msg);
                }
            } catch(Exception ee) {
                lastException =ee;
                fireEvent("onEventsFetchError", lastException);
            }
        }
    }

}
