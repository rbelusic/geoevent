package org.fm.app.geoevent.clientandroid;

import org.fm.app.geoevent.application.GeSessionContext;
import org.fm.app.geoevent.clientandroid.impl.dao.DaoInternal;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmFactory;

import java.util.Date;

public class Config {
    public static final String DB_VER                               = "0";
    public static String SERVER_URL                                 = null;
    public static Float MIN_REFRESH_DISTANCE                        = 100F;
    public static Float EVENTS_FETCH_DISTANCE                       = 3000F;
    public static long MIN_REFRESH_TIME_MS                          = 300000;
    public static Float NOTIF_RADIUS                                = 0.5F; // km , temp dok nije u eventima samima
    public static Float NOTIF_RADIUS_DISMISS                        = 0.8F; // km , temp dok nije u eventima samima
    public static int MAX_CACHED_EVENTS_LIST_SIZE                   = 50;
    public static int MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT      = 100;

    private static  DmConfiguration currentConfig = null;

    private static String getServerUrl() {
        if(Config.SERVER_URL == null || Config.SERVER_URL.trim().equals("")) {
            //Config.SERVER_URL = FmApplication.instance().getBuildProperty("fm.app.geoevent.server.url");
            Config.SERVER_URL = FmApplication.instance().getConfigProperty("fm.app.geoevent.server.url");
        }
        return Config.SERVER_URL;
    }

    public static DmConfiguration getConfiguration() {
        if (currentConfig == null) {
            currentConfig = getWorkingConfiguration();
        }
        return currentConfig;
    }

    public static void setConfiguration(DmConfiguration cfg) {
        currentConfig = cfg == null ? getWorkingConfiguration() : cfg;
        updateWorkingConfiguration();
        if (((GeSessionContext) FmContext.getSessionBean(FmSessionContext.class)).getSessionInfo() != null) {
            ((GeSessionContext) FmContext.getSessionBean(FmSessionContext.class)).getSessionInfo().setId(currentConfig.getToken(""));
        }

    }

    public static DmConfiguration loadConfiguration() {
        return FmApplication.instance().getDao(DaoInternal.class).get(DmConfiguration.class, Config.DB_VER);
    }

    public static void saveConfiguration(DmConfiguration config) {
        config.setUpdated(new Date());
        FmApplication.instance().getDao(DaoInternal.class).update(new DmConfiguration[]{config}, null);
    }

    public static void updateWorkingConfiguration() {
        DmConfiguration cur = getConfiguration();
        Config.SERVER_URL = cur.getServerUrl(getServerUrl());
        Config.MIN_REFRESH_DISTANCE = cur.getMinRefreshDistance(Config.MIN_REFRESH_DISTANCE);
        Config.EVENTS_FETCH_DISTANCE = cur.getEventsFetchDistance(Config.EVENTS_FETCH_DISTANCE);
        Config.MIN_REFRESH_TIME_MS = cur.getMinRefreshTimeMs((int) Config.MIN_REFRESH_TIME_MS);
        Config.NOTIF_RADIUS = cur.getNotifcationRadius(Config.NOTIF_RADIUS);
        Config.NOTIF_RADIUS_DISMISS = cur.getNotifcationRadiusDismiss(Config.NOTIF_RADIUS_DISMISS);
        Config.MAX_CACHED_EVENTS_LIST_SIZE = cur.getEventsListSize(Config.MAX_CACHED_EVENTS_LIST_SIZE);
        Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT = cur.getEventsListSizeImportant(Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT);
    }

    public static DmConfiguration getWorkingConfiguration() {
        DmConfiguration config = DmFactory.create(DmConfiguration.class);
        config.setId(Config.DB_VER);
        config.setCreated(new Date());
        config.setUpdated(config.getCreated(null));
        config.setServerUrl(getServerUrl());
        config.setUsername("");
        config.setToken("");
        config.setEventsFetchDistance(Config.EVENTS_FETCH_DISTANCE);
        config.setEventsListSize(Config.MAX_CACHED_EVENTS_LIST_SIZE);
        config.setEventsListSizeImportant(Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT);
        config.setEventsFetchDistance(Config.EVENTS_FETCH_DISTANCE);
        config.setMinRefreshDistance(Config.MIN_REFRESH_DISTANCE);
        config.setMinRefreshTimeMs((int) Config.MIN_REFRESH_TIME_MS);
        config.setNotifcationRadius(Config.NOTIF_RADIUS);
        config.setNotifcationRadiusDismiss(Config.NOTIF_RADIUS_DISMISS);
        config.setUpdated(new Date());
        return config;
    }
}
