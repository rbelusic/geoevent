package org.fm.app.geoevent.clientandroid.impl;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;

import org.fm.FmDispatcher;
import org.fm.FmEventsDispatcher;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.application.FmContext;
import org.fm.dm.DmFactory;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;


public class FmGoogleServicesLocationDispatcher  implements
        FmDispatcher, LocationListener,
        GoogleApiClient.ConnectionCallbacks,  GoogleApiClient.OnConnectionFailedListener
{
    private final static Logger LOG = FmContext.getLogger(FmGoogleServicesLocationDispatcher.class);
    public static final int MAX_LOCATION_AGE = 60000;
    public static final int TOLERATED_LOCATION_AGE = 10000;

    private final FmDispatcher dp = new FmEventsDispatcher(this);
    private final Context context;
    Location mCurrentLocation;
    String mLastUpdateTime;
    private GoogleApiClient googleApiClient;
    private Location bestLocation = null;
    private Location lastLocation = null;
    private long updInterval = 10000L;
    private long updFfastestInterval = 5000L;
    private int updPpriority = LocationRequest.PRIORITY_HIGH_ACCURACY;

    public FmGoogleServicesLocationDispatcher(Context ctx) {
        context = ctx;
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }

    public DmGpsLocation getLastKnownLocation() {
        DmGpsLocation location = null;
        try {
            if(googleApiClient.isConnected()) location = locationToDmGpsLocation(LocationServices.FusedLocationApi.getLastLocation(googleApiClient));
        } catch(SecurityException e) {
            e.printStackTrace();
        }
        return location;
    }

    public DmGpsLocation getLastLocation() {
        return locationToDmGpsLocation(lastLocation);
    }

    public DmGpsLocation getBestLocation() {
        return locationToDmGpsLocation(bestLocation == null ? lastLocation : bestLocation);
    }


    public void requestUpdates(long interval,long fastestInterval,int priority) {
        LOG.info("Connecting to Google API client");
        updInterval = interval;
        updFfastestInterval = fastestInterval;
        updPpriority = priority;
        if(googleApiClient.isConnected()) {
            stopLocationUpdates();
        }
        googleApiClient.connect(); // biti ce callback na onConnected
    }

    public void stopLocationUpdates() {

        googleApiClient.disconnect();
    }

    @Override
    public void onLocationChanged(Location location) {
        boolean isBetter = isBetterLocation(location,bestLocation);
        DmGpsLocation dmLoc = locationToDmGpsLocation(location);

        lastLocation = location;
        if(isBetter) {
            bestLocation = location;
        }
        LOG.info( "Location changed: " + dmLoc);
        fireEvent("onLocationChanged", dmLoc);
        if(isBetter) {
            LOG.info("Best location changed: " + dmLoc);
            fireEvent("onBestLocationChanged",dmLoc);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        LOG.info("Connected to Google API client. Requesting updates (interval=" + updInterval + ", fastInterval=" + updFfastestInterval + ", priority=" + updPpriority + ") ...");
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient,
                createLocationRequest(updInterval,updFfastestInterval,updPpriority),
                (LocationListener) this
        );
    }

    @Override
    public void onConnectionSuspended(int i) {
        LOG.warning("Connection to Google services is suspended.");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        LOG.severe("Connection to Google services failed!");
    }


    public boolean isConnected() {
        return googleApiClient != null && googleApiClient.isConnected();
    }
    public boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > MAX_LOCATION_AGE;
        boolean isSignificantlyOlder = timeDelta < -MAX_LOCATION_AGE;
        boolean isNewer = timeDelta > TOLERATED_LOCATION_AGE;
        boolean isOlder = timeDelta < -TOLERATED_LOCATION_AGE;

        // Check whether the new location fix is more or less accurate
        float accuracyDelta = (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = false;
        boolean isMoreAccurate = false;
        boolean isSignificantlyLessAccurate = false;
        if(location.hasAccuracy() && !lastLocation.hasAccuracy()) {
            isMoreAccurate = true;
            isLessAccurate = false;
            isSignificantlyLessAccurate = false;
        } else if(!location.hasAccuracy() && lastLocation.hasAccuracy()) {
            isMoreAccurate = false;
            isLessAccurate = true;
            isSignificantlyLessAccurate = true;
        } else if(!location.hasAccuracy() && !lastLocation.hasAccuracy()) {
            isMoreAccurate = false;
            isLessAccurate = false;
            isSignificantlyLessAccurate = false;
        } else { // both locations hawe accs
            isMoreAccurate = accuracyDelta < 0f;
            isLessAccurate = accuracyDelta > 0f;
            isSignificantlyLessAccurate = accuracyDelta > 100f;
        }

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer && !isSignificantlyLessAccurate) {
            LOG.severe("Location is better because is significantly newer (" + timeDelta + ").");
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            LOG.severe("Location is is NOT better because is significantly older (" + timeDelta + ").");
            return false;
        }

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = !"fused".equals(location.getProvider()) && (
                location.getProvider() == null ?
                currentBestLocation.getProvider() == null :
                location.getProvider().equals(currentBestLocation.getProvider())
            );

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate && !isOlder) {
            LOG.severe("Location is better because is more accurate and is not older (" + accuracyDelta + "m).");
            return true;
        } else if (!isLessAccurate && isNewer) {
            LOG.severe("Location is better because is newer (" + timeDelta + "ms), even if is not more accurate (" + accuracyDelta + "m).");
            return true;
        } else if (!isSignificantlyLessAccurate && isNewer && isFromSameProvider) {
            LOG.severe("Location is better because is newer (" + timeDelta + "ms), even if is less accurate (" +
                    accuracyDelta + "m) because is from same provider (" + location.getProvider() + ").");
            return true;
        }

        LOG.severe("Location is NOT better (accdiff=" + accuracyDelta + "m, tsdiff=" + timeDelta + "ms).");
        return false;
    }

    private DmGpsLocation locationToDmGpsLocation(Location l) {
        if(l == null) return null;
        DmGpsLocation dm = DmFactory.create(DmGpsLocation.class);

        dm.setAccuracy(Double.valueOf(l.getAccuracy()));
        dm.setAltitude(Double.valueOf(l.getAltitude()));
        dm.setBearing(Double.valueOf(l.getBearing()));
        dm.setLatitude(Double.valueOf(l.getLatitude()));
        dm.setLongitude(Double.valueOf(l.getLongitude()));
        dm.setSpeed(Double.valueOf(l.getSpeed()));
        dm.setProvider(l.getProvider());
        dm.setTimestamp(new Date(l.getTime()));

        return dm;
    }

    private LocationRequest createLocationRequest(long interval,long fastestInterval,int priority) {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(interval);
        mLocationRequest.setFastestInterval(fastestInterval);
        mLocationRequest.setPriority(priority);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        return mLocationRequest;
    }


    // -- event listener --
    @Override
    public <T> boolean addListener(T t) {
        return dp.addListener(t);
    }

    @Override
    public <T> void fireEvent(String s, T t) {
        dp.fireEvent(s,t);
    }

    @Override
    public void removeAllListeners() {
        dp.removeAllListeners();
    }

    @Override
    public <T> boolean removeListener(T t) {
        return dp.removeListener(t);
    }

    @Override
    public int getListenersCount() {
        return dp.getListenersCount();
    }

    @Override
    public <T> List<T> getListeners() {
        return dp.getListeners();
    }

    @Override
    public <T> boolean isListener(T t) {
        return dp.isListener(t);
    }

    @Override
    public String getID() {
        return dp.getID();
    }

}
