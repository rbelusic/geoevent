package org.fm.app.geoevent.clientandroid.impl;

import org.fm.FmDispatcher;
import org.fm.FmEventsDispatcher;
import org.fm.app.geoevent.client.GeRestClient;
import org.fm.app.geoevent.clientandroid.RestAsyncTask;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.http.client.impl.FmApacheHttpClient;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FmLocalEventsDispatcher implements FmDispatcher {
    private static final String TAG = FmLocalEventsDispatcher.class.getName();
    private final FmDispatcher dp = new FmEventsDispatcher(this);
    private String serverUrl;
    private GeRestClient cli = null;


    public FmLocalEventsDispatcher(String url) { // "http://bomeku.net:8080/GeoEvent/api/"
        serverUrl = url;
    }

    public void fetch(final DmGpsLocation loc, final Double distance) {

        new RestAsyncTask(this, new GeRestClient(new FmApacheHttpClient(), serverUrl))
                .execute(GeRestClient.Endpoint.EVENTS_FIND, new HashMap<String, Object>() {{
                    put("lat", loc.getLatitude(0D));
                    put("lon", loc.getLongitude(0D));
                    this.put("distance", distance);
                    this.put("tags", new ArrayList<String>());
                    this.put("nrows", 100);
                }});
    }

    private GeRestClient getClient() {
        if(cli == null) {
            cli = new GeRestClient(new FmApacheHttpClient(), serverUrl);

        }
        return cli;
    }

    public void onRestResponse(Object sender, Object data) {
        try {
            fireEvent("onEventsReceived", ((Object[]) data)[2]);
        } catch(Exception e) {}
    }

    public void onRestError(Object sender, Object data) {
        try {
            fireEvent("onEventsFetchError", ((Object[]) data)[2]);
        } catch(Exception e) {}
    }


    @Override
    public <T> boolean addListener(T t) {
        return dp.addListener(t);
    }

    @Override
    public <T> void fireEvent(String s, T t) {
        dp.fireEvent(s,t);
    }

    @Override
    public void removeAllListeners() {
        dp.removeAllListeners();
    }

    @Override
    public <T> boolean removeListener(T t) {
        return dp.removeListener(t);
    }

    @Override
    public int getListenersCount() {
        return dp.getListenersCount();
    }

    @Override
    public <T> List<T> getListeners() {
        return dp.getListeners();
    }

    @Override
    public <T> boolean isListener(T t) {
        return dp.isListener(t);
    }

    @Override
    public String getID() {
        return dp.getID();
    }
}
