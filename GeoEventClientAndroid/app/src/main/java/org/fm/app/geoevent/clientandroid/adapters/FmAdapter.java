package org.fm.app.geoevent.clientandroid.adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.fm.dm.DmAttribute;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;
import org.fm.utils.StringUtil;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class FmAdapter<T extends DmObject> extends ArrayAdapter {
    public final static DecimalFormat formatter6 = new DecimalFormat("#0.000000");
    public final static DecimalFormat formatter4 = new DecimalFormat("#0.0000");
    public final static DecimalFormat formatter2 = new DecimalFormat("#0.00");
    //public final static DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
    //public final static DateFormat TIME_FORMAT_HHMM = new SimpleDateFormat("HH:mm");


    private final Context aContext;
    private final List<T> values;
    private final int layoutId;

    public FmAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
        aContext = context;
        values = objects;
        layoutId = resource;
    }

    public Context actx() {
        return aContext;
    }

    public void refreshList(List<T> objects) {
        clear();
        addAll(objects);
        notifyDataSetChanged();
    }

    public List<T> getValues() {
        return values;
    }

    public View getRowView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) actx()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(getLayoutId(), parent, false);
        }
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        T dm = getValues().get(position);
        View rowView = getRowView(position, convertView, parent);
        updateViewFromAttr(null, dm, rowView);
        return rowView;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public <T extends DmObject> String formatValue(T dm, String attr) {
        Object value = dm.getAttr(attr, null);
        if(value == null) return "";

        if(value instanceof Double) {
            return formatter6.format((Double) value);
        } else if(value instanceof Float) {
            return formatter6.format((Float) value);
        } else if(value instanceof Date) {
            return DateUtils.formatDateTime(getContext(), ((Date)value).getTime(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE);
        } else {
            return value.toString();
        }
    }



    public void updateViewFromAttr(String prefix, DmObject dm, View rowView) {
        for (String dmattr : dm.getAttributeNames()) {
            String attr = prefix == null ? dmattr : (prefix  + "." + dmattr);
            View view = rowView.findViewWithTag(attr.toLowerCase());
            if (view == null && dm.getAttr(dmattr,null) != null) {
                DmAttribute meta = DmFactory.getDmAttributeMetadata(dm.getSubClass(), dmattr);
                if (DmObject.class.isAssignableFrom(meta.getType())) {
                    updateViewFromAttr(attr, (DmObject) dm.getAttr(dmattr,null), rowView);
                }
            } else if (view instanceof TextView) {
                String valStr = formatValue(dm, dmattr);
                ((TextView) view).setText(valStr);
            }
        }
    }
}
