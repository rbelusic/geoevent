package org.fm.app.geoevent.clientandroid.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;


import org.fm.FmEventsDispatcher;
import org.fm.app.geoevent.clientandroid.R;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.dm.DmObject;
import org.fm.utils.TimeUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class EventsAdapter extends FmAdapter<DmEvent>  {
    private FmEventsDispatcher dispatcher = new FmEventsDispatcher(this);

    public EventsAdapter(Context context, List<DmEvent> values) {
        super(context, R.layout.event_layout, values);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = super.getView(position, convertView, parent);


        //DmEvent dmEv = getValues().get(position);
        // Change the icon
        /*
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        if(imageView != null) imageView.setImageResource("gps".equals(dmEv.getProvider("err")) ? R.drawable.odd : R.drawable.even);
        */
        return rowView;
    }

    @Override
    public String formatValue(DmObject dm, String attr) {
        if(attr.equalsIgnoreCase("distance")) {
            return formatter2.format((Double) dm.getAttr(attr,0D));
        }
        if(attr.equalsIgnoreCase("timestamp")) {
            Date d = dm.getAttr(attr,null);
            return TimeUtil.strTimeBetween(d,new Date());
        }
        return super.formatValue(dm, attr);
    }

}
