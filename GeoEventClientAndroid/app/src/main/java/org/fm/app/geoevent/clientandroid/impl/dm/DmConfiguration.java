package org.fm.app.geoevent.clientandroid.impl.dm;

import org.fm.dm.DmObject;
import org.fm.dm.annotations.DmDataIdAttribute;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * Created by RobertoB on 7.3.2016..
 */
@Table(name = "configuration")
@DmDataIdAttribute(name = "id")
public interface DmConfiguration extends DmObject {
    public String getId(String def);
    public void setId(String def);

    @Column(name ="server_url")
    public String getServerUrl(String string);

    @Column(name ="server_url")
    public void setServerUrl(String string);

    public String getToken(String string);
    public void setToken(String string);

    public String getUsername(String string);
    public void setUsername(String string);

    public Date getCreated(Date date);
    public void setCreated(Date date);

    public Date getUpdated(Date date);
    public void setUpdated(Date date);

    @Column(name ="min_refresh_distance")
    public Float getMinRefreshDistance(Float v);
    @Column(name ="min_refresh_distance")
    public void setMinRefreshDistance(Float v);

    @Column(name ="events_fetch_distance")
    public Float getEventsFetchDistance(Float v);
    @Column(name ="events_fetch_distance")
    public void setEventsFetchDistance(Float v);

    @Column(name ="min_refresh_time_ms")
    public Integer getMinRefreshTimeMs(Integer v);
    @Column(name ="min_refresh_time_ms")
    public void setMinRefreshTimeMs(Integer v);

    @Column(name ="notification_radius")
    public Float getNotifcationRadius(Float v);
    @Column(name ="notification_radius")
    public void setNotifcationRadius(Float v);


    @Column(name ="notification_radius_dismiss")
    public Float getNotifcationRadiusDismiss(Float v);
    @Column(name ="notification_radius_dismiss")
    public void setNotifcationRadiusDismiss(Float v);

    @Column(name ="max_cached_events_list_size")
    public Integer getEventsListSize(Integer v);
    @Column(name ="max_cached_events_list_size")
    public void setEventsListSize(Integer v);

    @Column(name ="max_cached_events_list_size_imp")
    public Integer getEventsListSizeImportant(Integer v);
    @Column(name ="max_cached_events_list_size_imp")
    public void setEventsListSizeImportant(Integer v);
}
