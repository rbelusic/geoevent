package org.fm.app.geoevent.clientandroid.impl.dao;

import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.dao.DaoObjectImpl;
import org.fm.dm.DmObject;

import java.util.HashMap;
import java.util.List;


public class DaoInternalImpl extends DaoObjectImpl implements DaoInternal {

    public  List<DmGpsLocation> getRecentGpsLocations(final int fromrow, final int nrows) {
        return this.store().find(DmGpsLocation.class,"dao-select-recent-gpslocations.sql", new HashMap() {{
            put("fromrow",fromrow+1);
            put("nrows",nrows);
        }});
    }

}
