package org.fm.app.geoevent.clientandroid.services.events;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationRequest;

import org.fm.FmException;
import org.fm.app.geoevent.apis.osm.dm.NomAddress;
import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.app.geoevent.apis.osm.dm.NomPolygon;
import org.fm.app.geoevent.client.GeClientSessionContext;
import org.fm.app.geoevent.client.GeRestClient;
import org.fm.app.geoevent.client.GeUtils;
import org.fm.app.geoevent.clientandroid.Config;
import org.fm.app.geoevent.clientandroid.ParcelDm;
import org.fm.app.geoevent.clientandroid.impl.FmLocalEventsDispatcher;
import org.fm.app.geoevent.clientandroid.impl.FmGoogleServicesLocationDispatcher;
import org.fm.app.geoevent.clientandroid.impl.FmLocationDispatcher;
import org.fm.app.geoevent.clientandroid.impl.FmTimerDispatcher;
import org.fm.app.geoevent.clientandroid.impl.dao.DaoInternal;
import org.fm.app.geoevent.clientandroid.impl.dao.DaoInternalImpl;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.app.geoevent.clientandroid.impl.sensors.FmGoogleServicesActivityDispatcher;
import org.fm.app.geoevent.dm.DmException;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.DmSysInfo;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.app.geoevent.dm.event.DmLocation;
import org.fm.app.geoevent.dm.event.DmMessage;
import org.fm.app.geoevent.util.GeoUtil;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventsService extends Service {
    private final static Logger LOG = FmContext.getLogger(EventsService.class);
    private long serverStartTs = 0;
    private final Messenger mMessenger = new Messenger(new IncomingHandler());
    private final ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    private final Map<Messenger, Boolean> mClientsPused = new HashMap<Messenger, Boolean>();

    // events
    private final List<DmEvent> listOfEvents = new ArrayList<>();

    // location
    private DmGpsLocation lastLocation = null;
    private DmGpsLocation lastFetchLocation = null;
    private DmGpsLocation fetchInProgressLocation = null;
    private long lastFetchTs = 0;

    // user activity
    private Integer detectedUserActivity = null;
    private Integer oldUserActivity = null;
    private long detectedUserActivityTs = 0;
    private WorkingMode currentWorkingMode = WorkingMode.FAST;

    // address
    private NomPlace lastPlace = null;

    // etc
    private EventsServiceUtils eventsServiceUtils;

    // dispatchers
    private FmLocalEventsDispatcher evdispatcher = null;
    private FmLocationDispatcher gpsdispatcher = null;
    private FmGoogleServicesLocationDispatcher gsdispatcher = null;
    private FmGoogleServicesActivityDispatcher actdispatcher = null;
    private FmTimerDispatcher timerdispatcher = null;


    public EventsService() {
        super();
        LOG.log(Level.INFO, "Service new instance");
    }

    @Override
    public void onCreate() {
        LOG.log(Level.INFO, "Service onCreate()");
        //android.os.Debug.waitForDebugger();  // wait for debuger
        super.onCreate();
        // init
        init();

    }

    @Override
    public void onDestroy() {
        LOG.log(Level.INFO, "Service onDestroy()");
        serverStartTs = 0;
        if(timerdispatcher != null) {
            timerdispatcher.stopTimer();
            timerdispatcher.removeAllListeners();
        }
        if(gsdispatcher != null) {
            gsdispatcher.removeAllListeners();
            gsdispatcher.stopLocationUpdates();
        }
        if(gpsdispatcher != null) {
            gpsdispatcher.removeAllListeners();
            gsdispatcher.stopLocationUpdates();
        }
        if(evdispatcher != null) {
            evdispatcher.removeAllListeners();
        }

        if(actdispatcher != null) {
            actdispatcher.removeAllListeners();
            actdispatcher.stopUpdates();
        }
        stopForeground(true);
        eventsServiceUtils.showServiceNotificationStop();
        super.onDestroy();
    }

    public void showLongInfo(String s) {
        //if(true) return;
        Toast.makeText(getApplicationContext(),
                s, Toast.LENGTH_LONG).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LOG.log(Level.INFO, "Service onStartCommand()");
        super.onStartCommand(intent, flags, startId);

        String lastServerStatus = eventsServiceUtils.getPreference("servicemode",EventsServiceUtils.SERVICE_STATUS.FOREGROUND.toString());
        String action = intent != null && intent.getAction() != null ? intent.getAction() : "";

        EventsServiceUtils.SERVICE_STATUS newStatus =
            action.isEmpty() ? (
                lastServerStatus.equals(EventsServiceUtils.SERVICE_STATUS.FOREGROUND.toString()) ?
                EventsServiceUtils.SERVICE_STATUS.FOREGROUND : EventsServiceUtils.SERVICE_STATUS.ON_DEMAND
            ) : (
                action.equals(EventsServiceUtils.NOTIF_START_FG_ACTION) ?
                EventsServiceUtils.SERVICE_STATUS.FOREGROUND : EventsServiceUtils.SERVICE_STATUS.ON_DEMAND
            )
        ;

        //showLongInfo("On start service => " + action + "/" + lastServerStatus  + "/" +newStatus);

        if(serverStartTs == 0) serverStartTs = System.currentTimeMillis();
        if(eventsServiceUtils.setServiceStatus(this, newStatus)) {
            eventsServiceUtils.updateServiceNotification(lastLocation, detectedUserActivity, serverStartTs);
            LOG.log(Level.INFO, "Service mode changed (" + lastServerStatus + " -> " + newStatus + ").");
        } else {
            LOG.log(Level.INFO, "Service mode is not changed.");
        }


        LOG.log(Level.INFO, "Service started in " + newStatus + " mode.");
        return newStatus.equals(EventsServiceUtils.SERVICE_STATUS.FOREGROUND) ? START_STICKY : START_NOT_STICKY;
    }

    /**
     * When binding to the service, we return an interface to our messenger
     * for sending messages to the service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        LOG.log(Level.INFO, "Service onBind()");
        return mMessenger.getBinder();
    }

    // -- priv -------------------------------------------------------------------------------------

    private void init() {
        LOG.log(Level.INFO, "Service init ...");

        // -- main --
        fmContextLoader();

        //check db

        eventsServiceUtils = new EventsServiceUtils(this);
        if (!eventsServiceUtils.isDbCreated()) {
            eventsServiceUtils.createDatabase();
        }

        Config.setConfiguration(Config.loadConfiguration());

        // kreiraj loc dispatcher
        if (eventsServiceUtils.isGooglePlayServicesAvailable()) {
            LOG.log(Level.INFO, "Google play services are available");
            gsdispatcher = new FmGoogleServicesLocationDispatcher(this);
            if (listOfEvents.size() < 1) {
                lastLocation = gsdispatcher.getLastKnownLocation();
            }
            gsdispatcher.addListener(this);
            gsdispatcher.requestUpdates(currentWorkingMode.getGpsInterval(), 3000, currentWorkingMode.getGpsPriority());

            actdispatcher = new FmGoogleServicesActivityDispatcher(this);
            actdispatcher.addListener(this);
            actdispatcher.requestUpdates();
        } else {
            LOG.log(Level.INFO, "Google play services are not available!");
            gpsdispatcher = new FmLocationDispatcher(
                    (LocationManager) getSystemService(Context.LOCATION_SERVICE));
            if (listOfEvents.size() < 1) {
                lastLocation = gpsdispatcher.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                refreshAddress(true);
            }
            gpsdispatcher.addListener(this);
            gpsdispatcher.requestUpdatesFromProvider(LocationManager.NETWORK_PROVIDER, 10000, 10);
            gpsdispatcher.requestUpdatesFromProvider(LocationManager.GPS_PROVIDER, 10000, 10);
        }

        // kreiraj events dispatcher
        evdispatcher = new FmLocalEventsDispatcher(Config.SERVER_URL);
        evdispatcher.addListener(this);

        // kreiraj update sheduler
        timerdispatcher = new FmTimerDispatcher();
        timerdispatcher.addListener(this);
        timerdispatcher.startTimer(10000, currentWorkingMode.getEventsRefreshPeriod());


        // done
        LOG.log(Level.INFO, "Service init done.");
    }

    private boolean refreshAddress(boolean forced) {
        if(lastLocation == null) {
            LOG.log(Level.INFO, "Fetch address aborted, gps location is unqnown.");
            return false;
        }

        if((
            lastLocation.getInsAttr("address",null) == null &&
            System.currentTimeMillis() - lastLocation.getTimestamp(null).getTime() > 5000L
        ) || forced) {
            LOG.log(Level.INFO, "Fetch address ...");
            lastLocation.setInsAttr("address", null);
            eventsServiceUtils.getAddress(this, lastLocation);
            return true;
        }
        return false;
    }

    private boolean refreshEventsList(boolean forced) {
        LOG.log(Level.INFO, "Refreshing events list ...");
        if(lastLocation == null)  {
            LOG.log(Level.INFO, "Location is still unqnown, aborting.");
            return false;
        }

        if(forced || checkRefreshConstraints()) {
            if(fetchInProgressLocation != null) {
                if(lastLocation.getID().equals(fetchInProgressLocation.getID())) {
                    LOG.log(Level.INFO, "Fetching data for this location is currently in progress! Aborting requestUpdate.");
                } else {
                    LOG.log(Level.INFO, "Fetching data from server is in progress, aborting & postponning refresh for current location.");
                    lastLocation.setInsAttr("refreshRequired", Boolean.TRUE);
                }
                return false;
            }


            fetchInProgressLocation = lastLocation;
            LOG.log(Level.INFO, "Fetching data from server for " + fetchInProgressLocation.toString() + " ...");
            evdispatcher.fetch(fetchInProgressLocation, Config.EVENTS_FETCH_DISTANCE.doubleValue());
            return true;
        }

        LOG.log(Level.INFO, "Refreshing events list is not required.");
        return false;
    }

    enum WorkingMode {
        STILL(new int[]{DetectedActivity.STILL}
                ,3000L,30000L, LocationRequest.PRIORITY_LOW_POWER),
        SLOW(new int[]{DetectedActivity.UNKNOWN, DetectedActivity.TILTING, DetectedActivity.ON_FOOT, DetectedActivity.WALKING}
                ,3000L,20000L, LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY),
        MOWING(new int[]{DetectedActivity.RUNNING, DetectedActivity.ON_BICYCLE}
                ,3000L,10000L, LocationRequest.PRIORITY_HIGH_ACCURACY),
        FAST(new int[]{DetectedActivity.IN_VEHICLE}
                ,3000L, 5000L, LocationRequest.PRIORITY_HIGH_ACCURACY)
        ;

        private static WorkingMode DEFAULT = SLOW;

        private final int [] myactivities;
        private long eventsRefreshPeriod;
        private int gpsPriority;
        private long gpsInterval;

        WorkingMode(int [] activities, long evRefreshPeriod,long interval, int priority) {
            myactivities = activities;
            eventsRefreshPeriod = evRefreshPeriod;
            gpsInterval = interval;
            gpsPriority = priority;
        }

        public static WorkingMode find(int activity) {
            for(WorkingMode wm: WorkingMode.values()) {
                if(wm.hasActivity(activity)) {
                    return wm;
                }
            }
        return WorkingMode.DEFAULT;
        }

        private boolean hasActivity(int activity) {
            for(int act: myactivities) {
                if(act == activity) return true;
            }
            return false;
        }

        public long getEventsRefreshPeriod() {
            return eventsRefreshPeriod;
        }

        public int getGpsPriority() {
            return gpsPriority;
        }

        public long getGpsInterval() {
            return gpsInterval;
        }
    }

    private boolean chooseWorkingMode() {
        if((gsdispatcher != null && !gsdispatcher.isConnected()) || detectedUserActivity == null) {
            LOG.log(Level.INFO, "WorkingMode not chosen, location dispatcher is not connected yet.");
            return(false); // stay on fast track before first location
        }
        long timeDiff = System.currentTimeMillis() - detectedUserActivityTs;
        if(timeDiff < 5000L) {
            LOG.log(Level.INFO, "WorkingMode not chosen, current activity is not enough old (" + timeDiff/1000L + "s),");
            return false;
        }
        WorkingMode newWorkingMode = WorkingMode.find(detectedUserActivity);

        if(!newWorkingMode.equals(currentWorkingMode)) {
            LOG.log(Level.INFO, "Choosen WorkingMode is " + newWorkingMode + ", switching from " + currentWorkingMode + " ...");
            setLevelOfGpsActivity(newWorkingMode);
            setEventsRefreshTimeouts(newWorkingMode);
            currentWorkingMode = newWorkingMode;
            LOG.log(Level.INFO, "Current WorkingMode i snow  " + currentWorkingMode + ".");
        } else {
            LOG.log(Level.INFO, "Choosen WorkingMode is same as old mode (" + newWorkingMode + "), nothing to do.");
        }
        return true;
    }

    private void setEventsRefreshTimeouts(WorkingMode newWorkingMode) {
        if (currentWorkingMode.getEventsRefreshPeriod() != newWorkingMode.getEventsRefreshPeriod()) {
            timerdispatcher.stopTimer();
            timerdispatcher.startTimer(5000, newWorkingMode.getEventsRefreshPeriod());
        }
    }

    private void setLevelOfGpsActivity(WorkingMode newWorkingMode) {
        if (
                currentWorkingMode.getGpsInterval() != newWorkingMode.getGpsInterval() ||
                currentWorkingMode.getGpsPriority() != newWorkingMode.getGpsPriority()
        ) {
            LOG.log(Level.INFO, "Changing level of GPS activity.");
            if(gsdispatcher != null) {
                gsdispatcher.stopLocationUpdates();
                int priority = newWorkingMode.getGpsPriority();
                if(
                    priority != LocationRequest.PRIORITY_HIGH_ACCURACY &&
                    (
                            lastLocation == null ||
                            lastLocation.getAccuracy(999D) > 100D ||
                            System.currentTimeMillis() - lastLocation.getTimestamp(null).getTime() > gsdispatcher.MAX_LOCATION_AGE
                    )
                ) {
                    priority = LocationRequest.PRIORITY_HIGH_ACCURACY;
                }

                gsdispatcher.requestUpdates(newWorkingMode.getGpsInterval(),3000,priority);
            }
        } else {
            LOG.log(Level.INFO, "There is no need to change level of GPS activity.");
        }
    }

    private void checkTimeoutsAndTimeTriggers() {
        if (!detectedActivityUpdated) onActivityChanged(this,detectedUserActivity);
        refreshAddress(false);
        refreshEventsList(false);
    }

    private boolean checkRefreshConstraints() {
        if(lastLocation == null) {
            LOG.log(Level.INFO, "Refreshing events is NOT RQUIRED because location is null.");
            return false;
        }
        if(lastFetchLocation == null) {
            LOG.log(Level.INFO, "Refreshing events is RQUIRED because location is null.");
            return true;
        }

        long curtime = System.currentTimeMillis();
        long timeDiff = curtime - lastFetchTs;
        if(timeDiff > Config.MIN_REFRESH_TIME_MS) {
            LOG.log(Level.INFO, "Refreshing events is RQUIRED because of time difference since last refresh: " + timeDiff / 1000D + " sec.");
            return true;
        }

        Double distance = GeoUtil.distanceHaversine(
                lastFetchLocation.getLatitude(0D),lastFetchLocation.getLongitude(0D),0D,
                lastLocation.getLatitude(0D),lastLocation.getLongitude(0D),0D
        );
        if (distance > Config.MIN_REFRESH_DISTANCE) {
            LOG.log(Level.INFO, "Refreshing events is RQUIRED because of distance difference between current and last fetched location: " + distance + " m.");
            return true;
        }

        LOG.log(Level.INFO, "Refreshing events is NOT RQUIRED. (distance=" + distance + "m, time diff=" + timeDiff / 1000D + " sec) < (" +
                Config.MIN_REFRESH_DISTANCE + "m, " + Config.MIN_REFRESH_TIME_MS / 1000D + "sec).");
        return false;
    }



    // -- listeners --------------------------------------------------------------------------------
    // -- Fm Callbacks --
    boolean detectedActivityUpdated = true;
    public void onActivityChanged(Object sender, Object data) {
        Integer activity = (Integer) data;

        LOG.log(Level.INFO, "Changing user activity to " + eventsServiceUtils.getUserAactivityName(activity) + ".");
        if(detectedUserActivity ==  activity && detectedActivityUpdated) {
            LOG.log(Level.INFO, "Nothing to do, detected activity is the same as old.");
            return;
        }

        if(detectedUserActivity !=  activity) {
            LOG.log(Level.INFO, "New user activity is different, replacing current (" + eventsServiceUtils.getUserAactivityName(detectedUserActivity) + ").");
            oldUserActivity = detectedUserActivity;
            detectedUserActivity =  activity;
            detectedUserActivityTs = System.currentTimeMillis();
        }
        LOG.log(Level.INFO, "Check working mode ...");
        detectedActivityUpdated = chooseWorkingMode();
        if(detectedActivityUpdated) {
            if(lastLocation != null) {
                lastLocation.setActivity(EventsServiceUtils.getUserAactivityName(detectedUserActivity));
                eventsServiceUtils.updateServiceNotification(lastLocation,detectedUserActivity,serverStartTs);
                eventsServiceUtils.sendMsgLocationChanged(getListOfActiveClients(),lastLocation);
            }
            LOG.log(Level.INFO, "Working mode successufully changed.");
        } else {
            LOG.log(Level.INFO, "Working mode in not changed, further calls are required.");
        }
    }


    public void onEventsReceived(Object sender, Object data) {
        LOG.log(Level.INFO, "Events received.");
        List<DmEvent> rcvEvents =  ((List<DmEvent>) data);
        LOG.log(Level.INFO, "Received events list has " + rcvEvents.size() + " events.");

        // new events
        List<DmEvent> diffEvents = DmUtil.listNotIn(rcvEvents, listOfEvents);
        LOG.log(Level.INFO, "There is " + diffEvents.size() + " new events in received list of events.");

        // map of old important events
        Map<String, DmEvent> importantEventsMap = new HashMap<>();
        for(DmEvent ev: listOfEvents) {
            if(ev.getInsAttr("important", Boolean.FALSE)) {
                importantEventsMap.put(ev.getId(""), ev);
            }
        }
        LOG.log(Level.INFO, "Old events list had " + importantEventsMap.size() + " events.");

        // create new list
        List<DmEvent> allEvents = DmUtil.listNotIn(listOfEvents, rcvEvents);
        allEvents.addAll(rcvEvents);

        List<DmEvent> newListOfEvents =  GeUtils.optimizeSortedEventsList(
                GeUtils.recalculateEventDistancesAndSort(
                        lastLocation, allEvents, Config.NOTIF_RADIUS.doubleValue(), Config.NOTIF_RADIUS_DISMISS.doubleValue()
                ),
                Config.MAX_CACHED_EVENTS_LIST_SIZE, Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT
        );
        LOG.log(Level.INFO, "New events list has " + newListOfEvents.size() + " events.");

        // create list for nortif
        List<DmEvent> notifEvList = new ArrayList<>();
        for(DmEvent ev: newListOfEvents) {
            if(ev.getInsAttr("important",Boolean.FALSE) && !importantEventsMap.containsKey(ev.getId(""))) {
                notifEvList.add(ev);
            }
        }
        LOG.log(Level.INFO, "List of events with changed importance has " + notifEvList.size() + " events.");

        LOG.log(Level.INFO, "Updating list of cached events ...");
        listOfEvents.clear();
        listOfEvents.addAll(newListOfEvents);

        LOG.log(Level.INFO, "Cleanup requestUpdate process ...");
        fetchInProgressLocation.setInsAttr("refreshRequired", Boolean.FALSE);
        lastFetchLocation = fetchInProgressLocation;
        lastFetchTs = System.currentTimeMillis();
        fetchInProgressLocation = null; // Allow furter fetchs.

        // display notifications for important events
        if(notifEvList.size() > 0) {
            LOG.log(Level.INFO, "Displaying of new events notifications is required.");
            eventsServiceUtils.showNotifications(notifEvList);
        }

        // update clients
        if(diffEvents.size() > 0) {
            LOG.log(Level.INFO, "Updating clients with new events is required.");
            eventsServiceUtils.sendMsgEventsChanged(getListOfActiveClients(),diffEvents);
        }

        LOG.log(Level.INFO, "Handling new events is done.");
    }

    public void onEventsFetchError(Object sender, Object data) {
        if(sender == evdispatcher) {
            fetchInProgressLocation = null;
            LOG.log(Level.WARNING, "Refreshing events error!", ((Exception) data));
            try {
                FmException e = (FmException)data;
                if(e.getErrorCode().equals("FE0002")) {
                    DmConfiguration config = Config.getConfiguration();
                    if(!config.getToken("").isEmpty()) {
                        config.setToken("");
                        Config.setConfiguration(config);
                        eventsServiceUtils.sendMsgConfigChanged(getListOfActiveClients(), config);
                    }
                }
            } catch(Exception e) {
                LOG.log(Level.WARNING, "Changinf configuration error!", ((Exception) data));
            }
        }
    }

    private boolean updateGpsLocationAddress() {
        boolean updated = false;
        if (lastPlace == null || lastLocation == null) {
            return updated;
        }

        Double oldDist = lastLocation.getInsAttr("addressDistance", Double.MAX_VALUE);
        Double newDist = lastPlace == null ? Double.MAX_VALUE :
                GeoUtil.distanceHaversine(
                        lastPlace.getInsAttr("queryLatitude", 0D), lastPlace.getInsAttr("queryLongitude", 0D), 0D,
                        lastLocation.getLatitude(0D), lastLocation.getLongitude(0D), 0D
                );
        if (lastPlace.getInsAttr("queryLocationID", "").equals(lastLocation.getID())) {
            LOG.log(Level.INFO, "New address is for current gps location, replacing old address.");
            lastLocation.setInsAttr("address", lastPlace.getDisplayName(""));
            lastLocation.setInsAttr("addressDistance", newDist);
            lastLocation.setInsAttr("addressPlace", lastPlace);
            updated = true;
        } else if(newDist < oldDist && newDist < EventsServiceUtils.ADDRESS_RADIUS) {
            LOG.log(Level.INFO, "New address is better and within adress radius, replacing old address in gps location.");
            lastLocation.setInsAttr("address", lastPlace.getDisplayName(""));
            lastLocation.setInsAttr("addressDistance", newDist);
            lastLocation.setInsAttr("addressPlace", lastPlace);
            updated = true;
        }
        if(updated) {
            LOG.log(Level.INFO, "Address changed, updating clients is required.");
            eventsServiceUtils.sendMsgAddressChanged(getListOfActiveClients(), lastPlace);
        }

        return updated;
    }

    public void onRestResponse(Object sender, Object data) {
        LOG.log(Level.INFO, "New REST response received.");

        boolean updated = false;
        GeRestClient.Endpoint endPoint = (GeRestClient.Endpoint) ((Object[])data)[0];
        Map<Object, String> args = (Map<Object, String>) ((Object[])data)[1];

        if(lastLocation != null && endPoint.equals(GeRestClient.Endpoint.GEO_REVERSE)) {
            LOG.log(Level.INFO, "New address received.");
            NomPlace newPlace = (NomPlace) ((Object[]) data)[2];
            newPlace.setInsAttr("queryLongitude", args.get("lon"));
            newPlace.setInsAttr("queryLatitude", args.get("lat"));
            newPlace.setInsAttr("queryLocationID", args.get("@ID"));
            lastPlace = newPlace;
            if (updateGpsLocationAddress()) {
                LOG.log(Level.INFO, "Address changed, service notification is required.");
                eventsServiceUtils.updateServiceNotification(lastLocation, detectedUserActivity, serverStartTs);
            }
        }
    }

    public void onBestLocationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New location received.");
        DmGpsLocation formerLocation = lastLocation;

        lastLocation = (DmGpsLocation) data;
        lastLocation.setId(lastLocation.getID());
        lastLocation.setSpeed(lastLocation.getSpeed(0D));
        lastLocation.setReceivedAt(new Date());
        lastLocation.setActivity(EventsServiceUtils.getUserAactivityName(detectedUserActivity));

        // add loc to db
        LOG.log(Level.INFO, "Saving new location in DB ...");
        try {
            eventsServiceUtils.saveGpsLocation(lastLocation);
            LOG.log(Level.INFO, "Location saved.");
        } catch(Exception e) {
            LOG.log(Level.INFO, "Location save error:", e);
        }

        LOG.log(Level.INFO, "Current location is now " + lastLocation + ".");
        LOG.log(Level.INFO, "Updating clients is required.");
        eventsServiceUtils.sendMsgLocationChanged(getListOfActiveClients(), lastLocation);

        // address
        updateGpsLocationAddress();
        eventsServiceUtils.updateServiceNotification(lastLocation,detectedUserActivity,serverStartTs);

        LOG.log(Level.INFO, "Check if event list refresh is needed ...");
        if(!refreshEventsList(false)) { // if refresh is not req just recalculate distances in list and sort
            LOG.log(Level.INFO, "Updating clients and service notification is not required, maintain current list.");
            List<DmEvent> newEvents =  GeUtils.optimizeSortedEventsList(
                    GeUtils.recalculateEventDistancesAndSort(
                            lastLocation, listOfEvents, Config.NOTIF_RADIUS.doubleValue(), Config.NOTIF_RADIUS_DISMISS.doubleValue()
                    ),
                    Config.MAX_CACHED_EVENTS_LIST_SIZE, Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT
            );
            listOfEvents.clear();
            listOfEvents.addAll(newEvents);
            LOG.log(Level.INFO, "Updating clients and service notification is done.");
        }

        LOG.log(Level.INFO, "Check working mode ...");
        if(!detectedActivityUpdated) onActivityChanged(this, detectedUserActivity);

        LOG.log(Level.INFO, "Check address requestUpdate ...");
        refreshAddress(formerLocation == null);

        LOG.log(Level.INFO, "Handling new location event is done.");
    }


    public void onTimer(Object sender, Object data) {
        LOG.log(Level.INFO, "New timer event is received.");
        try {
            checkTimeoutsAndTimeTriggers();
            LOG.log(Level.INFO, "Handling timer event is done.");
        } catch(Exception e) {
            LOG.log(Level.WARNING, "Handling timer event is finished with error.",e);
        }
    }

    public int getNumberOfClients() {
        return mClients.size();
    }

    public List<Messenger> getListOfActiveClients() {
        List<Messenger> activeClients = new ArrayList<>();
        for(Messenger msgr: mClients) {
            if(mClientsPused.get(msgr) == null) {
                activeClients.add(msgr);
            }
        }
        return activeClients;
    }

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {


        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case EventsServiceUtils.MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    mClientsPused.remove(msg.replyTo);
                    if(getNumberOfClients() == 0 && !eventsServiceUtils.getServiceStatus(EventsService.class).equals(EventsServiceUtils.SERVICE_STATUS.FOREGROUND)) {
                        eventsServiceUtils.setServiceStatus(EventsService.this, EventsServiceUtils.SERVICE_STATUS.UNKNOWN);
                    }
                    break;
                case EventsServiceUtils.MSG_PAUSE_CLIENT:
                    if(!mClients.contains(msg.replyTo)) break;
                    if(mClientsPused.get(msg.replyTo)!= null) break;
                    mClientsPused.put(msg.replyTo, true);
                    break;
                case EventsServiceUtils.MSG_RESUME_CLIENT:
                    if(!mClients.contains(msg.replyTo)) break;
                    mClientsPused.remove(msg.replyTo);
                    break;
                case EventsServiceUtils.MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo); // continue as update request
                case EventsServiceUtils.MSG_REQUEST_UPDATE:
                    if(!mClients.contains(msg.replyTo)) break;
                    if(mClientsPused.get(msg.replyTo)!= null) break;
                    Messenger client = msg.replyTo;
                    try {
                        client.send(eventsServiceUtils.createConfigChangeMsg(Config.getConfiguration()));
                        if(lastLocation != null)
                            client.send(eventsServiceUtils.createLocationChangeMsg(lastLocation));
                        if(lastLocation != null && lastLocation.getInsAttr("addressPlace",null) != null)
                            client.send(eventsServiceUtils.createAddressChangeMsg((NomPlace) lastLocation.getInsAttr("addressPlace", null)));
                        if (listOfEvents.size() > 0)
                            client.send(eventsServiceUtils.createEventsChangedMsg(listOfEvents));
                    } catch (RemoteException e) {
                        // The client is dead.  Remove it from the list;
                        mClients.remove(client);
                    }
                    break;
                case EventsServiceUtils.MSG_REQUEST_RELOAD_CONFIG:
                    if(!mClients.contains(msg.replyTo)) break;
                    if(mClientsPused.get(msg.replyTo) != null) break;
                    Bundle data = msg.getData();
                    if(data != null) {
                        Map<String, Object> map = (HashMap<String, Object>) data.getSerializable("configuration");
                        if (map != null) {
                            try {
                                DmConfiguration dm = (DmConfiguration) ParcelDm.mapToDm(map, getClass().getClassLoader());
                                if (dm != null) {
                                    Config.saveConfiguration(dm);
                                    Config.setConfiguration(dm);
                                    eventsServiceUtils.sendMsgConfigChanged(getListOfActiveClients(), dm);
                                    listOfEvents.clear();
                                    refreshEventsList(true);
                                }
                            } catch (Exception e) {
                                LOG.warning(e.getMessage());
                            }
                        }
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /*
    private void initServiceRestartTimer() {
        PendingIntent localPendingIntent =  PendingIntent.getService(this, 0, new Intent(this, UsbService.class), 0);

        AlarmManager localAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Calendar localCalendar = Calendar.getInstance();
        localCalendar.setTimeInMillis(System.currentTimeMillis());
        localCalendar.add(13, 50);
        localAlarmManager.set(0, localCalendar.getTimeInMillis(), localPendingIntent)

    }
    */


    public GeClientSessionContext getGeSession() {
        return (GeClientSessionContext) FmContext.getSessionBean(FmSessionContext.class);
    }

    public void fmContextLoader() {
        try {
            FmApplication.instance(FmApplication.class,"GeoEventService");
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        FmContext.addSessionBean(FmSessionContext.class, new GeClientSessionContext());

        FmContext.addDmClassType(DmUser.class.getSimpleName(), DmUser.class);
        FmContext.addDmClassType(DmException.class.getSimpleName(), DmException.class);
        FmContext.addDmClassType(DmGpsLocation.class.getSimpleName(), DmGpsLocation.class);
        FmContext.addDmClassType(DmSession.class.getSimpleName(), DmSession.class);
        FmContext.addDmClassType(DmSysInfo.class.getSimpleName(), DmSysInfo.class);
        FmContext.addDmClassType(DmEvent.class.getSimpleName(),DmEvent.class);
        FmContext.addDmClassType(DmLocation.class.getSimpleName(), DmLocation.class);
        FmContext.addDmClassType(DmMessage.class.getSimpleName(), DmMessage.class);

        FmContext.addDmClassType(NomPlace.class.getSimpleName(), NomPlace.class);
        FmContext.addDmClassType(NomAddress.class.getSimpleName(), NomAddress.class);
        FmContext.addDmClassType(NomPolygon.class.getSimpleName(), NomPolygon.class);

        FmApplication.instance().setDao(DaoInternal.class, new DaoInternalImpl());
    }

}
