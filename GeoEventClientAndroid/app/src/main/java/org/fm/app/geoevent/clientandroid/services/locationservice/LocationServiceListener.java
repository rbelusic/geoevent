package org.fm.app.geoevent.clientandroid.services.locationservice;

import android.location.Location;

/**
 * Created by RobertoB on 8.2.2016..
 */
public interface LocationServiceListener {
    public void onLocation(Location l);

    public void onBestLocation(Location l);

    public void onQuit();
}
