package org.fm.app.geoevent.clientandroid.adapters;

        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ArrayAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import org.fm.FmDispatcher;
        import org.fm.FmEventsDispatcher;
        import org.fm.app.geoevent.apis.osm.dm.NomPlace;
        import org.fm.app.geoevent.clientandroid.R;

        import java.util.List;


public class PlacesAdapter extends ArrayAdapter<NomPlace> implements FmDispatcher {
    private FmEventsDispatcher dispatcher = new FmEventsDispatcher(this);
    private final Context context;
    private final NomPlace[] values;

    public PlacesAdapter(Context context, NomPlace[] values) {
        super(context, R.layout.event_layout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NomPlace nomPlace = values[position];

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.place_layout, parent, false);

        TextView titleView = (TextView) rowView.findViewById(R.id.label);
        TextView detailsView = (TextView) rowView.findViewById(R.id.details);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);


        titleView.setText("Place Id: " + nomPlace.getPlaceId(0L) + "(" + nomPlace.getOsmType("") + "/" + nomPlace.getOsmId(0L) + ")");
        detailsView.setText(nomPlace.getDisplayName(""));

        // Change the icon for even and odd rows
        imageView.setImageResource((System.currentTimeMillis() & 1) == 1 ? R.drawable.odd : R.drawable.even);


        return rowView;
    }


    public void refreshList(NomPlace[] places) {
        clear();
        addAll(places);
    }

    // -- fmdispatcher -----------------------------------------------------------------------------
    @Override
    public <T> boolean addListener(T t) {
        return dispatcher.addListener(t);
    }

    @Override
    public <T> void fireEvent(String s, T t) {
        fireEvent(s,t);
    }

    @Override
    public void removeAllListeners() {
        dispatcher.removeAllListeners();
    }

    @Override
    public <T> boolean removeListener(T t) {
        return dispatcher.addListener(t);
    }

    @Override
    public int getListenersCount() {
        return dispatcher.getListenersCount();
    }

    @Override
    public <T> List<T> getListeners() {
        return dispatcher.getListeners();
    }

    @Override
    public <T> boolean isListener(T t) {
        return dispatcher.isListener(t);
    }

    @Override
    public String getID() {
        return dispatcher.getID();
    }
}
