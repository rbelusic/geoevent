package org.fm.app.geoevent.clientandroid.impl.sensors;

import android.app.IntentService;
import android.content.Intent;

import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

/**
 * Service that receives ActivityRecognition updates. It receives updates
 * in the background, even if the main Activity is not visible.
 */
public class ActivityRecognitionIntentService extends IntentService {
    //private LocalBroadcastManager broadcastManager = null;
    private int activityType = 0;
    private String activityTypeName = "";


    public ActivityRecognitionIntentService() {
        super(ActivityRecognitionIntentService.class.getName());
        info("!!!!! INIT onHandleIntent");

    }


    //..
    /**
     * Called when a new activity detection update is available.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        info("!!!!!onHandleIntent");
        //...
        // If the intent contains an update
        if (ActivityRecognitionResult.hasResult(intent)) {
            // Get the update
            ActivityRecognitionResult result =
                    ActivityRecognitionResult.extractResult(intent);

            DetectedActivity mostProbableActivity
                    = result.getMostProbableActivity();

            // Get the confidence % (probability)
            int confidence = mostProbableActivity.getConfidence();

            // Get the type
            activityType = mostProbableActivity.getType();
            activityTypeName = mostProbableActivity.toString();

            info("onHandleIntent: " + activityType);
           /* types:
            * DetectedActivity.IN_VEHICLE
            * DetectedActivity.ON_BICYCLE
            * DetectedActivity.ON_FOOT
            * DetectedActivity.STILL
            * DetectedActivity.UNKNOWN
            * DetectedActivity.TILTING
            */
            // process
            Intent broadcastIntent = new Intent(ActivityRecognitionIntentService.class.getName());
            broadcastIntent.putExtra("activityType",activityType);

            // Broadcast *locally* to other components in this app
            //if(broadcastManager == null) broadcastManager  = LocalBroadcastManager.getInstance(this);
           // broadcastManager.sendBroadcast(broadcastIntent);
            sendBroadcast(broadcastIntent);
        }
    }


    private void info(String s) {
//        Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT).show();
    }
}