package org.fm.app.geoevent.clientandroid.services.events;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateUtils;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.DetectedActivity;

import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.app.geoevent.application.GeSessionContext;
import org.fm.app.geoevent.client.GeRestClient;
import org.fm.app.geoevent.clientandroid.Config;
import org.fm.app.geoevent.clientandroid.ParcelDm;
import org.fm.app.geoevent.clientandroid.R;
import org.fm.app.geoevent.clientandroid.RestAsyncTask;
import org.fm.app.geoevent.clientandroid.activities.EventMapActivity;
import org.fm.app.geoevent.clientandroid.activities.EventsListActivity;
import org.fm.app.geoevent.clientandroid.activities.GpsLocationsListActivity;
import org.fm.app.geoevent.clientandroid.activities.LoginActivity;
import org.fm.app.geoevent.clientandroid.activities.NewEventActivity;
import org.fm.app.geoevent.clientandroid.adapters.LocationsAdapter;
import org.fm.app.geoevent.clientandroid.impl.dao.DaoInternal;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.app.geoevent.dm.event.DmLocation;
import org.fm.app.geoevent.dm.event.DmMessage;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmFactory;
import org.fm.dm.DmUtil;
import org.fm.http.client.impl.FmApacheHttpClient;
import org.fm.utils.SystemUtil;
import org.fm.utils.TimeUtil;

import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by user on 27.2.2016..
 */
public class EventsServiceUtils {
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public static final String NOTIF_START_FG_ACTION = "START";
    public static final String NOTIF_STOP_FG_ACTION = "STOP";

    public static final int LOGIN_DIALOG_REQUEST_ID = 1000;
    public static final int CAMERA_DIALOG_REQUEST_ID = 1001;
    public static final Double ADDRESS_RADIUS = 30D;
    // messenger
    public static final int NOTIFICATION_EVENTS_SERVER_ID = 1000;
    public static final int MSG_REGISTER_CLIENT = 100;
    public static final int MSG_UNREGISTER_CLIENT = 101;
    public static final int MSG_REQUEST_UPDATE = 102;
    public static final int MSG_REQUEST_RELOAD_CONFIG = 103;
    public static final int MSG_PAUSE_CLIENT = 104;
    public static final int MSG_RESUME_CLIENT = 105;
    public static final int MSG_RESPONSE_CONFIG = 200;
    public static final int MSG_RESPONSE_LOCATION = 201;
    public static final int MSG_RESPONSE_EVENTS = 202;
    public static final int MSG_RESPONSE_ADDRESS = 203;
    private final static Logger LOG = FmContext.getLogger(EventsService.class);

    private final Context context;
    private final NotificationManager mNM;
    private RemoteViews remoteWidget = null;
    private NotificationCompat.Builder mBuilder = null;
    private GeRestClient restClient = null;
    private DmConfiguration currentConfig = null;
    private SERVICE_STATUS serviceStatus = SERVICE_STATUS.UNKNOWN;
    private Boolean firstRun = null;
    private SharedPreferences PREFS = null;

    public EventsServiceUtils(Context ctx) {
        context = ctx;
        mNM = (NotificationManager) ctx.getSystemService(ctx.NOTIFICATION_SERVICE);
        Config.updateWorkingConfiguration();

    }

    public static String[] formatGpsLocationInfo(Context ctx, DmGpsLocation lastLocation) {
        String locationString = String.format(
                "At %9.4f%s %9.4f%s %8.2f m",
                Math.abs(lastLocation.getLongitude(0D)),
                lastLocation.getLatitude(0D) > 0 ? "E" : "W",
                Math.abs(lastLocation.getLatitude(0D)),
                lastLocation.getLongitude(0D) > 0 ? "N" : "S",
                lastLocation.getAltitude(0D)
        );
        String locationetcString = String.format(
                "%s, accr %5.1f m, at %s",
                (lastLocation.getInsAttr("address", "").isEmpty() ?
                        lastLocation.getActivity("?") : lastLocation.getInsAttr("address", "")
                ), lastLocation.getAccuracy(0D),
                DateUtils.formatDateTime(ctx, ((Date) lastLocation.getTimestamp(null)).getTime(), DateUtils.FORMAT_SHOW_TIME)
        );
        return new String[]{locationString, locationetcString};
    }

    public static String getUserAactivityName(Integer detectedActivityType) {
        if (detectedActivityType == null) return "...";
        switch (detectedActivityType) {
            case DetectedActivity.IN_VEHICLE:
                return "driving";
            case DetectedActivity.ON_BICYCLE:
                return "biking";
            case DetectedActivity.ON_FOOT:
                return "walking";
            case DetectedActivity.RUNNING:
                return "running";
            case DetectedActivity.WALKING:
                return "walking";
            case DetectedActivity.STILL:
                return "still";
            case DetectedActivity.TILTING:
                return "tilting";
            case DetectedActivity.UNKNOWN:
                return "unknown";
            default:
                return "?(" + detectedActivityType + ")";
        }

    }

    public void getAddress(Object listener, final DmGpsLocation location) {
        if(location == null) return;
        if(!isNetworkOnline()) {
            return;
        }

        new RestAsyncTask(listener, new GeRestClient(new FmApacheHttpClient(), Config.SERVER_URL))
                .execute(GeRestClient.Endpoint.GEO_REVERSE, new HashMap<String, Object>() {{
                    //{"lat", "lon", "zoom", "address", "tags", "details", "osmtype", "osmid"}
                    put("@ID", location.getID());
                    put("lat", location.getLatitude(0D));
                    put("lon", location.getLongitude(0D));
                    put("zoom", 18);
                    put("address", true);
                    put("tags", true);
                    put("details", true);
                }});
    }

    public void showServiceNotificationStop() {
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = "Stopped, click here to restart.";
        CharSequence title = "Event service status";

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = createNotificationIntent();

        // Set the info for the views that show in the notification panel.
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(title)
                        .setTicker(text)  // the status text
                        .setWhen(System.currentTimeMillis())  // the time stamp
                        .setGroup(this.getClass().getSimpleName())
                        .setContentText(text);

        mBuilder.setContentIntent(contentIntent);

        Notification notif = mBuilder.build();
        notif.flags |= Notification.FLAG_AUTO_CANCEL;
        mNM.notify(NOTIFICATION_EVENTS_SERVER_ID, notif);
    }

    public Message createEventsChangedMsg(List<DmEvent> events) {
        try {
            Message message = Message.obtain(null, MSG_RESPONSE_EVENTS);
            Bundle responseData = new Bundle();
            ArrayList<HashMap<String, Object>> list = new ArrayList<>();

            for (DmEvent ev : events) {
                list.add(DmUtil.dmToMap(ev, context.getClassLoader()));
            }
            responseData.putSerializable("events", list);
            message.setData(responseData);
            return message;
        } catch (Exception ee) {
            LOG.log(Level.WARNING, ee.getMessage(), ee);
        }
        return null;
    }

    public void sendMsgEventsChanged(List<Messenger> mClients, List<DmEvent> newEvents) {
        Message message = createEventsChangedMsg(newEvents);
        if (message != null) {
            for (Messenger client : mClients) {
                try {
                    client.send(message);
                } catch (RemoteException e) {
                    mClients.remove(client);
                    LOG.log(Level.WARNING, e.getMessage(), e);
                }
            }
        }
    }

    public Message createLocationChangeMsg(DmGpsLocation lastLocation) {
        try {
            Message message = Message.obtain(null, MSG_RESPONSE_LOCATION);
            Bundle responseData = new Bundle();
            HashMap<String, Object> map = ParcelDm.dmToMap(lastLocation, context.getClassLoader());
            responseData.putSerializable("location", map);
            message.setData(responseData);
            return (message);
        } catch (Exception ee) {
            LOG.log(Level.WARNING, ee.getMessage(), ee);
        }
        return null;
    }

    public Message createAddressChangeMsg(NomPlace place) {
        try {
            Message message = Message.obtain(null, MSG_RESPONSE_ADDRESS);
            Bundle responseData = new Bundle();
            HashMap<String, Object> map = ParcelDm.dmToMap(place, context.getClassLoader());
            responseData.putSerializable("place", map);
            message.setData(responseData);
            return (message);
        } catch (Exception ee) {
            LOG.log(Level.WARNING, ee.getMessage(), ee);
        }
        return null;
    }

    public Message createConfigChangeMsg(DmConfiguration cfg) {
        try {
            Message message = Message.obtain(null, MSG_RESPONSE_CONFIG);
            Bundle responseData = new Bundle();
            HashMap<String, Object> map = ParcelDm.dmToMap(cfg, context.getClassLoader());
            responseData.putSerializable("configuration", map);
            message.setData(responseData);
            return (message);
        } catch (Exception ee) {
            LOG.log(Level.WARNING, ee.getMessage(), ee);
        }
        return null;
    }

    public void sendMsgLocationChanged(List<Messenger> mClients, DmGpsLocation lastLocation) {
        Message message = createLocationChangeMsg(lastLocation);
        if (message != null) {
            try {
                for (Messenger client : mClients) {
                    try {
                        client.send(message);
                    } catch (RemoteException e) {
                        mClients.remove(client);
                    }
                }
            } catch (Exception ee) {
                LOG.log(Level.WARNING, ee.getMessage(), ee);
            }
        }
    }

    public void sendMsgAddressChanged(List<Messenger> mClients, NomPlace place) {
        Message message = createAddressChangeMsg(place);
        if (message != null) {
            try {
                for (Messenger client : mClients) {
                    try {
                        client.send(message);
                    } catch (RemoteException e) {
                        mClients.remove(client);
                    }
                }
            } catch (Exception ee) {
                LOG.log(Level.WARNING, ee.getMessage(), ee);
            }
        }
    }

    public void sendMsgConfigChanged(List<Messenger> mClients, DmConfiguration cfg) {
        Message message = createConfigChangeMsg(cfg);
        if (message != null) {
            try {
                for (Messenger client : mClients) {
                    try {
                        client.send(message);
                    } catch (RemoteException e) {
                        mClients.remove(client);
                    }
                }
            } catch (Exception ee) {
                LOG.log(Level.WARNING, ee.getMessage(), ee);
            }
        }
    }

    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(context);
        if (result != ConnectionResult.SUCCESS) {
            return false;
        }

        return true;
    }

    private PendingIntent createNotificationIntent() {
        Intent resultIntent = new Intent(context, EventsListActivity.class);
        TaskStackBuilder stackBuilder = null;
        stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(EventsListActivity.class);// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addNextIntent(resultIntent); // Adds the Intent that starts the Activity to the top of the stack
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT
        );

        return resultPendingIntent;
    }

    private PendingIntent createNotificationBtnIntent(String action) {
        Intent actionIntent = new Intent(context, EventsService.class);
        actionIntent.setAction(action);
        PendingIntent btnIntent = PendingIntent.getService(context, 0, actionIntent, 0);
        return btnIntent;
    }

    public void showNotifications(List<DmEvent> evlst) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        // create notifs for each event
        for (DmEvent ev : evlst) {
            // Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(context, EventsListActivity.class);
            TaskStackBuilder stackBuilder = null;
            stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(EventsListActivity.class);// Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addNextIntent(resultIntent); // Adds the Intent that starts the Activity to the top of the stack
            PendingIntent resultPendingIntent = createNotificationIntent();

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_notification)
                            .setContentTitle(ev.getMessage(null).getTitle(""))
                            .setGroup(this.getClass().getSimpleName())
                            .setContentText(ev.getMessage(null).getMessageBody(""));

            mBuilder.setSound(alarmSound);
            mBuilder.setContentIntent(resultPendingIntent);

            Notification notif = mBuilder.build();
            notif.flags |= Notification.FLAG_AUTO_CANCEL;
            mNM.notify(ev.getId("").hashCode(), notif);
        }
    }

    public void updateServiceNotification(DmGpsLocation lastLocation, Integer detectedUserActivity, long serverStartTs) {
        mNM.notify(NOTIFICATION_EVENTS_SERVER_ID, createServiceNotification(lastLocation, detectedUserActivity, serverStartTs));
    }

    public Notification createServiceNotification(DmGpsLocation lastLocation, Integer detectedUserActivity, long serverStartTs) {
        // setup
        CharSequence title = "Event service status"; 
        CharSequence titleInit = "Event service";


        if (remoteWidget == null) {

            PendingIntent contentIntent = createNotificationIntent();
            remoteWidget = new RemoteViews(context.getPackageName(), R.layout.events_service_notif_layout);
            remoteWidget.setOnClickPendingIntent(R.layout.events_service_notif_layout, contentIntent);

            mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_notification)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setContent(remoteWidget)
                            .setContentTitle(titleInit)
                            .setTicker(titleInit)  // the status text
                            .setWhen(System.currentTimeMillis())  // the time stamp
                            .setGroup(this.getClass().getSimpleName())
                            .setContentIntent(contentIntent)

            ;
        }

        mBuilder.mActions.clear();

        mBuilder.mActions.clear();
        if (getServiceStatus(EventsService.class).equals(SERVICE_STATUS.FOREGROUND)) {
            mBuilder.addAction(android.R.drawable.ic_menu_close_clear_cancel, "Stop", createNotificationBtnIntent(NOTIF_STOP_FG_ACTION));
        } else if (getServiceStatus(EventsService.class).equals(SERVICE_STATUS.ON_DEMAND)) {
            mBuilder.addAction(android.R.drawable.ic_media_play, "Start", createNotificationBtnIntent(NOTIF_START_FG_ACTION));
        }

        mBuilder.setContentTitle(titleInit+ ": " +getServiceStatus(EventsService.class));


        // update ui
        if (lastLocation != null) {
            updateRemoteViews(lastLocation, detectedUserActivity, serverStartTs, remoteWidget);
        }

        // Set the info for the views that show in the notification panel.

        Notification notif = mBuilder.build();

        return notif;
    }

    public void updateRemoteViews(DmGpsLocation lastLocation, Integer detectedUserActivity, long serverStartTs, RemoteViews remoteViews) {
        String titleString = "Events service";
        String startuptimeString =
                DateUtils.formatDateTime(
                        context, serverStartTs,
                        DateUtils.FORMAT_SHOW_TIME |
                                (TimeUtil.daysBetween(new Date(serverStartTs), new Date()) != 0 ? DateUtils.FORMAT_SHOW_DATE : 0)
                );

        String[] locationInfo = formatGpsLocationInfo(context, lastLocation);

        String locationString = locationInfo[0];

        String locationetcString =
            isNetworkOnline() ? locationInfo[1] : context.getString(R.string.network_error)
        ;


        remoteViews.setTextViewText(R.id.title, titleString);
        remoteViews.setTextViewText(R.id.startuptime, startuptimeString);
        remoteViews.setTextViewText(R.id.location, locationString);
        remoteViews.setTextViewText(R.id.locationetc, locationetcString);
    }

    public String getAndroidAppPackageName() {
        return context.getApplicationContext().getPackageName();
    }

    // -- db ---------------------------------------------------------------------------------------
    public boolean isDbCreated() {
        boolean created = true;
        try {
            String dbFileName =
                    "/data/data/" + context.getApplicationContext().getPackageName() + "/databases/ge-" + Config.DB_VER + ".db";

            // check for db file existence
            File f = new File(dbFileName);
            if (!f.exists()) {
                if (null != f.getParent()) {
                    f.getParentFile().mkdirs();
                }
                created = false;
            }

            // create db if neccessary
            SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(dbFileName, null);
            db.close();

            if (!created) return (created);

            // try to load config, create table on error
            DmConfiguration config = null;
            try {
                config = Config.loadConfiguration();
            } catch (Exception e) {
            }

            if (config == null) {
                created = false;
            }

            return created;
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return false;
    }

    public void createDatabase() {
        try {
            if (isDbCreated()) return;

            FmSessionContext ctx = (FmSessionContext) FmContext.getSessionBean(FmSessionContext.class);

            ctx.getStorageConnection().execute(
                    "dao-drop-table-configuration.sql",
                    new HashMap<String, Object>()
            );


            ctx.getStorageConnection().execute(
                    "dao-create-table-configuration.sql",
                    new HashMap<String, Object>()
            );

            ctx.getStorageConnection().execute(
                    "dao-drop-table-gpslocations.sql",
                    new HashMap<String, Object>()
            );


            ctx.getStorageConnection().execute(
                    "dao-create-table-gpslocations.sql",
                    new HashMap<String, Object>()
            );


            DaoInternal dao = FmApplication.instance().getDao(DaoInternal.class);
            dao.put(new DmConfiguration[]{Config.getWorkingConfiguration()});
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    public List<DmGpsLocation> loadGpsLocations(int start, int count) {
        List<DmGpsLocation> locs = FmApplication.instance().getDao(DaoInternal.class).getRecentGpsLocations(start, count);
        return locs;
    }

    public void saveGpsLocation(DmGpsLocation loc) {
        FmApplication.instance().getDao(DaoInternal.class).put(new DmGpsLocation[]{loc});
    }

    // -- activities managment ---------------------------------------------------------------------
    public void startLoginActivity() {
        Intent intent = new Intent(context, LoginActivity.class);
        Bundle args = new Bundle();

        args.putSerializable(
                "username",
                Config.getConfiguration().getUsername("")
        );

        intent.putExtras(args);
        if (Activity.class.isAssignableFrom(context.getClass())) {
            ((Activity) context).startActivityForResult(intent, LOGIN_DIALOG_REQUEST_ID);
        } else {
            context.startActivity(intent);
        }
    }

    public void startMapActivity(DmEvent event, DmGpsLocation location) {
        Intent intent = new Intent(context, EventMapActivity.class);
        Bundle args = new Bundle();
        if (event != null) {
            args.putSerializable(
                    "event",
                    DmUtil.dmToMap(event, context.getClassLoader())
            );
        }
        if (location != null) {
            args.putSerializable(
                    "location",
                    DmUtil.dmToMap(location, context.getClassLoader())
            );
        }

        intent.putExtras(args);
        context.startActivity(intent);
    }

    /**
     * Create a File for saving an image or video
     */
    public File getOutputMediaFile() {
        String name = context.getClass().getName() + ".png";

        String appStorageDir = context.getPackageName();

        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appStorageDir
        );
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + name);
        return mediaFile;
    }

    public void startPhotoChooseActivity() {
        File file = getOutputMediaFile();
        Uri fileUri = Uri.fromFile(file);

        Intent intent = createPhotoChooseIntent(fileUri);
        if (Activity.class.isAssignableFrom(context.getClass())) {
            ((Activity) context).startActivityForResult(intent, CAMERA_DIALOG_REQUEST_ID);
        } else {
            context.startActivity(intent);
        }
    }

    private Intent createPhotoChooseIntent(Uri cameraFileUri) {
        // Camera
        final List<Intent> cameraIntents = new ArrayList<Intent>();

        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        return chooserIntent;
    }

    public void startNewEventActivity() {
        Intent intent = new Intent(context, NewEventActivity.class);
        context.startActivity(intent);
    }


    public void startLocationsListActivity() {
        Intent intent = new Intent(context, GpsLocationsListActivity.class);
        context.startActivity(intent);
    }

    public boolean isFirstRun() {
        if (firstRun == null) {
            firstRun = getPreference("firstrun", "true").equals("true");
            if (firstRun) {
                setPreference("firstrun", "false");
            }
        }
        return firstRun;
    }

    public void setFirstRun(boolean b) {
        firstRun = b;
    }

    public String getPreference(String name, String def) {
        if (PREFS == null) {
            PREFS = context.getSharedPreferences(context.getPackageName(), context.MODE_PRIVATE);
        }
        return PREFS.getString(name, def);
    }

    public void setPreference(String name, String value) {
        if (PREFS == null) {
            PREFS = context.getSharedPreferences(context.getPackageName(), context.MODE_PRIVATE);
        }
        PREFS.edit().putString(name, value).commit();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


    public boolean setServiceStatus(EventsService service, SERVICE_STATUS newstatus) {
        SERVICE_STATUS oldstatus = getServiceStatus(service.getClass());

        // STOP
        if (newstatus.equals(SERVICE_STATUS.UNKNOWN)) {
            service.stopSelf(); // stop service
            return true;
        }
        // START
        else if (newstatus.equals(SERVICE_STATUS.ON_DEMAND)) {
            setPreference("servicemode", SERVICE_STATUS.ON_DEMAND.toString());
            if (oldstatus.equals(SERVICE_STATUS.FOREGROUND)) {
                if (service.getNumberOfClients() == 0 && !getServiceStatus(EventsService.class).equals(EventsServiceUtils.SERVICE_STATUS.FOREGROUND)) {
                    service.stopForeground(true);
                    service.stopSelf();
                } else {
                    service.stopForeground(false); // leave norification
                }
                return true;
            } else if (oldstatus.equals(SERVICE_STATUS.UNKNOWN)) {
                // nista
                return true;
            } else {
                return false;
            }
        } else if (newstatus.equals(SERVICE_STATUS.FOREGROUND)) {
            setPreference("servicemode", SERVICE_STATUS.FOREGROUND.toString());
            if (!oldstatus.equals(SERVICE_STATUS.FOREGROUND)) {
                service.startForeground(
                        NOTIFICATION_EVENTS_SERVER_ID,
                        createServiceNotification(null, 0, System.currentTimeMillis())
                );
                return true;
            }
            return false;
        }

        return true;
    }

    public SERVICE_STATUS getServiceStatus(Class srvc) {
        boolean serviceRunning = false;
        boolean serviceRunningFg = false;

        ActivityManager am = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> l = am.getRunningServices(100);
        Iterator<ActivityManager.RunningServiceInfo> i = l.iterator();
        while (i.hasNext()) {
            ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo) i.next();

            if (runningServiceInfo.service.getClassName().equals(srvc.getName())) {
                serviceRunning = true;
                if (runningServiceInfo.foreground) {
                    serviceRunningFg = true;
                }
            }
        }

        return serviceRunning ? (serviceRunningFg ? SERVICE_STATUS.FOREGROUND : SERVICE_STATUS.ON_DEMAND) : SERVICE_STATUS.UNKNOWN;
    }

    public DmEvent createEvent(String title, String msgtext, DmGpsLocation lastLocation, NomPlace lastPlace) {
        DmEvent event = DmFactory.create(DmEvent.class);
        event.setVisibility(DmEvent.VISIBILITY_PUBLIC); // for now only public
        event.setTimestamp(new Date());

        DmLocation loc = DmFactory.create(DmLocation.class);
        loc.setLongitude(lastLocation.getLongitude(null));
        loc.setLatitude(lastLocation.getLatitude(null));
        loc.setZpos(lastLocation.getAltitude(0D));
        loc.setTimestamp(event.getTimestamp(null));
        event.setLocation(loc);

        DmMessage message = DmFactory.create(DmMessage.class);
        message.setContextType("text/plain");
        message.setTitle(title);
        message.setMessageBody(msgtext);
        message.setTimestamp(event.getTimestamp(null));
        event.setMessage(message);

        return event;
    }

    public boolean isNetworkOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public enum SERVICE_STATUS {
        UNKNOWN, FOREGROUND, ON_DEMAND
    }

}

