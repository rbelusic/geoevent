package org.fm.app.geoevent.clientandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.library21.custom.SwipeRefreshLayoutBottom;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.fm.app.geoevent.client.GeUtils;
import org.fm.app.geoevent.clientandroid.Config;
import org.fm.app.geoevent.clientandroid.R;
import org.fm.app.geoevent.clientandroid.adapters.EventsAdapter;
import org.fm.app.geoevent.clientandroid.adapters.LocationsAdapter;
import org.fm.app.geoevent.clientandroid.impl.FmEventServiceMsgsDispatcher;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.app.geoevent.clientandroid.services.events.EventsService;
import org.fm.app.geoevent.clientandroid.services.events.EventsServiceUtils;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.application.FmContext;
import org.fm.dm.DmUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EventsListActivity extends FmActivity implements  SwipeRefreshLayoutBottom.OnRefreshListener{
    private final static Logger LOG = FmContext.getLogger(EventsListActivity.class);
    private EventsAdapter adapter = null;
    private LocationsAdapter gpsLocAdapter = null;

    private FmEventServiceMsgsDispatcher eventsDispatcher;
    private DmGpsLocation lastLocation = null;
    private EventsAdapter eventsListAdapter = null;
    private List<DmEvent> listOfEvents = new ArrayList<>();
    private SwipeRefreshLayoutBottom swpBottom;

    // -- android ui owr ---------------------------------------------------------------------------
    // onCreate() -> onStart() -> onResume() --> RUNNING ---> onPause() -> onStop() -> onDestroy()
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // build ui
        buildUI();

        // start service
        startService(new Intent(getBaseContext(), EventsService.class));

        // create events dispatcher
        eventsDispatcher = new FmEventServiceMsgsDispatcher(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
        if(eventsDispatcher.isListener(this)) {
            eventsDispatcher.resume();
            eventsDispatcher.requestUpdate();
        } else {
            eventsDispatcher.addListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        eventsDispatcher.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();  // Always call the superclass method first
        eventsDispatcher.removeAllListeners();
    }

    private MenuItem loginMenuItem;
    private void updateLoginMenuItem() {
        loginMenuItem.setIcon(Config.getConfiguration().getToken("").isEmpty() ? R.drawable.ic_action_login : R.drawable.ic_action_logout);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling_list, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(EventsListActivity.this, "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_LONG).show();
                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                searchItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                return false;
            }
        });

        loginMenuItem = menu.findItem(R.id.action_login);
        updateLoginMenuItem();

        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
        } else if(id == R.id.action_showmap) {
            getEventsServiceUtils().startMapActivity(null, lastLocation);
        } else if(id == R.id.action_new_event) {
            getEventsServiceUtils().startNewEventActivity();
        } else if(id == R.id.action_gpslist) {
            getEventsServiceUtils().startLocationsListActivity();;
        } else if(id == R.id.action_login) {
            DmConfiguration config = Config.getConfiguration();
            if(config.getToken("").isEmpty()) {
                getEventsServiceUtils().startLoginActivity();
            } else {
                config.setToken("");
                eventsDispatcher.requestReloadConfiguration(config);
            }
        } else {
            return super.onOptionsItemSelected(item);
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == EventsServiceUtils.LOGIN_DIALOG_REQUEST_ID) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {

            }
        }
    }
    // -- fm calllbacks ----------------------------------------------------------------------------
    public void onServiceConnected(Object sender, Object params) {
    }

    private boolean firstConfigReceived = false;
    public void onConfigurationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New configuration received.");
        DmConfiguration cfg = (DmConfiguration) data;
        Config.setConfiguration(cfg);
        updateLoginMenuItem();

        // first call after start of activity
        if(!firstConfigReceived) {
            firstConfigReceived = true;
            if(cfg.getToken("").isEmpty() && getEventsServiceUtils().isFirstRun()) {
                getEventsServiceUtils().setFirstRun(false);
                getEventsServiceUtils().startLoginActivity();
            }
        }
    }

    public void onBestLocationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New location received.");
        DmGpsLocation formerLocation = lastLocation;
        lastLocation = (DmGpsLocation) data;
        lastLocation.setId(lastLocation.getID());
        lastLocation.setReceivedAt(new Date());

        LOG.log(Level.INFO, "Current location is now " + lastLocation + ".");
        List<DmGpsLocation> locs =  new ArrayList<>();
        locs.add(lastLocation);
        gpsLocAdapter.refreshList(locs);

        LOG.log(Level.INFO, "Maintain current list of events ....");
        List<DmEvent> newEvents =  GeUtils.optimizeSortedEventsList(
                GeUtils.recalculateEventDistancesAndSort(
                        lastLocation, listOfEvents, Config.NOTIF_RADIUS.doubleValue(), Config.NOTIF_RADIUS_DISMISS.doubleValue()
                ),
                Config.MAX_CACHED_EVENTS_LIST_SIZE,
                Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT
        );

        listOfEvents.clear();
        listOfEvents.addAll(newEvents);
        eventsListAdapter.refreshList(listOfEvents);
        LOG.log(Level.INFO, "Handling new location event is done.");
    }

    public void onEventsReceived(Object sender, Object data) {
        LOG.log(Level.INFO, "Events received.");
        List<DmEvent> rcvEvents =  ((List<DmEvent>) data);
        LOG.log(Level.INFO, "Received events list has " + rcvEvents.size() + " events.");

        // create new list
        List<DmEvent> allEvents = DmUtil.listNotIn(listOfEvents, rcvEvents);
        allEvents.addAll(rcvEvents);

        List<DmEvent> newListOfEvents =  GeUtils.optimizeSortedEventsList(
                lastLocation == null ? allEvents :
                GeUtils.recalculateEventDistancesAndSort(
                        lastLocation, allEvents, Config.NOTIF_RADIUS.doubleValue(), Config.NOTIF_RADIUS_DISMISS.doubleValue()
                ),
                Config.MAX_CACHED_EVENTS_LIST_SIZE, Config.MAX_CACHED_EVENTS_LIST_SIZE_IF_IMPORTANT
        );
        LOG.log(Level.INFO, "New events list has " + newListOfEvents.size() + " events. Updating list of cached events ...");
        listOfEvents.clear();
        listOfEvents.addAll(newListOfEvents);
        eventsListAdapter.refreshList(listOfEvents);

        LOG.log(Level.INFO, "Handling new events is done.");
    }


    // -- private --
    private void buildUI() {
        // main ui
        setContentView(R.layout.activity_scrolling_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        // swype
        swpBottom = (SwipeRefreshLayoutBottom) findViewById(R.id.swiperefresh);
        swpBottom.setOnRefreshListener(this);

        // list and adapter
        final ListView listView = (ListView) findViewById(R.id.list);
        eventsListAdapter = new EventsAdapter(this, new ArrayList<DmEvent>());
        listView.setAdapter(eventsListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                try {
                    getEventsServiceUtils().startMapActivity((DmEvent) listView.getAdapter().getItem(position), lastLocation);
                } catch (Exception e) {
                    showLongInfo("Show map err:" + e.getMessage());
                }
            }
        });

        // new act button
        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getEventsServiceUtils().startNewEventActivity();

            }
        });
        */

        // add adapters
        gpsLocAdapter = new LocationsAdapter(this, new ArrayList<DmGpsLocation>());
  //      ((ListView) findViewById(R.id.gpslocation)).setAdapter(gpsLocAdapter);

        eventsListAdapter = new EventsAdapter(this, new ArrayList<DmEvent>());
        ((ListView) findViewById(R.id.list)).setAdapter(eventsListAdapter);
    }



    public void sendEvents(List<DmEvent> data) {
    }

    @Override
    public void onRefresh() {
        swpBottom.setRefreshing(false);
    }
}
