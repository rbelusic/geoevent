package org.fm.app.geoevent.clientandroid.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.fm.app.geoevent.client.GeRestClient;
import org.fm.app.geoevent.clientandroid.Config;
import org.fm.app.geoevent.clientandroid.R;
import org.fm.app.geoevent.clientandroid.RestAsyncTask;
import org.fm.app.geoevent.clientandroid.impl.FmEventServiceMsgsDispatcher;
import org.fm.app.geoevent.clientandroid.impl.dm.DmConfiguration;
import org.fm.app.geoevent.clientandroid.services.events.EventsService;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.dm.DmUtil;
import org.fm.http.client.impl.FmApacheHttpClient;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends FmActivity {
    private final static Logger LOG = FmContext.getLogger(LoginActivity.class);
    private FmEventServiceMsgsDispatcher eventsDispatcher = null;
    private RestAsyncTask mAuthTask = null;

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mUsernameView = (EditText) findViewById(R.id.username);
        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.post || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button signInButton = (Button) findViewById(R.id.login_button);
        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        // start service
        startService(new Intent(getBaseContext(), EventsService.class));
        eventsDispatcher = new FmEventServiceMsgsDispatcher(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(eventsDispatcher.isListener(this)) {
            eventsDispatcher.resume();
            eventsDispatcher.requestUpdate();
        } else {
            eventsDispatcher.addListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        eventsDispatcher.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();  // Always call the superclass method first
        eventsDispatcher.removeAllListeners();
    }

    // -- fm callbacks ---
    public void onConfigurationChanged(Object sender, Object data) {
        LOG.log(Level.INFO, "New configuration received.");
        DmConfiguration cfg = (DmConfiguration) data;
        Config.setConfiguration(cfg);
        if(!cfg.getUsername("").isEmpty())
            mUsernameView.setText(cfg.getUsername(""));
    }

    /**
     * Attempts to sign in to the account specified by the login form.
     * If there are form errors (missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void onRestError(Object sender, Object data) {
        mAuthTask = null;
        showProgress(false);
        ((TextView)findViewById(R.id.status)).setText(((Exception)data).getMessage());
        mPasswordView.setError(getString(R.string.error_invalid_login));
        mPasswordView.requestFocus();
    }

    public void onRestResponse(Object sender, Object data) {
        showProgress(false);

        GeRestClient.Endpoint endPoint = (GeRestClient.Endpoint) ((Object[])data)[0];
        if(endPoint.equals(GeRestClient.Endpoint.SYSTEM_LOGIN)) {
            mAuthTask = null;
            DmSession session = (DmSession) ((Object[])data)[2];
            ((TextView)findViewById(R.id.status)).setText("Login successful");
            returnSession(session);
        }
    }

    @Override
    public void onBackPressed() {
        returnSession(null);
    }

    private void returnSession(DmSession session) {

        // Create intent to deliver some kind of result data
        Intent result = new Intent("LOGIN_ACTION");
        Bundle args = new Bundle();

        if(session != null) {
            args.putSerializable(
                    "session",
                    DmUtil.dmToMap(session, LoginActivity.this.getClassLoader())
            );
            args.putString("username", mUsernameView.getText().toString().trim());
            result.putExtras(args);
            setResult(Activity.RESULT_OK, result);

            DmConfiguration config = Config.getConfiguration();
            config.setToken(session.getId(""));
            config.setUsername(mUsernameView.getText().toString());
            eventsDispatcher.requestReloadConfiguration(config);
        } else {
            setResult(Activity.RESULT_CANCELED, result);
        }

        finish();
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String username = mUsernameView.getText().toString().trim();
        final String password = mPasswordView.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.login_error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.login_error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);



            mAuthTask =
                (RestAsyncTask) new RestAsyncTask(this,new GeRestClient(new FmApacheHttpClient(),
                        FmApplication.instance().getConfigProperty("fm.app.geoevent.server.url")))
                    .execute(GeRestClient.Endpoint.SYSTEM_LOGIN, new HashMap<String, Object>() {{
                        put("username", username);
                        put("password", password);
                    }});
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}

