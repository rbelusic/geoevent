package org.fm.app.geoevent.clientandroid.impl.observers;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;
import android.widget.Button;

import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;

import java.util.UUID;

/**
 * Created by robertob on 2.3.2016..
 */
public class MlButton extends Button implements MlObserver {
    private final static String namespace="";
    private boolean executed = false;

    private MlHost host;

    //props
    private String attributeName;
    private String eventName;
    private String id;

    private boolean changed = false;
    private long updateTimestamp = 0;

    public MlButton(Context context) {
        super(context);
    }

    public MlButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        attributeName = attrs.getAttributeValue(namespace, "attribute");
        eventName = attrs.getAttributeValue(namespace, "eventName");
    }

    public MlButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        attributeName = attrs.getAttributeValue(namespace, "attribute");
        eventName = attrs.getAttributeValue(namespace, "eventName");
    }

    @Override
    public void init() {
        MlHost hst = findHost(this.getParent());
        if(hst != null) {
            hst.addObserver(this);
        }
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MlHost h = getHost();
                DmObject dm = h == null ? null : h.getDmObject();
                sendEventToHost(eventName, dm);
            }
        });
    }

    @Override
    public <H extends MlHost> void run(H h) {
        host = h;
        executed = true;
        update(h);
    }

    @Override
    public boolean isExecuted() {
        return executed;
    }

    @Override
    public void dispose() {
        executed = false;
        host = null;
    }

    @Override
    public Object getValue() {
        MlHost h = getHost();
        if(h == null) return null;
        DmObject dm = h.getDmObject();
        if(dm == null) return null;
        Object value = dm.getAttr(getAttributeName(), null);
        return value;
    }

    @Override
    public <H extends MlHost> void setHost(H h) {
        host = h;
    }

    @Override
    public <H extends MlHost> H getHost() {
        return (H) host;
    }

    @Override
    public <H extends MlHost> void update(H caller) {
        DmObject dm = getHost().getDmObject();

        if (dm != null && getAttributeName() != null) {
            if (getUpdateTimestamp() < dm.getUpdateTimestamp()) {
                Object dmValue = dm.getAttr(getAttributeName(), null);
                Object value = getText();
                setUpdateTimestamp(System.currentTimeMillis());
                setChanged(false);
                if(!value.equals(dmValue)) {
                    setText(dmValue.toString());
                }
            }
        }
    }

    @Override
    public void verify() throws FmException {

    }

    @Override
    public void sendEventToHost(String ev, Object evdata) {
        MlHost h = getHost();
        if(h != null) h.onObserverEvent(this, ev, evdata);
    }

    @Override
    public Object getNode() {
        return this;
    }

    @Override
    public String getID() {
        if (id == null) {
            id = UUID.randomUUID().toString();
        }
        return (id);

    }

    /**
     * @return the attributeName
     */
    @Override
    public String getAttributeName() {
        return attributeName;
    }

    public static MlHost findHost(Object c) {
        while(c != null ) {
            if(c instanceof MlHost) {
                return ((MlHost)c);
            }
            c = ((ViewParent)c).getParent();
        }

        return null;
    }

    /**
     * @return the changed
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * @param changed the changed to set
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    private void setUpdateTimestamp(long l) {
        updateTimestamp = l;
    }

}
