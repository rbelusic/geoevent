package org.fm.app.geoevent.clientandroid.impl.observers;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;
import android.widget.EditText;

import org.fm.FmException;
import org.fm.dm.DmObject;
import org.fm.ml.hosts.MlHost;
import org.fm.ml.observers.MlObserver;

import java.util.UUID;

/**
 * Created by robertob on 2.3.2016..
 */
public class MlEditText extends EditText implements MlObserver {
    private final static String namespace="";
    private String updateOn; // CHNAGE, BLUR
    private boolean executed = false;
    private Object oldValue=null;

    private MlHost host;

    //props
    private String attributeName;
    private String id;

    private boolean skipEvent = false;
    private boolean changed = false;
    private long updateTimestamp = 0;

    public MlEditText(Context context) {
        super(context);
    }

    public MlEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        attributeName = attrs.getAttributeValue(namespace, "attribute");
        updateOn = attrs.getAttributeValue(namespace, "updateOn");
    }

    public MlEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        attributeName = attrs.getAttributeValue(namespace, "attribute");
        updateOn = attrs.getAttributeValue(namespace, "updateOn");
    }

    @Override
    public void init() {
        MlHost hst = findHost(this.getParent());
        if(hst != null) {
            hst.addObserver(this);
        }
        setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if(getUpdateOn().equals("BLUR")) {
                        skipEvent = false;
                        _updateDm();
                    }
                }
            }
        });

        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(getUpdateOn().equals("CHANGE")) {
                    skipEvent = false;
                    _updateDm();
                }
            }
        });
    }

    @Override
    public <H extends MlHost> void run(H h) {
        host = h;
        executed = true;
        update(h);
    }

    @Override
    public boolean isExecuted() {
        return executed;
    }

    @Override
    public void dispose() {
        executed = false;
        host = null;
    }

    @Override
    public Object getValue() {
        MlHost h = getHost();
        if(h == null) return null;
        DmObject dm = h.getDmObject();
        if(dm == null) return null;
        Object value = dm.getAttr(getAttributeName(), null);
        return value;
    }

    @Override
    public <H extends MlHost> void setHost(H h) {
        host = h;
    }

    @Override
    public <H extends MlHost> H getHost() {
        return (H) host;
    }

    @Override
    public <H extends MlHost> void update(H caller) {
        DmObject dm = getHost().getDmObject();

        if (dm != null && getAttributeName() != null) {
            if (getUpdateTimestamp() < dm.getUpdateTimestamp()) {
                Object dmValue = dm.getAttr(getAttributeName(), null);
                Object value = getText();
                setUpdateTimestamp(System.currentTimeMillis());
                setChanged(false);
                if(!value.equals(dmValue)) {
                    skipEvent = true;
                    setText(dmValue.toString());
                }
            }
        }
    }

    @Override
    public void verify() throws FmException {

    }

    @Override
    public void sendEventToHost(String ev, Object evdata) {
        MlHost h = getHost();
        if(h != null) h.onObserverEvent(this, ev, evdata);
    }

    @Override
    public Object getNode() {
        return this;
    }

    @Override
    public String getID() {
        if (id == null) {
            id = UUID.randomUUID().toString();
        }
        return (id);

    }

    /**
     * @return the attributeName
     */
    @Override
    public String getAttributeName() {
        return attributeName;
    }

    public static MlHost findHost(Object c) {
        while(c != null ) {
            if(c instanceof MlHost) {
                return ((MlHost)c);
            }
            c = ((ViewParent)c).getParent();
        }

        return null;
    }

    /**
     * @return the changed
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * @param changed the changed to set
     */
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    /**
     * @return the updateOn
     */
    public String getUpdateOn() {
        return updateOn == null || updateOn.isEmpty() ? "CHANGE" : updateOn;
    }

    /**
     * @param updateOn the updateOn to set
     */
    public void setUpdateOn(String updateOn) {
        this.updateOn = updateOn;
    }

    public long getUpdateTimestamp() {
        return updateTimestamp;
    }

    private void setUpdateTimestamp(long l) {
        updateTimestamp = l;
    }


    private void _updateDm() {
        if(!isExecuted() || skipEvent) {
            skipEvent = false;
            return;
        }

        DmObject dm = getHost().getDmObject();
        setChanged(false);
        if (dm != null && getAttributeName() != null) {
            String value = getText().toString();
            Object dmValue = dm.getAttr(getAttributeName(),null);
            if(!value.equals(dmValue)) {
                dm.setAttr(getAttributeName(), value);
            }
        }
    }


}
