package org.fm.app.geoevent.clientandroid.impl.sensors;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.common.api.ResultCallback;

import org.fm.FmDispatcher;
import org.fm.FmEventsDispatcher;

import java.util.List;


public class FmGoogleServicesActivityDispatcher implements
        FmDispatcher,
        GoogleApiClient.ConnectionCallbacks,  GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<Status>
{
    private static final String TAG = FmGoogleServicesActivityDispatcher.class.getName();

    private final FmDispatcher dp = new FmEventsDispatcher(this);
    private final GoogleApiClient googleApiClient;
    private final Context context;
    private final PendingIntent pendingIntent;
    private final IntentFilter broadcastFilter;
    private final BroadcastReceiver broadcastReceiver;


    //private LocalBroadcastManager broadcastManager;
    private Toast lastInfo = null;

    public FmGoogleServicesActivityDispatcher(Context ctx) {
        context = ctx;
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        // service
        Intent intent = new Intent(
            ctx, ActivityRecognitionIntentService.class
        );


        pendingIntent =  PendingIntent.getService(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Set the broadcast receiver intent filer
        //broadcastManager = LocalBroadcastManager.getInstance(ctx);

        // Create a new Intent filter for the broadcast receiver
        broadcastFilter = new IntentFilter(ActivityRecognitionIntentService.class.getName());


        /**
        * Broadcast receiver that receives activity update intents
        * This receiver is local only. It can't read broadcast Intents from other apps.
        */
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                info("!!!!! onReceive");
                info("ACTRCV:" + intent.getIntExtra("activityType", -1));
                updateUserActivity(intent.getIntExtra("activityType", -1));
            }
        };
        ctx.registerReceiver(broadcastReceiver, broadcastFilter);
        //broadcastManager.registerReceiver(broadcastReceiver, broadcastFilter);
    }

    private void updateUserActivity(Integer activityType) {
        fireEvent("onActivityChanged",activityType);
    }


    public void requestUpdates() {
        info("AC Connecting to Google API client");
        googleApiClient.connect(); // biti ce callback na onConnected
    }

    public void stopUpdates() {
        info("AC Disconnecting Google API client");
        googleApiClient.disconnect();
    }


    @Override
    public <T> boolean addListener(T t) {
        return dp.addListener(t);
    }

    @Override
    public <T> void fireEvent(String s, T t) {
        dp.fireEvent(s,t);
    }

    @Override
    public void removeAllListeners() {
        dp.removeAllListeners();
    }

    @Override
    public <T> boolean removeListener(T t) {
        boolean retc = dp.removeListener(t);
        return retc;
    }

    @Override
    public int getListenersCount() {
        return dp.getListenersCount();
    }

    @Override
    public <T> List<T> getListeners() {
        return dp.getListeners();
    }

    @Override
    public <T> boolean isListener(T t) {
        return dp.isListener(t);
    }

    @Override
    public String getID() {
        return dp.getID();
    }

    @Override
    public void onConnected(Bundle bundle) {
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(googleApiClient, 1000, pendingIntent).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onResult(Status status) {
        if (status.isSuccess()) {
            info("Successfully added activity detection.");

        } else {
            info("Error: " + status.getStatusMessage());
        }
    }

    private void info(String s) {
        /*
        if(lastInfo != null) lastInfo.cancel();
        lastInfo = Toast.makeText(context.getApplicationContext(), s, Toast.LENGTH_SHORT);
        lastInfo.show();
*/
    }

}
