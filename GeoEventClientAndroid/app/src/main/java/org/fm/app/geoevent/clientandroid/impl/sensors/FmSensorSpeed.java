package org.fm.app.geoevent.clientandroid.impl.sensors;

import android.content.Context;

import org.fm.app.geoevent.dm.DmSensorData;
import org.fm.client.sensors.FmSensor;
import org.fm.dm.DmFactory;
import org.fm.filters.FmLowPassFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robertob on 24.2.2016..
 */
public class FmSensorSpeed extends FmSensor {
    private static final double DIFF_THRESHOLD = 0.03D;
    private static final double SHAKE_THRESHOLD = 600D;
    private static final double NEAR_ZERO = 1D;
    private static final int NEAR_ZERO_COUNT = 20;
    private static final double NEAR_ZERO_THRESHOLD = 0.2D;

    private final Context ctx;
    private FmSensorAndroid accSensor;
    private FmSensorAndroid magSensor;
    private DmSensorData lastAccSample = null;
    private DmSensorData lastMagSample = null;
    private long lastMagUpdate = 0L;
    private long lastAccUpdate = 0L;

    private DmSensorData lastSpeedEvent = null;
    private double lastSpeedCalculated = 0D;
    private long lastSpeedCalculatedTs = 0L;
    private double lastSpeedCalculatedX = 0L;
    private double lastSpeedCalculatedY = 0L;
    private double lastSpeedCalculatedZ = 0L;

    public FmSensorSpeed(Object owner, Context context) {
        super(owner, FmSensor.SENSOR_TYPE.CUSTOM);
        ctx= context;
    }


    @Override
    public void start() {
        // ako nije vec
        if(accSensor == null) {
            accSensor = new FmSensorAndroid(this, ctx, SENSOR_TYPE.ACCELEROMETER);
            magSensor = new FmSensorAndroid(this, ctx, SENSOR_TYPE.MAGNETIC_FIELD);


            //accSensor.addFilter(new FmLowPassFilter(0.1, 3, 100));
            //magSensor.addFilter(new FmLowPassFilter(0.1, 3, 100));
            if (accSensor != null) {
                accSensor.addListener(this);
                accSensor.start();
            }
            if (magSensor != null) {
                magSensor.addListener(this);
                magSensor.start();
            }
        }
    }

    @Override
    public void stop() {
        if(accSensor != null) {
            accSensor.stop();
        }
        if(magSensor != null) {
            magSensor.stop();
        }
    }


    public void onSensorChanged(Object sender, Object evdata) {
        DmSensorData dm = (DmSensorData)evdata;
        if(SENSOR_TYPE.ACCELEROMETER.equals(dm.getSensor(null))) {
            newAccEvent(dm);
        } else if(SENSOR_TYPE.MAGNETIC_FIELD.equals(dm.getSensor(null))) {
            newMagEvent(dm);
        }
    }

    private void newMagEvent(DmSensorData evdata) {
        lastMagSample = evdata;
        lastMagUpdate = System.currentTimeMillis();
    }

    private void newAccEvent(DmSensorData evdata) {
        if(evdata.getTimestamp(0L) - lastAccUpdate < 100) return;


        double deltaSpeed = updateSpeed(evdata.getTimestamp(0L));
        lastAccSample = evdata;
        lastAccUpdate = evdata.getTimestamp(0L);
        DmSensorData speedEv = null;

        if(lastSpeedEvent == null || deltaSpeed > DIFF_THRESHOLD) {
            speedEv = DmFactory.create(DmSensorData.class);
            speedEv.setSensor(SENSOR_TYPE.CUSTOM);
            speedEv.setTimestamp(lastSpeedCalculatedTs);
            speedEv.setAccuracy(SENSOR_STATUS_ACCURACY_MEDIUM);
            speedEv.setValues(new ArrayList<Double>() {{
                add(lastSpeedCalculated);
            }});
            lastSpeedEvent = speedEv;
            fireEvent("onSpeedChanged",speedEv);
        }
    }



    private int nearZeroCount=0;
    private double updateSpeed(long curr_T) {

        if(lastAccSample == null || lastMagSample == null) {
            return 0D;
        }

        long last_T = lastAccSample.getTimestamp(0L);
        List<Double> values = lastAccSample.getValues(null);
        double x = values.get(0); // - mvalues.get(0);
        double y = values.get(1); // - mvalues.get(1);
        double z = values.get(2); // - mvalues.get(2);

        long diffTime = (curr_T - last_T); // ms
        double deltaSpeedX = (x * diffTime)/1000D;
        double deltaSpeedY = (y * diffTime)/1000D;
        double deltaSpeedZ = (z * diffTime)/1000D;
        double deltaSpeed = Math.sqrt(Math.pow(deltaSpeedX, 2) + Math.pow(deltaSpeedY, 2) + Math.pow(deltaSpeedZ, 2)); // to sec
        if(deltaSpeed > DIFF_THRESHOLD && deltaSpeed < SHAKE_THRESHOLD) {
            lastSpeedCalculatedX += deltaSpeedX;
            lastSpeedCalculatedY += deltaSpeedY;
            lastSpeedCalculatedZ += deltaSpeedZ;
            lastSpeedCalculated = Math.sqrt(Math.pow(lastSpeedCalculatedX, 2) + Math.pow(lastSpeedCalculatedY, 2) + Math.pow(lastSpeedCalculatedZ, 2));
            if(lastSpeedCalculated < NEAR_ZERO_THRESHOLD) lastSpeedCalculated = 0D;
            lastSpeedCalculatedTs = System.currentTimeMillis();
            nearZeroCount = 0;
        } else {
            deltaSpeed = 0D;
            if(lastSpeedCalculated != 0D && lastSpeedCalculated < NEAR_ZERO) nearZeroCount++;
            if(nearZeroCount >= NEAR_ZERO_COUNT) {
                lastSpeedCalculatedX = 0D;
                lastSpeedCalculatedY = 0D;
                lastSpeedCalculatedZ = 0D;
                lastSpeedCalculated = 0D;
                lastSpeedCalculatedTs = System.currentTimeMillis();
            }
        }


        return(deltaSpeed);
    }
}
