CREATE TABLE GPS_LOCATIONS
(
    ID                              VARCHAR(128)    NOT NULL PRIMARY KEY,
    ACCURACY                        DOUBLE          NOT NULL,
    ALTITUDE                        DOUBLE          NOT NULL,
    BEARING                         DOUBLE          NOT NULL,
    SPEED                           DOUBLE          NOT NULL,
    ACTIVITY                        VARCHAR(128)    NOT NULL,
    LATITUDE                        DOUBLE          NOT NULL,
    LONGITUDE                       DOUBLE          NOT NULL,
    PROVIDER                        VARCHAR(64)     NOT NULL,
    TSTAMP                          TIMESTAMP       NOT NULL,
    RECEIVED_AT                     CURRENT_TIMESTAMP  NOT NULL
)
