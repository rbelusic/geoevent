CREATE TABLE CONFIGURATION
(
    ID                              VARCHAR(128)    NOT NULL PRIMARY KEY,
    SERVER_URL                      VARCHAR(128)    NOT NULL,
    USERNAME                        VARCHAR(128)    NOT NULL,
    TOKEN                           VARCHAR(128)    NOT NULL,
    --
    MIN_REFRESH_DISTANCE            REAL            NOT NULL,
    EVENTS_FETCH_DISTANCE           REAL            NOT NULL,
    MIN_REFRESH_TIME_MS             INT             NOT NULL,
    NOTIFICATION_RADIUS             REAL            NOT NULL,
    NOTIFICATION_RADIUS_DISMISS     REAL            NOT NULL,
    MAX_CACHED_EVENTS_LIST_SIZE     INT             NOT NULL,
    MAX_CACHED_EVENTS_LIST_SIZE_IMP INT             NOT NULL,
    --
    CREATED                          TIMESTAMP      DEFAULT CURRENT_TIMESTAMP,
    UPDATED                          TIMESTAMP
)
