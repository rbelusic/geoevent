select * from (
    select e.*,
        6371.00 * acos(
        cos( radians([:latitude]) ) * 
        cos( radians( l.LATITUDE ) )* 
        cos( radians( l.LONGITUDE ) - radians([:longitude]) ) 
        + 
        sin( radians([:latitude]) ) * sin(radians(l.LATITUDE)) 
    )  AS DISTANCE
    from EVENTS e, LOCATIONS l 
    where 
        l.ID = e.LOCATIONID 
    and (e.USERID = [:userid] or e.VISIBILITY = 1)
    AND l.GEINDEX LIKE [:tilerange]
    and 6371.00 * acos(
        cos( radians([:latitude]) ) * 
        cos( radians( l.LATITUDE ) )* 
        cos( radians( l.LONGITUDE ) - radians([:longitude]) ) 
        + 
        sin( radians([:latitude]) ) * sin(radians(l.LATITUDE)) 
    )  < [:distance] 
) as tmp
order by tmp.DISTANCE
limit 0, [:nrows]

