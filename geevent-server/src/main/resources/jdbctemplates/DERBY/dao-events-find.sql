select * from (
    select e.*, row_number() over() as row_num,
        6371.00 * acos(
        cos( radians([:latitude]) ) * 
        cos( radians( l.LATITUDE ) )* 
        cos( radians( l.LONGITUDE ) - radians([:longitude]) ) 
        + 
        sin( radians([:latitude]) ) * sin(radians(l.LATITUDE)) 
    )  AS distance

    from EVENTS e, LOCATIONS l 
    where 
        l.id = e.locationid 
    and (e.USERID = [:userid] or e.VISIBILITY = 1)
    and 6371.00 * acos(
        cos( radians([:latitude]) ) * 
        cos( radians( l.LATITUDE ) )* 
        cos( radians( l.LONGITUDE ) - radians([:longitude]) ) 
        + 
        sin( radians([:latitude]) ) * sin(radians(l.LATITUDE)) 
    ) < [:distance]
) as tmp
where row_num < [:nrows]
order by tmp.distance

