select row_number() over() as row_num, tmp.* from (
    select * from (
        select e.*, 
            6371.00 * acos(
        cos( radians([:latitude]) ) * 
        cos( radians( l.LATITUDE ) )* 
        cos( radians( l.LONGITUDE ) - radians([:longitude]) ) 
        + 
        sin( radians([:latitude]) ) * sin(radians(l.LATITUDE)) 
    )  AS distance
        from EVENTS e, LOCATIONS l 
        where 
            l.id = e.locationid 
        and (e.USERID = [:userid] or e.VISIBILITY = 1)
        AND l.GEINDEX LIKE [:tilerange]    
    ) as tmp1
    where tmp1.distance < [:distance]
    order by distance
) as tmp
where tmp.row_num < [:nrows]
order by tmp.row_num

