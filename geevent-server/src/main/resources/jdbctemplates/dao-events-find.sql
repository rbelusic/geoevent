select * from (
    select e.*,
        6371.00 * acos(
        cos( radians([:latitude]) ) * 
        cos( radians( l.LATITUDE ) )* 
        cos( radians( l.LONGITUDE ) - radians([:longitude]) ) 
        + 
        sin( radians([:latitude]) ) * sin(radians(l.LATITUDE)) 
    )  AS DISTANCE
    from EVENTS e, LOCATIONS l 
    where 
        l.ID = e.LOCATIONID 
    and (e.USERID = [:userid] or e.VISIBILITY = 1)
    AND l.GEINDEX LIKE [:tilerange]  
) as tmp
where tmp.distance < [:distance]
order by tmp.distance
limit 0, [:nrows]


























DEGREES(ACOS(
              COS(RADIANS([:latitude])) *
              COS(RADIANS([l.LATITUDE)) *
              COS(RADIANS(l.LONGITUDE) - RADIANS([:longitude])) +
              SIN(RADIANS([:latitude])) * SIN(RADIANS(l.LATITUDE))
            ))

SELECT id, ( 
6371.00 * acos(
    cos( radians([:latitude]) ) * 
    cos( radians( l.LATITUDE ) )* 
    cos( radians( l.LONGITUDE ) - radians([:longitude]) ) 
    + 
    sin( radians([:latitude]) ) * sin(radians(l.LATITUDE)) 
    ) 
) AS distance 
FROM markers 
HAVING distance < 25 
ORDER BY distance 
LIMIT 0 , 20;