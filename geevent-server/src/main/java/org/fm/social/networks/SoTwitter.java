package org.fm.social.networks;

import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;


/**
 *
 * @author RobertoB
 */
public class SoTwitter implements SocialNetwork {
    final static String propBase = SoTwitter.class.getPackage().getName() + ".twitter.";
    private static TwitterFactory factory = null;

    private static TwitterFactory getTwitterFactory() {
        if (factory == null) {
            factory = new TwitterFactory();
        }
        return factory;
    }

    private Twitter twitter = null;

    private Twitter getTwitter() {
        FmApplication app = FmApplication.instance();
        String consumerKey = app == null ? null : app.getApplicationProperty(propBase + "consumer.key");
        String consumerSecret = app == null ? null : app.getApplicationProperty(propBase + "consumer.secret");
        String accessToken = app == null ? null : app.getApplicationProperty(propBase + "access.token");
        String tokenSecret = app == null ? null : app.getApplicationProperty(propBase + "token.secret");
        
        
        if (twitter == null) {
            twitter = getTwitterFactory().getInstance();
            twitter.setOAuthConsumer(
                    consumerKey,consumerSecret
            );
            AccessToken accToken = new AccessToken(accessToken, tokenSecret);
            twitter.setOAuthAccessToken(accToken);
        }
        return twitter;
    }
    
    
    public ResponseList<Status> getUserTimeline(String uname) throws TwitterException {
        ResponseList<Status> response = getTwitter().getUserTimeline(uname);
        return response;
    }

    public QueryResult search(String q) throws TwitterException {
        Query query = new Query(q);
        query.count(100);
        QueryResult response = getTwitter().search(query);
        return response;
    }    
    
}
