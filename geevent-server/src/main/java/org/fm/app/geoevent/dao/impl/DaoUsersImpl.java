
package org.fm.app.geoevent.dao.impl;

import org.fm.dao.DaoObjectImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fm.app.geoevent.dao.DaoUsers;
import org.fm.app.geoevent.dm.DmRole;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.app.geoevent.dm.DmUserRole;


public class DaoUsersImpl extends DaoObjectImpl implements DaoUsers {
    
    @Override
    public DmUser get(String id) {
        List<DmUser> dmList = get(new String[]{id});
        
        return dmList.size() < 1 ? null : dmList.get(0);
    }

    @Override
    public List<DmUser> get(String[] ids) {
        List<DmUser> dmList = ctx().getStorageConnection().get(DmUser.class, ids);
        return dmList;
    }

    @Override
    public List<DmRole> getRoles(String userId) {
        final List<DmRole> allRoles = new ArrayList<DmRole>();
        
        for (DmRole role : _getUserRoles(userId)) {
            if (!_contains(allRoles,role)) {
                _getSubRoles(role, allRoles);
            }
        }

        return (allRoles);   
     }
    
    // -- private --------------------------------------------------------------
    private List<DmRole> _getUserRoles(String userId) {
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("userId", userId);

        List<DmUserRole> lstUserRoles = store().findBy(DmUserRole.class, "userId", userId);
        List<String> roleNames = new ArrayList<String>();
        for(DmUserRole usrRole: lstUserRoles) {
            roleNames.add(usrRole.getRole(""));
        }

        List<DmRole> lstRoles = store().findBy(DmRole.class, "id", roleNames);
        return lstRoles;
    }

    private List<DmRole> _getSubRoles(DmRole role, final List<DmRole> lstRoles) {
        lstRoles.add(role);

        List<DmRole> subRoles = store().find(
                DmRole.class,
                "select * from ROLES where ID in (select CHILD_ROLE from ROLES_HIERARCHY where ROLE = [:id])",
                role.getAttr()
        );

        for (DmRole srole : subRoles) {
            if (!_contains(lstRoles,srole)) {
                _getSubRoles(srole, lstRoles);                
            }
        }

        return lstRoles;
    }
    
    private boolean _contains(List<DmRole> allRoles, DmRole role) {
        for(DmRole r: allRoles) {
            if(r.getId("").equals(role.getId(""))) {
                return true;
            }
        }
        
        return false;
    }
}
