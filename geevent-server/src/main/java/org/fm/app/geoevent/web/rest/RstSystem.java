package org.fm.app.geoevent.web.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.fm.app.geoevent.bo.BoSystem;
import org.fm.app.geoevent.dm.DmSysInfo;
import org.fm.application.FmContext;
import org.fm.dm.DmFactory;



@Path("/sys")
public class RstSystem extends RstObject {

    @GET
    @Path("login")
    @Produces(APPLICATION_JSON_UTF8)
    public Response login(@QueryParam("username") String username, @QueryParam("password") String password) {
        return(
            Response.ok(
                bo(BoSystem.class).login(username, password)
            ).build()
        );
    }
    
    @GET
    @Path("logout")
    @Produces(APPLICATION_JSON_UTF8)
    public Response logout() {
        bo(BoSystem.class).logout();        
        return(Response.ok().build());
    }
    
    @GET
    @Path("info")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getBuildInfo() {
        return(
            Response.ok(ctx.getBuildInfo()).build()
        );
    }    
}
