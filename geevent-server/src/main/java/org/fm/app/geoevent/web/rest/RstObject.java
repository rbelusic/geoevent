package org.fm.app.geoevent.web.rest;

import org.fm.app.geoevent.web.GeServerSessionContext;
import org.fm.bo.BoObject;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;

import org.fm.http.FmHttpService;

public class RstObject extends FmHttpService {
    public final GeServerSessionContext ctx = (GeServerSessionContext) FmContext.getSessionBean(FmSessionContext.class);
    
    public <T extends BoObject> T bo(Class<T> c) {
        return FmApplication.instance().getBo(c);
    }
}


