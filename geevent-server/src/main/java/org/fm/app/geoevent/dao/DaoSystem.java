package org.fm.app.geoevent.dao;

import java.util.List;
import java.util.Set;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.dao.DaoObject;
import org.fm.dm.DmObject;

public interface DaoSystem extends DaoObject {
    public <T extends DmObject> List<T> getBy(Class<T> dmObjectClass,String attrName, Object attrValue); // from DaoObjectImpl.class
    
    public void putSession(DmSession session);
    public void updateSession(DmSession session, Set<String> attrs);
    public DmSession getSession(String sessionId); 
}
