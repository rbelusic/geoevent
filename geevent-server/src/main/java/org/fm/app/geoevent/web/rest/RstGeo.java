package org.fm.app.geoevent.web.rest;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.fm.app.geoevent.bo.BoOsm;
import org.fm.http.FmHttpService;
import org.fm.utils.StringUtil;

@Path("/geo")
public class RstGeo extends RstObject {

    @GET
    @Path("/search")
    @Produces(APPLICATION_JSON_UTF8)
    public Response search(
            @QueryParam("query") @DefaultValue("") String query,
            @QueryParam("countries") @DefaultValue("") String countries,
            @QueryParam("left") Double left,
            @QueryParam("top") Double top,
            @QueryParam("right") Double right,
            @QueryParam("bottom") Double bottom,
            @QueryParam("bounded")  @DefaultValue("false") Boolean bounded,
            @QueryParam("address") @DefaultValue("true") Boolean addressDetails,
            @QueryParam("tags") @DefaultValue("false") Boolean extraTags,
            @QueryParam("details") @DefaultValue("true") Boolean nameDetails,
            @QueryParam("polygon") @DefaultValue("false") Boolean polygon,
            @QueryParam("limit") Integer limit
    ) {
        return (Response.ok(
                bo(BoOsm.class).search(
                        query,
                        StringUtil.tokenizeStringToList(countries, ',', '\"'),
                        left, top, right, bottom,
                        bounded, addressDetails, extraTags, nameDetails, limit, polygon
                )
        ).build());
    }
    
    @GET
    @Path("/reverse")
    @Produces(APPLICATION_JSON_UTF8)
    public Response reverse(
            @QueryParam("lat") Double lat, 
            @QueryParam("lon") Double lon, 
            @QueryParam("zoom") @DefaultValue("18") Integer zoom,
            @QueryParam("address") @DefaultValue("true") Boolean addressDetails, 
            @QueryParam("tags") @DefaultValue("false") Boolean extraTags,
            @QueryParam("details") @DefaultValue("true") Boolean nameDetails,
            @QueryParam("osmtype") String osmType, 
            @QueryParam("osmid") Long osmId
    ) {
        return (Response.ok(
                bo(BoOsm.class).reverse(lat, lon , zoom, addressDetails, extraTags, nameDetails, osmType, osmId)                
        ).build());
    }
}
