package org.fm.app.geoevent.web;

import java.util.List;
import java.util.UUID;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.fm.app.geoevent.application.GeSessionContext;
import org.fm.app.geoevent.dao.impl.DaoUsersImpl;
import org.fm.app.geoevent.dm.DmRole;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.DmSysInfo;
import org.fm.application.FmApplication;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmFactory;

/**
 *
 * @author user
 */
public class GeServerSessionContext extends GeSessionContext {
    private ServletRequest servletRequest = null;
    private ServletResponse servletResponse = null;
    private String servletRequestToken = null;
    private String servletRequestIp = null;
    private List<DmRole> userRoles = null;


    /**
     * @return the servletRequest
     */
    public ServletRequest getServletRequest() {
        return servletRequest;
    }

    /**
     * @param servletRequest the servletRequest to set
     */
    public void setServletRequest(ServletRequest servletRequest) {
        this.servletRequest = servletRequest;
    }

    /**
     * @return the servletResponse
     */
    public ServletResponse getServletResponse() {
        return servletResponse;
    }

    /**
     * @param servletResponse the servletResponse to set
     */
    public void setServletResponse(ServletResponse servletResponse) {
        this.servletResponse = servletResponse;
    }

    /**
     * @return the servletRequestToken
     */
    public String getServletRequestToken() {
        return servletRequestToken;
    }

    /**
     * @param servletRequestToken the servletRequestToken to set
     */
    public void setServletRequestToken(String servletRequestToken) {
        this.servletRequestToken = servletRequestToken;
    }

    /**
     * @return the servletRequestIp
     */
    public String getServletRequestIp() {
        return servletRequestIp;
    }

    /**
     * @param servletRequestIp the servletRequestIp to set
     */
    public void setServletRequestIp(String servletRequestIp) {
        this.servletRequestIp = servletRequestIp;
    }
   

    // -- roles ----------------------------------------------------------------
    public List<DmRole> getUserRoles(String userId) {
        return userRoles;
    }

    public boolean checkUserRole(String roleid) {
        if (userRoles == null || getSessionInfo() == null) {
            return false;
        }

        for (DmRole role : userRoles) {
            if (role.getId("").equalsIgnoreCase(roleid)) {
                return true;
            }
        }
        return false;
    }

}
