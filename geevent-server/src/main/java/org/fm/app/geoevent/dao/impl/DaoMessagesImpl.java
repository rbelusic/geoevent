
package org.fm.app.geoevent.dao.impl;

import org.fm.dao.DaoObjectImpl;
import java.util.List;
import org.fm.app.geoevent.dao.DaoMessages;
import org.fm.app.geoevent.dm.event.DmMessage;


public class DaoMessagesImpl extends DaoObjectImpl implements DaoMessages {
    
    @Override
    public DmMessage get(String id) {
        List<DmMessage> dmList = get(new String[]{id});
        
        return dmList.size() < 1 ? null : dmList.get(0);
    }

    @Override
    public List<DmMessage> get(String[] ids) {
        List<DmMessage> dmList = ctx().getStorageConnection().get(DmMessage.class, ids);
        return dmList;
    }

    @Override
    public void put(DmMessage[] locations) {
        ctx().getStorageConnection().put(locations);        
    }
}
