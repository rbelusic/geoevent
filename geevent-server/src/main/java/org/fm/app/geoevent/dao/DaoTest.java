package org.fm.app.geoevent.dao;

import java.util.List;
import org.fm.dao.DaoObject;
import org.fm.dm.DmObject;

public interface DaoTest extends DaoObject {
    public <T extends DmObject> List<T> getBy(Class<T> dmObjectClass,String attrName, Object attrValue); // from DaoObjectImpl.class
    
    
}
