
package org.fm.app.geoevent.dao.impl;

import java.util.ArrayList;
import org.fm.dao.DaoObjectImpl;
import java.util.HashMap;
import java.util.List;
import org.fm.app.geoevent.application.GeSessionContext;
import org.fm.app.geoevent.dao.DaoEvents;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.dm.DmFactory;


public class DaoEventsImpl extends DaoObjectImpl implements DaoEvents {
    @Override
    public List<DmEvent> get(String[] ids) {
        final DmSession ses = ((GeSessionContext)ctx()).getSessionInfo();
        String userId = ses == null ? "" : ses.getUserId("");
        List<DmEvent> dmList = ctx().getStorageConnection().get(DmEvent.class, ids);
        List<DmEvent> dmListRetc = new ArrayList<DmEvent>();
        for(DmEvent e: dmList) {
            if(e.getVisibility(DmEvent.VISIBILITY_USER).equals(DmEvent.VISIBILITY_PUBLIC) || userId.equals(e.getUserId(userId))) {
                dmListRetc.add(e);
            }
        }
        return dmListRetc;
    }

    @Override
    public void put(DmEvent[] events) {
        ctx().getStorageConnection().put(events);
    }
    
    
    @Override
    public List<DmEvent> findNearestEvents(final Double lat, final Double lon, final Double distance, final int nrows, final String[] tagsArray, final String userId) {
        List<DmEvent> dmList = ctx().getStorageConnection().find(
            DmEvent.class, 
            "dao-events-find.sql" , 
            new HashMap<String,Object> () {{
                put("userid",userId);
                put("latitude",lat);
                put("longitude",lon);
                put("distance",distance/1000D);
                put("tags",tagsArray);                
                put("nrows",nrows);
            }}
        );
        
        return dmList;        
    }
    
    
    @Override
    public List<DmEvent> findNearestEventsByTileRange(final Double lat, final Double lon, final Double distance, final String tileRange, final int nrows, final String[] tagsArray, final String userId) {
        final DmSession ses = ((GeSessionContext)ctx()).getSessionInfo();
        
        List<DmEvent> dmList = ctx().getStorageConnection().find(
            DmEvent.class, 
            "dao-events-find-by-range.sql" , 
            new HashMap<String,Object> () {{
                put("userid",userId);
                put("latitude",lat);
                put("longitude",lon);
                put("distance",distance/1000D);
                put("tilerange",/*tileRange +*/ "%");
                put("tags",tagsArray);                
                put("nrows",nrows);
            }}
        );
        
        return dmList;        
    }
}
