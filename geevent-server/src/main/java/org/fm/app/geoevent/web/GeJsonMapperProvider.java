package org.fm.app.geoevent.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import org.fm.app.geoevent.util.rs.GeObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
 @Produces(MediaType.APPLICATION_JSON)
 @Consumes(MediaType.APPLICATION_JSON)
public class GeJsonMapperProvider implements ContextResolver<ObjectMapper> {
    private static Logger L = LoggerFactory.getLogger(GeJsonMapperProvider.class);
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static GeObjectMapper dmMapper = null;
    public GeJsonMapperProvider() {
        L.info("Init" );
    }
    
    @Override
    public GeObjectMapper getContext(Class<?> type) {
        L.info("Start" );
        if (dmMapper == null) {
            dmMapper = new GeObjectMapper();
        }
        L.info("End" );
        return dmMapper;
    }
    
}
