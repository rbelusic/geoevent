
package org.fm.app.geoevent.bo.impl;

import org.fm.app.geoevent.web.GeServerSessionContext;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.bo.BoObject;
import org.fm.dao.DaoObject;

public class BoObjectImpl implements BoObject {    
    @Override
    public String getID() {
        return(getClass().getSimpleName() + "-ID:" + Thread.currentThread().getId());
        
    }
    
    public GeServerSessionContext ctx() {
        return (GeServerSessionContext) FmContext.getSessionBean(FmSessionContext.class);
    }
        
    public <T extends DaoObject> T dao(Class<T> c) {
        return FmApplication.instance().getDao(c);
    }
}
