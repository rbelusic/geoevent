package org.fm.app.geoevent.web;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
//import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeApplication extends ResourceConfig  {
    private static Logger L = LoggerFactory.getLogger(GeApplication.class);

    public GeApplication() {
        L.info("start" );
        property(ServerProperties.TRACING, "ALL");
        
        // init REST & JSON
        //register(JacksonFeature.class);
        packages("com.fasterxml.jackson.jaxrs.json");
        
        
        register(JacksonJsonProvider.class);
        register(MultiPartFeature.class);
        
        /*
        register(GeMapperProvider.class);
        register(LoggingFilter.class);
        */
        // REST pkgs
        //packages("org.fm.app.geoevent.web.rest.impl");
        L.info("end" );
                
    }
}
