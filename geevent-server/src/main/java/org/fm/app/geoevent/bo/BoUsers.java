package org.fm.app.geoevent.bo;

import org.fm.app.geoevent.dm.DmUser;
import java.util.List;
import org.fm.app.geoevent.dm.DmRole;
import org.fm.bo.BoObject;
import org.fm.bo.annotations.BoRoles;

public interface BoUsers extends BoObject {
    @BoRoles(roles={"USER"})
    public List<DmUser> getUsers(String [] ids);    

    @BoRoles(roles={"ADMIN"})
    public List<DmRole> getUserRoles(String userId);
}
