package org.fm.app.geoevent.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.fm.FmException;
import org.fm.app.geoevent.dao.DaoSystem;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmFactory;
import org.slf4j.LoggerFactory;

/**
 * Za sada samo ubacujemo servlet config, request i response u context. 
 * TODO:
 provjera tokena, povuci trenutnog usera te ubaciti i njega u GeoEventSession
 *
 */
public class GeContextLoader implements Filter {
    private static org.slf4j.Logger L = LoggerFactory.getLogger(GeContextLoader.class);
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        L.info("Init");
        // kreiraj novu app        
        ServletContext ctx = filterConfig.getServletContext();        
        String appName = ctx.getContextPath().startsWith("/") ?
                ctx.getContextPath().substring(1) :
                ctx.getContextPath();
        
        
        try {
            FmApplication.instance(FmApplication.class, appName);     
        } catch (InstantiationException ex) {
            Logger.getLogger(GeContextLoader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GeContextLoader.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        L.info("doFilter");
        
        // session
        GeServerSessionContext sessionCtx = new GeServerSessionContext();
        
        sessionCtx.setServletRequest(request);
        sessionCtx.setServletResponse(response);
        sessionCtx.setServletRequestToken(((HttpServletRequest) request).getHeader("X-gevt-token"));
        sessionCtx.setServletRequestIp(((HttpServletRequest) request).getRemoteAddr());
        
        DmSession geSession = DmFactory.create(DmSession.class);
        
        if(sessionCtx.getServletRequestToken() != null && !sessionCtx.getServletRequestToken().isEmpty()) {
            DaoSystem daoSys = FmApplication.instance().getDao(DaoSystem.class);            
            geSession = daoSys.getSession(sessionCtx.getAttr("servletRequestToken","").toString());
            
            if(geSession == null || !geSession.getIp("").equals(sessionCtx.getAttr("servletRequestIp","").toString())) {
                throw new FmException("FE0002","Invalid authorisation token.");
            }
            if(geSession.getClosedAt(null) != null) {
                throw new FmException("FE0002","Authorisation expired.");                
            }
            geSession.setTimestamp(new Date());
            daoSys.updateSession(geSession,null);
            sessionCtx.setSessionInfo(geSession);
        } else {
        }
        FmContext.addSessionBean(FmSessionContext.class, sessionCtx);

 // Read from request
        /*
    StringBuilder buffer = new StringBuilder();
    BufferedReader reader = request.getReader();
    String line;
    while ((line = reader.readLine()) != null) {
        buffer.append(line);
    }
    String data = buffer.toString();
    L.info("REQDATA[" + data + "]");
    */
        // Pass request back down the filter chain
        chain.doFilter(request, response);
                
        // cleanup session
        sessionCtx.destroy();
    }

    @Override
    public void destroy() {
        L.info("destroy");
    }

}
