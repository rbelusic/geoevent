package org.fm.app.geoevent.bo.impl;

import java.util.List;
import org.fm.app.geoevent.bo.BoUsers;
import org.fm.app.geoevent.dao.DaoUsers;
import org.fm.app.geoevent.dm.DmRole;
import org.fm.app.geoevent.dm.DmUser;

/**
 *
 * @author RobertoB
 */
public class BoUsersImpl extends BoObjectImpl implements BoUsers {

    /**
     * Get list of users
     *
     * @param ids
     * @return
     */
    @Override
    public List<DmUser> getUsers(String[] ids) {
        List<DmUser> dmList = dao(DaoUsers.class).get(ids);
        return dmList;
    }

    @Override
    public List<DmRole> getUserRoles(String userId) {
        List<DmRole> dmList = dao(DaoUsers.class).getRoles(userId);
        return dmList;
    }
   
    
}
