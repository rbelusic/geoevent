
package org.fm.app.geoevent.dao.impl;

import org.fm.dao.DaoObjectImpl;
import java.util.ArrayList;
import java.util.List;
import org.fm.app.geoevent.dao.DaoLocations;
import org.fm.app.geoevent.dm.event.DmLocation;


public class DaoLocationsImpl extends DaoObjectImpl implements DaoLocations {
    
    @Override
    public DmLocation get(String id) {
        List<DmLocation> dmList = get(new String[]{id});
        
        return dmList.size() < 1 ? null : dmList.get(0);
    }

    @Override
    public List<DmLocation> get(String[] ids) {
        List<DmLocation> dmList = ctx().getStorageConnection().get(DmLocation.class, ids);
        return dmList;
    }

    @Override
    public void put(DmLocation[] locations) {
        ctx().getStorageConnection().put(locations);        
    }
}
