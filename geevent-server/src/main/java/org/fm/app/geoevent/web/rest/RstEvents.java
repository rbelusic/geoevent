package org.fm.app.geoevent.web.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.fm.FmException;
import org.fm.app.geoevent.bo.BoEvents;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.application.FmApplication;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("/events")
public class RstEvents extends RstObject {

    @GET
    @Path("/get/{ids}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response get(@PathParam("ids") String ids) {
        return (Response.ok(
                bo(BoEvents.class).getEvents(getIdsArray(ids))
        ).build());
    }

    @GET
    @Path("/find")
    @Produces(APPLICATION_JSON_UTF8)
    public Response find(
            @QueryParam("lat") Double lat,
            @QueryParam("lon") Double lon,
            @QueryParam("distance") @DefaultValue("1000") Double distance,
            @QueryParam("tags") @DefaultValue("") String tags,
            @QueryParam("nrows") @DefaultValue("20") Integer nrows
    ) {
        return (Response.ok(
                bo(BoEvents.class).findNearestEvents(lat, lon, distance, getIdsArray(tags), nrows)
        ).build());
    }

    @POST
    @Path("/add")
    @Produces(APPLICATION_JSON_UTF8)
    @Consumes(APPLICATION_JSON_UTF8)
    public Response add(DmEvent[] events) {
        return (Response.status(Response.Status.CREATED).entity(
                bo(BoEvents.class).addEvents(events)
        ).build());
    }

    @POST
    @Path("/{eventId}/photo")
    @Consumes(MULTIPART_FORM_DATA)
    public Response uploadEventPhoto(
            @PathParam("eventId") String eventId,
            @FormDataParam("file") File file,
            @FormDataParam("file") FormDataContentDisposition fileDetail
    ) {
        // get event
        if (bo(BoEvents.class).getEvents(new String[]{eventId}).size() != 1) {
            throw new FmException("Event not found.");
        }

        String dataPath = FmApplication.instance().getApplicationProperty("fm.app.geoevent.data.path").trim();
        if (dataPath == null || dataPath.isEmpty()) {
            dataPath = File.separator + "tmp";
        }
        if (!dataPath.endsWith(File.separator)) {
            dataPath += File.separator;
        }

        // file type        
        String fileName = "";
        try {
            fileName = dataPath + eventId + "." + getPhotoFileType(fileDetail.getFileName());
        } catch (Exception e) {
            fileName = dataPath + eventId + ".png";
        }

        // save
        try {
            // save file to the server
            File srvFile = new File(fileName);
            if (srvFile.exists()) {
                srvFile.delete();
            }
            OutputStream outpuStream = new FileOutputStream(srvFile);
            int read = 0;
            byte[] bytes = new byte[1024];
            outpuStream = new FileOutputStream(new File(fileName));
            FileInputStream fileInputStream = new FileInputStream(file);
            while ((read = fileInputStream.read(bytes)) != -1) {
                outpuStream.write(bytes, 0, read);
            }
            fileInputStream.close();
            outpuStream.flush();
            outpuStream.close();
        } catch (Exception e) {
            throw new FmException("Error saving event photo", fileName, e);
        }

        return Response.status(Response.Status.CREATED).build();
    }

    private String getPhotoFileType(String fname) {
        String ext = (fname.lastIndexOf(".") < 0 ? fname.substring(fname.lastIndexOf(".") + 1) : "png").trim();
        return ext.isEmpty() ? "png" : ext;
    }

    @GET
    @Path("/check")
    @Produces(APPLICATION_JSON_UTF8)
    public Response check() {
        bo(BoEvents.class).check();
        return (Response.ok().build());
    }

}
