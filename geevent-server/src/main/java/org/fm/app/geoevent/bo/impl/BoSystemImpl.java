package org.fm.app.geoevent.bo.impl;

import java.util.Date;
import java.util.List;
import org.fm.FmException;
import org.fm.app.geoevent.bo.BoSystem;
import org.fm.app.geoevent.dao.DaoSystem;
import org.fm.app.geoevent.dao.DaoUsers;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.DmSysInfo;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.application.FmContext;
import org.fm.dm.DmFactory;
import org.fm.security.Md5Authorizer;
import org.fm.utils.StringUtil;

public class BoSystemImpl extends BoObjectImpl implements BoSystem {

    @Override
    public DmSession getSession(String id) {
        DmSession dm
                = dao(DaoSystem.class).getSession(id);
        return dm;
    }

    @Override
    public void setSession(DmSession s) {
        dao(DaoSystem.class).putSession(s);
    }

    @Override
    public DmSession login(final String username, final String password) {
        List<DmUser> dmList
                = dao(DaoUsers.class).getBy(DmUser.class,"title",username);        
        if(dmList.size() != 1) {
            throw new FmException("Unknown user");            
        } 
        DmUser user = dmList.get(0);
        String md5pass = StringUtil.byteArrayToHexString(Md5Authorizer.createMd5sum(password));
        if(!user.getPassword("").equalsIgnoreCase(md5pass)) {
            throw new FmException("Invalid password");
        }
        
        DmSession session = DmFactory.create(DmSession.class);
        session.setId(ctx().generateRequestToken());
        session.setUserId(user.getId(null));
        session.setIp(ctx().getServletRequestIp());        
        session.setTimestamp(new Date());     
        session.setUser(dao(DaoUsers.class).get(user.getId("")));
        dao(DaoSystem.class).putSession(session);
        session = getSession(session.getId(""));
        ctx().setSessionInfo(session);
        
        return session;
    }

    @Override
    public void logout() {
        DmSession session = ctx().getSessionInfo();
        if(session != null) {
            session.setClosedAt(new Date());
            dao(DaoSystem.class).updateSession(session,null);
            ctx().setSessionInfo(null);
        }
    }

    @Override
    public DmSysInfo getBuildInfo() {
        return ctx().getBuildInfo();
    }
}
