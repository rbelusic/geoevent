package org.fm.app.geoevent.dao;

import java.util.List;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.dao.DaoObject;
import org.fm.dm.DmObject;

public interface DaoEvents extends DaoObject {
    public <T extends DmObject> List<T> getBy(Class<T> dmObjectClass,String attrName, Object attrValue); // from DaoObjectImpl.class
    public <T extends DmObject> T get(Class<T> dmObjectClass,String id);
    public <T extends DmObject> List<T> get(Class<T> dmObjectClass,String [] ids);
    public <T extends DmObject> void put(T [] dms);   
    
    public List<DmEvent> get(String [] ids);
    public void put(DmEvent [] events);

    public List<DmEvent> findNearestEvents(final Double lat, final Double lon, final Double distance, final int nrows, final String[] tagsArray, final String userId);
    public List<DmEvent> findNearestEventsByTileRange(final Double lat, final Double lon, final Double distance, final String tileRange, final int nrows, final String[] tagsArray, final String userId);

}
