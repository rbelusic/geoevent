package org.fm.app.geoevent.bo;

import java.util.List;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.bo.BoObject;
import org.fm.bo.annotations.BoRoles;

public interface BoEvents extends BoObject {
    @BoRoles(roles={"USER"})
    public List<DmEvent> getEvents(String [] ids);    

    @BoRoles(roles={"USER"})
    public List<DmEvent> addEvents(DmEvent [] events);

    public List<DmEvent> findNearestEvents(Double lat, Double lon, Double distance, String[] tagsArray, int nrows);

    public void check();
}
