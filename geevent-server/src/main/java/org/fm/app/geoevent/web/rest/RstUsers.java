package org.fm.app.geoevent.web.rest;

import org.fm.app.geoevent.bo.BoUsers;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;


@Path("/users")
public class RstUsers extends RstObject {

    @GET
    @Path("get/{ids}")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getUsers(@PathParam("ids") String ids) {        
        return(
            Response.ok(
                bo(BoUsers.class).getUsers(getIdsArray(ids))
            ).build()
        );
    }    

    @GET
    @Path("get/roles")
    @Produces(APPLICATION_JSON_UTF8)
    public Response getUserRoles() {        
        return(
            Response.ok(bo(BoUsers.class).getUserRoles(ctx.getUserId())
            ).build()
        );
    }    
}
