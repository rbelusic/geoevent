package org.fm.app.geoevent.apis.osm;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fm.FmException;
import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.app.geoevent.util.rs.GeObjectMapper;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.http.client.FmHttpClient;
import org.fm.http.client.FmHttpParamList;
import org.fm.http.client.FmHttpResponse;
import org.fm.utils.Base64;


/**
 *
 * @author user
 */
public class NominatimApiClient {        
    final static String propBase = NominatimApiClient.class.getPackage().getName() + ".";

    private final String apiURL = "http://nominatim.openstreetmap.org/";
    private final FmHttpClient restClient;
    private final String email =  
        FmApplication.instance()== null ? 
        "" : FmApplication.instance().getApplicationProperty(propBase + "email")
        ;
    private final String language;
    private GeObjectMapper objectMapper;
    
    public <T extends FmHttpClient> NominatimApiClient(T cl,String lang) {
        language = lang;
        restClient = cl;
    }

    public String getApiURL() {
        return apiURL;
    }

    public FmHttpResponse invoke(FmHttpClient.REQUEST_TYPE method, String endpoint, Map<String, Object> params) {
        Map<String, String> headers = new HashMap<String, String>();
        addCommonHeaders(headers);

        try {
            return restClient.invoke(method, getApiURL() + endpoint,
                    "application/json; charset=utf-8",
                    new FmHttpParamList(params),
                    headers
            );
        } catch (Exception ex) {
            throw new FmException("HTTP error", ex);
        }
    }

    // -- API calls 
    public <T extends NomPlace> List<T> search(
            final String query, final List<String> countryCodes,
            final Double left, final Double top, 
            final Double right, final Double bottom, 
            final Boolean bounded, 
            final Boolean addressDetails, final Boolean extraTags, final Boolean nameDetails,
            final Integer limit,final Boolean polygon
    ) throws IOException {
        Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("format", "json");
                if(email != null)  put("email", email);
            }
        };
        String ccStr = listToString(countryCodes);
        
        params.put("q", query);
        params.put("countrycodes", ccStr);
        if (addressDetails != null) {
            params.put("addressdetails", addressDetails ? "1" : "0");
        }

        if (extraTags != null) {
            params.put("extratags", extraTags ? "1" : "0");
        }

        if (nameDetails != null) {
            params.put("namedetails", nameDetails ? "1" : "0");
        }

        if (limit != null) {
            params.put("limit", limit);
        }
        
        if (bounded != null) {
            params.put("bouded", bounded ? "1" : "0");
        }

        if (polygon != null) {
            params.put("polygon_geojson", polygon ? "1" : "0");
        }

        if(left != null && top != null && right != null && bottom != null) {
            params.put(
                "viewbox", 
                left.toString() + "," + 
                top.toString() + "," + 
                right.toString() + "," + 
                bottom.toString()
            );
        }

        FmHttpResponse response = invoke(FmHttpClient.REQUEST_TYPE.GET, "search",
                params
        );
        System.out.println(" response --> " + new String(response.getResponse()));
        return (List<T>) responseToElementsArray(response,NomPlace.class);

    }

    public <T extends NomPlace> List<T> addressLookup(
            final String type, final Integer id,
            final Boolean addressDetails, final Boolean extraTags, final Boolean nameDetails) throws IOException {
        
        return addressLookup(new HashMap<String, Integer>() {
            {
                put(type, id);                
            }
            },
            addressDetails, extraTags, nameDetails
        );

    }

    public <T extends NomPlace> List<T> addressLookup(
            final Map<String, Integer> ids,
            final Boolean addressDetails, final Boolean extraTags, final Boolean nameDetails
    ) throws IOException {
        Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("format", "json");
                if(email != null)  put("email", email);
            }
        };
        String idsStr = "";
        for (Map.Entry<String, Integer> id : ids.entrySet()) {
            idsStr += (!idsStr.isEmpty() ? "," : "") + id.getKey().substring(0, 1).toUpperCase() + id.getValue().toString();
        }
        params.put("osm_ids", idsStr);
        if (addressDetails != null) {
            params.put("addressdetails", addressDetails ? "1" : "0");
        }

        if (extraTags != null) {
            params.put("extratags", extraTags ? "1" : "0");
        }

        if (nameDetails != null) {
            params.put("namedetails", nameDetails ? "1" : "0");
        }

        return (List<T>) responseToElementsArray(invoke(FmHttpClient.REQUEST_TYPE.GET, "lookup",
                params
        ),NomPlace.class);
    }

    public <T extends NomPlace> T reverseGeocode(
            final Double lat, final Double lon, final Integer zoom,
            final Boolean addressDetails, final Boolean extraTags, final Boolean nameDetails,
            final String osmType, final Long osmId
    ) throws IOException {
        Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("format", "json");
                if(email != null)  put("email", email);
            }
        };

        if (lat != null && lon != null) {
            params.put("lat", lat);
            params.put("lon", lon);
        }
        if (zoom != null) {
            params.put("zoom", zoom);
        }
        if (addressDetails != null) {
            params.put("addressdetails", addressDetails ? "1" : "0");
        }

        if (extraTags != null) {
            params.put("extratags", extraTags ? "1" : "0");
        }

        if (nameDetails != null) {
            params.put("namedetails", nameDetails ? "1" : "0");
        }

        if (osmType != null && !osmType.isEmpty() && osmId != null) {
            params.put("osm_type", osmType.substring(0, 1).toUpperCase());
            params.put("osm_id", osmId);
        }

        return (T) responseToElement(invoke(FmHttpClient.REQUEST_TYPE.GET, "reverse",
                params
        ), NomPlace.class);
    }


    // util
    private Map<String, String> addCommonHeaders(Map<String, String> headers) {
        FmApplication app = FmApplication.instance();
        String userName = app == null ? null : app.getApplicationProperty(propBase + "user");
        String userPassword = app == null ? null : app.getApplicationProperty(propBase + "password");
        
        if (userName != null && !userName.isEmpty()) {
            headers.put("Authorization", Base64.encodeBytes((userName + ":" + (userPassword == null ? "" : userPassword)).getBytes()));
        }
        headers.put("Accept-Language", language == null || language.isEmpty() ? "en" : language + ";en" );
        
        return headers;
    }


    private <T extends Object> String listToString(List<T> lst) {
        if(lst == null) {
            return "";
        }
        return arrayToString(lst.toArray(new Object[lst.size()]));
    }
    
    private String arrayToString(Object[] lst) {
        String s = "";
        if(lst == null) {
            return s;
        }
        for (Object e : lst) {
            s += (s.isEmpty() ? "" : ",") + e;
        }

        return s;
    }

    private <T extends NomPlace> List<T> responseToElementsArray(FmHttpResponse resp,Class<T> dmClass) throws IOException {
        List<T> lst = getObjectMapper().readListOfValues(new String(resp.getResponse()),dmClass);
        return lst; 
    }

    private <T extends NomPlace> T responseToElement(FmHttpResponse resp, Class<T> dmClass) throws IOException {
        T dm = getObjectMapper().readValue(new String(resp.getResponse()), dmClass);
        return dm;
    }   
    

    private  GeObjectMapper getObjectMapper() {
        if(objectMapper == null) {
            objectMapper = new GeObjectMapper();
        }
        return objectMapper;
    }        
}
