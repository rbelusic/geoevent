package org.fm.app.geoevent.web;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.fm.FmException;
import org.fm.app.geoevent.dm.DmException;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;

import org.fm.dm.DmFactory;

@Provider
public class GeExceptionMapper implements ExceptionMapper<FmException> {
    public GeServerSessionContext ctx() {
        return (GeServerSessionContext) FmContext.getSessionBean(FmSessionContext.class);
    }
    
    @Override
    public Response toResponse(FmException ex) {
        HttpServletRequest sc = (HttpServletRequest) ctx().getServletRequest();
        
        
        DmException de = DmFactory.create(DmException.class);
        de.setId(ex.getErrorCode());
        de.setDescription(ex.getUserMessage());
        de.setTimestamp(new Date());
        de.setRequestPath(
            sc.getMethod() + " " +
            sc.getRequestURI().substring(sc.getContextPath().length()) +
            (sc.getQueryString() == null ? "" : "?" + sc.getQueryString())
        );
        de.setBuildInfo(ctx().getBuildInfo());
        
        return Response.status(500)
                .entity(de)
                .type(MediaType.APPLICATION_JSON).
                build();
    }
}
