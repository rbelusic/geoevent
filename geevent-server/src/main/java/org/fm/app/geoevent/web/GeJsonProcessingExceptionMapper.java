package org.fm.app.geoevent.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.fm.app.geoevent.dm.DmException;
import org.fm.app.geoevent.web.rest.RstSystem;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmFactory;

@Provider
public class GeJsonProcessingExceptionMapper implements ExceptionMapper<JsonProcessingException> {
    public GeServerSessionContext ctx() {
        return (GeServerSessionContext) FmContext.getSessionBean(FmSessionContext.class);
    }
    
    @Override
    public Response toResponse(JsonProcessingException ex) {
        HttpServletRequest sc = (HttpServletRequest) ctx().getServletRequest();
        
        
        DmException de = DmFactory.create(DmException.class);
        de.setId("FE9999");
        de.setDescription(ex.getMessage());
        de.setTimestamp(new Date());
        de.setRequestPath(
            sc.getMethod() + " " +
            sc.getRequestURI().substring(sc.getContextPath().length()) +
            (sc.getQueryString() == null ? "" : "?" + sc.getQueryString())
        );
        de.setBuildInfo(ctx().getBuildInfo());
        
        return Response.status(500)
                .entity(de)
                .type(RstSystem.APPLICATION_JSON_UTF8).
                build();
    }
}
