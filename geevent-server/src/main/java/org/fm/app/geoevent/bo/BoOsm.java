package org.fm.app.geoevent.bo;

import java.io.IOException;
import java.util.List;
import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.bo.BoObject;
import org.fm.bo.annotations.BoRoles;

public interface BoOsm extends BoObject {
    @BoRoles(roles={"USER"})
    public <T extends NomPlace> T[] search(
            final String query, final List<String> countryCodes,
            final Double left, final Double top, 
            final Double right, final Double bottom, 
            final Boolean bounded, 
            final Boolean addressDetails, final Boolean extraTags, final Boolean nameDetails,
            final Integer limit,final Boolean polygon
    );
    
    @BoRoles(roles={"USER"})
    public <T extends NomPlace> T reverse(
            final Double lat, final Double lon, final Integer zoom,
            final Boolean addressDetails, final Boolean extraTags, final Boolean nameDetails,
            final String osmType, final Long osmId
    );
}
