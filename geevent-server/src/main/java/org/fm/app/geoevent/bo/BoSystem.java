
package org.fm.app.geoevent.bo;

import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.DmSysInfo;
import org.fm.bo.BoObject;


public interface BoSystem extends BoObject {
    public DmSession getSession(String id);    
    public void setSession(DmSession s);
    public DmSession login(final String uname, final String password);
    public void logout();
    
    public DmSysInfo getBuildInfo();
}
