package org.fm.app.geoevent.bo.impl;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.FmException;
import org.fm.app.geoevent.apis.osm.NominatimApiClient;
import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.app.geoevent.bo.BoOsm;
import org.fm.http.client.FmHttpClient;

/**
 *
 * @author RobertoB
 */
public class BoOsmImpl extends BoObjectImpl implements BoOsm {
   
    private NominatimApiClient cli = null;
    
    private  NominatimApiClient getClient() {
        if(cli == null) {
            cli = new NominatimApiClient(ctx().getHttpClientInstance(),"hr");
        }
        return cli;
    }
    
    @Override
    public <T extends NomPlace> T[] search(
            String query, 
            List<String> countryCodes, 
            Double left, Double top, Double right, Double bottom, 
            Boolean bounded, Boolean addressDetails, Boolean extraTags, Boolean nameDetails, 
            Integer limit, Boolean polygon
    ) {
        List<NomPlace> result;
        try {
            result = getClient().search(
                    query, countryCodes,left,top,right,bottom,
                    bounded, addressDetails, extraTags, nameDetails,
                    limit, polygon
            );
        } catch (IOException ex) {
            throw new FmException("", ex);
        }
        return (T[]) result.toArray(new NomPlace[result.size()]);
    }

    @Override
    public <T extends NomPlace> T reverse(
            final Double lat, final Double lon, final Integer zoom,
            final Boolean addressDetails, final Boolean extraTags, final Boolean nameDetails,
            final String osmType, final Long osmId
    ) {
        T result;
        try {
            result = getClient().reverseGeocode(lat, lon, zoom, addressDetails, extraTags, nameDetails, osmType, osmId);
        } catch (IOException ex) {
            throw new FmException("", ex);
        }
        return (result);
    }

}
