package org.fm.app.geoevent.dao.impl;

import org.fm.dao.DaoObjectImpl;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.fm.app.geoevent.dao.DaoSystem;
import org.fm.app.geoevent.dm.DmSession;

public class DaoSystemImpl extends DaoObjectImpl implements DaoSystem {

    @Override
    public void putSession(DmSession session) {
        store().put(session);
    }

    @Override
    public void updateSession(DmSession session, Set<String> attrs) {
        store().update(session,attrs);
    }

    @Override
    public DmSession getSession(String sessionId) {
        DmSession dm = store().get(DmSession.class, sessionId);
        return dm;
    }    
}
