package org.fm.app.geoevent.dao;

import java.util.List;
import org.fm.app.geoevent.dm.event.DmMessage;
import org.fm.dao.DaoObject;

public interface DaoMessages extends DaoObject {   
    public DmMessage get(String id);
    public List<DmMessage> get(String [] ids);
    public void put(DmMessage [] msgs);

}
