package org.fm.app.geoevent.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fm.app.geoevent.dm.event.DmLocation;
import org.fm.dao.DaoObject;
import org.fm.dm.DmObject;

public interface DaoLocations extends DaoObject {   
    public DmLocation get(String id);
    public List<DmLocation> get(String [] ids);
    public void put(DmLocation [] locations);
    public <T extends DmObject> void update(T [] dms, Set<String> columns);    
    public <T extends DmObject> List<T> find(Class<T> dmObjectClass,String sql,Map<String, Object> params);
}
