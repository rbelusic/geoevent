package org.fm.app.geoevent.dao;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fm.FmException;
import org.fm.app.geoevent.web.GeServerSessionContext;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.bo.annotations.BoRoles;
import org.fm.proxy.GenericInterceptor;


public class DaoInterceptor implements GenericInterceptor {
    private static Logger L = FmContext.getLogger(DaoInterceptor.class);

    private static String getMethodName(Method method) {
        return method.getDeclaringClass().getSimpleName() + "." + method.getName();
    }
    
    private Map<Method,List<String>> roles = new HashMap<Method, List<String>>();
    
    @Override
    public void before(Method method, Object[] args) {
        L.log(Level.INFO,"(before) {0}({1}) ", new Object[]{getMethodName(method), args});
        if(!checkMethodAthorisation(method)) {        
            throw new FmException("Not autorized");
        }
    }

    @Override
    public void after(Method method, Object[] args, Object response) {        
        L.log(Level.INFO, "(after) {0}({1}) = {2}", new Object[]{getMethodName(method), args, response});
    }
        
    // priv
    private List<String> getMethodRoles(Method m) {
        if(roles.get(m) == null) {
            BoRoles ann = (BoRoles)m.getAnnotation(BoRoles.class);
            if(ann != null) {
                roles.put(m, Arrays.asList(ann.roles()));
            }
            roles.put(m, new ArrayList<String>());
        }
        
        return(roles.get(m));
        
    }
    
    private boolean checkMethodAthorisation(Method method) {
        List<String> mroles = getMethodRoles(method);
        if(mroles.size() < 1) {
            return true;
        }
        
        GeServerSessionContext ctx = (GeServerSessionContext) FmContext.getSessionBean(FmSessionContext.class);        
        for(String rname: getMethodRoles(method)) {
            if(ctx.checkUserRole(rname)) return true;                
        }
        
        return false;
    }
    
}
