package org.fm.app.geoevent.bo.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.fm.FmException;
import org.fm.app.geoevent.bo.BoEvents;
import org.fm.app.geoevent.dao.DaoEvents;
import org.fm.app.geoevent.dao.DaoLocations;
import org.fm.app.geoevent.dao.DaoMessages;
import org.fm.app.geoevent.dao.DaoUsers;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.app.geoevent.dm.DmUtil;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.app.geoevent.dm.event.DmLocation;
import org.fm.app.geoevent.dm.event.DmMessage;
import org.fm.app.geoevent.util.GeoUtil;
import org.fm.dm.DmFactory;

/**
 *
 * @author RobertoB
 */
public class BoEventsImpl extends BoObjectImpl implements BoEvents {
    @Override
    public List<DmEvent> getEvents(String[] ids) {
        DaoEvents daoEv = dao(DaoEvents.class);
        DaoUsers daoUsr = dao(DaoUsers.class);
        DaoLocations daoLoc = dao(DaoLocations.class);
        DaoMessages daoMsg = dao(DaoMessages.class);
        
        Map<String,DmUser> usrCache = new HashMap<String,DmUser>();
        Map<String,DmLocation> locCache = new HashMap<String,DmLocation>();
        Map<String,DmMessage> msgCache = new HashMap<String,DmMessage>();
        
        // all events
        List<DmEvent> dmListRead = daoEv.get(ids);
        List<DmEvent> dmList = new ArrayList<DmEvent>();
        for(DmEvent e: dmListRead) {
            if(e.getUserId("").equals(ctx().getUserId()) || e.getVisibility(DmEvent.VISIBILITY_USER).equals(DmEvent.VISIBILITY_PUBLIC)) {
                dmList.add(e);
            }
        }
        
        // fill id's
        for(DmEvent event: dmList) {
            usrCache.put(event.getUserId(""), null);
            locCache.put(event.getLocationId(""), null);
            msgCache.put(event.getMessageId(""), null);            
        }

        // fetch
        List<DmLocation> locList = daoLoc.get(locCache.keySet().toArray(new String[locCache.size()]));
        for(DmLocation o: locList) {
            locCache.put(o.getId(""), o);
            usrCache.put(o.getUserId(""),null);
        }
        List<DmMessage> msgList = daoMsg.get(msgCache.keySet().toArray(new String[msgCache.size()]));
        for(DmMessage o: msgList) {
            msgCache.put(o.getId(""), o);
            usrCache.put(o.getUserId(""),null);
        }

        List<DmUser> usrList = daoUsr.get(usrCache.keySet().toArray(new String[usrCache.size()]));
        for(DmUser o: usrList) {
            usrCache.put(o.getId(""), o);
        }

        
        // fill user transients on loc and msg
        for(DmLocation o: locList) {
            o.setUser(usrCache.get(o.getUserId("")));
        }
        
        for(DmMessage o: msgList) {
            o.setUser(usrCache.get(o.getUserId("")));
        }
                
        for(DmEvent o: dmList) {
            o.setUser(usrCache.get(o.getUserId("")));
            o.setLocation(locCache.get(o.getLocationId("")));
            o.setMessage(msgCache.get(o.getMessageId("")));
        }
        
        return dmList;    
    }

    @Override
    public List<DmEvent> addEvents(DmEvent[] events) {
        List<String> evIds = new ArrayList<String>();
        List <DmLocation> locList = new ArrayList<DmLocation>();
        List <DmMessage> msgList = new ArrayList<DmMessage>();
        
        for(DmEvent event: events) {
            event.setId(event.getID());
            evIds.add(event.getId(""));
            
            event.setTimestamp(new Date());
            if(event.getUserId("").isEmpty()) {
                event.setUserId(ctx().getUserId());
            }
            if(!ctx().getUserId().equals(event.getUserId(null))) {
                throw new FmException("FE0002", "Invalid user");
            }
            if(event.getLocationId("").isEmpty() && event.getLocation(null) != null ) {
                DmLocation dmLoc = event.getLocation(null);
                dmLoc.setId(dmLoc.getID()); // we will use instanceid of object
                event.setLocationId(dmLoc.getId(""));
                dmLoc.setUserId(ctx().getUserId());
                dmLoc.setTimestamp(new Date());
                dmLoc.setGeindex(dmLoc.getGeindex(""));
                locList.add(dmLoc);
            }
            if(event.getMessage(null) == null ) {
                throw new FmException("FEM002", "No message");
            }
            DmMessage dmMsg = event.getMessage(null);
            dmMsg.setId(dmMsg.getID()); // we will use instanceid of object
            event.setMessageId(dmMsg.getId(""));
            dmMsg.setUserId(ctx().getUserId());
            dmMsg.setTimestamp(new Date());
            msgList.add(dmMsg);
        }
        
        dao(DaoLocations.class).put((DmLocation[]) locList.toArray(new DmLocation[locList.size()])); 
        dao(DaoMessages.class).put((DmMessage[]) msgList.toArray(new DmMessage[msgList.size()]));
        dao(DaoEvents.class).put(events);
        
        return getEvents(evIds.toArray(new String[evIds.size()]));
    }
   
    @Override
    public List<DmEvent> findNearestEvents(Double lat, Double lon, Double distance, String[] tagsArray, int nrows) {
        // and l.tile_index like '[:tile_range]%'
        DmLocation dm, dmLowerLeft, dmUpperRight;
        String dmLowerLeftTile, dmUpperRightTile;
        
        dm = DmFactory.create(DmLocation.class);
        dm.setLatitude(lat);
        dm.setLongitude(lon);
        
        // ll point
        dmLowerLeft = DmUtil.getPointOnDistance(dm, distance, 180D + 45D/2D);
        dmLowerLeftTile = GeoUtil.geoGenerateTileIndex(dmLowerLeft.getLongitude(0D), dmLowerLeft.getLatitude(0D), DmUtil.GEINDEX_LEVELS);
        
        // ll point
        dmUpperRight = DmUtil.getPointOnDistance(dm, distance, 45D/2D);
        dmUpperRightTile = GeoUtil.geoGenerateTileIndex(dmUpperRight.getLongitude(0D), dmUpperRight.getLatitude(0D), DmUtil.GEINDEX_LEVELS);

        // range
        String tileRange = GeoUtil.geoGenerateTileRange(dmLowerLeftTile, dmUpperRightTile, DmUtil.GEINDEX_LEVELS);

        // List<DmEvent> events = dao(DaoEvents.class).findNearestEvents(lat,lon,distance,nrows,tagsArray);        
        List<DmEvent> events = dao(DaoEvents.class).findNearestEventsByTileRange(lat,lon,distance, tileRange,nrows,tagsArray,ctx().getUserId());        
        
        List<String>ids = new ArrayList<String>();
        Map<String, Double> evDistances = new HashMap<String, Double>();
        for(DmEvent ev: events) {
            evDistances.put(ev.getId(""), ev.getDistance(null));
            ids.add(ev.getId(""));            
        }
        
        List<DmEvent> loadedEvents = getEvents(ids.toArray(new String[ids.size()]));
        Map<String,DmEvent> loadedEventsMap = new HashMap<String, DmEvent>();
        for(DmEvent ev: loadedEvents) {
            ev.setDistance(evDistances.get(ev.getId("")));
            loadedEventsMap.put(ev.getId(""), ev);
        }
        
        List<DmEvent> retEvents = new ArrayList<DmEvent>();
        for(DmEvent ev: events) {
            retEvents.add(loadedEventsMap.get(ev.getId("")));
        }
        return retEvents;
    }    

    @Override
    public void check() {
        List<DmLocation> locs = dao(DaoLocations.class).find(DmLocation.class, "SELECT * FROM LOCATIONS WHERE GINDEX IS NULL", null);
        for(DmLocation loc: locs) {
            //loc.setGindex(GeoUtil.geoGenerateTileIndex(loc.getXpos(0D), loc.getYpos(Double.NaN), GeoUtil.TILESIZE));
        }
        dao(DaoLocations.class).update(locs.toArray(new DmLocation[locs.size()]), new HashSet<String>() {{
            add("geindex");
        }});
    }
}
