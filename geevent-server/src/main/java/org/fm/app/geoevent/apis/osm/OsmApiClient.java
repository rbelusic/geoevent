package org.fm.app.geoevent.apis.osm;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.Column;
import org.fm.FmException;
import org.fm.app.geoevent.apis.osm.dm.OsmElement;
import org.fm.app.geoevent.apis.osm.dm.OsmRelation;
import org.fm.app.geoevent.apis.osm.dm.OsmUser;
import org.fm.app.geoevent.apis.osm.dm.OsmWay;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.dm.DmAttribute;
import org.fm.dm.DmFactory;
import org.fm.dm.DmObject;
import org.fm.http.client.FmHttpClient;
import org.fm.http.client.FmHttpParamList;
import org.fm.http.client.FmHttpResponse;
import org.fm.utils.Base64;


public class OsmApiClient {
    final static String propBase = OsmApiClient.class.getPackage().getName() + ".";
            
    final String apiVersion;
    
    private final String apiURLBase = "http://api.openstreetmap.org/api";
    private final String apiURL;
    private final FmHttpClient restClient;
    /*
    http://api.openstreetmap.org/api/0.6/capabilities/
    */
    
    public OsmApiClient(FmHttpClient cl, String ver) {
        apiVersion = ver == null ? "" : ver;
        apiURL =  apiURLBase + 
                (apiVersion.isEmpty() ? "" : "/" + apiVersion + "/");
                
        restClient = cl;
    }
    

    public String getApiVersion() {
        return apiVersion;
    }

    public String getApiURL() {
        return apiURL;
    }

    
    public FmHttpResponse invoke(FmHttpClient.REQUEST_TYPE method, String endpoint, Map<String, Object> params) {
        Map<String, String> headers = new HashMap<String, String>();
        addCommonHeaders(headers);
        
        try {
            return restClient.invoke(method, getApiURL() + endpoint,
                    "application/json; charset=utf-8",
                    new FmHttpParamList(params),
                    headers
            );
        } catch (Exception ex) {
            throw new FmException("HTTP error",ex);
        }
    }
    
    
    // -- API calls 
    public OsmUser  getUserProfile() {
        List<OsmUser> lst = responseToElementsArray(invoke(FmHttpClient.REQUEST_TYPE.GET, "user/details",
            null
        ));
        return lst.size() > 0 ? lst.get(0) : null;
    }
    
    public <T extends OsmElement> List<T> getElements(final Double left,final Double bottom,final Double right,final Double top) {
        return responseToElementsArray(invoke(FmHttpClient.REQUEST_TYPE.GET, "map",
            new HashMap() {{
                put("bbox",left.toString() + "," + 
                    bottom.toString() + "," + 
                    right.toString() + "," + 
                    top.toString()
                );
            }}
        ));
    }
    
    public <T extends OsmElement> T getElement(final String type,final Integer id) {
        
        List<T> lst = responseToElementsArray(invoke(FmHttpClient.REQUEST_TYPE.GET, type + "/" + id,
                null                
        ));
        
        return lst.size() > 0 ? lst.get(0) : null;
    }
    
    public <T extends OsmElement> List<T> getElements(final String type,final Integer ids[]) {
        
        List<T> lst = responseToElementsArray(invoke(FmHttpClient.REQUEST_TYPE.GET, type + "s?" + type + "s=" + arrayToString(ids),
                null                
        ));
        
        return lst;
    }
    
    public <T extends OsmRelation> List<T> getElementRelations(final String type,final Integer id) {
        
        List<T> lst = responseToElementsArray(invoke(FmHttpClient.REQUEST_TYPE.GET,  type + "/"+ id + "/relations",
                null                
        ));
        
        return lst;
    }
    
    public <T extends OsmWay> List<T> getNodeWays(final Integer id) {
        
        List<T> lst = responseToElementsArray(invoke(FmHttpClient.REQUEST_TYPE.GET,  "node/"+ id + "/ways",
                null                
        ));
        
        return lst;
    }
    
    public <T extends OsmWay> List<T> getRecursive(final String type,final Integer id) {
        
        List<T> lst = responseToElementsArray(invoke(FmHttpClient.REQUEST_TYPE.GET, type + "/" + id + "/full",
                null                
        ));
        
        return lst;
    }
        
    // util
    private Map<String, String> addCommonHeaders(Map<String, String> headers) {
        FmApplication app = FmApplication.instance();
        String userName = app == null ? null : app.getApplicationProperty(propBase + "user");
        String userPassword = app == null ? null : app.getApplicationProperty(propBase + "password");
        if(userName != null) {
            headers.put("Authorization", Base64.encodeBytes((userName + ":" + (userPassword == null ? "" : userPassword)).getBytes()));
        }
        return headers;
    }

    
    private <T extends OsmElement> List<T> responseToElementsArray(FmHttpResponse resp) {
        return null;
    }
    /*
    private <T extends OsmElement> T responseToElement(FmHttpResponse resp, String rootNodeName) {
        XmlDocument xmlDoc = XmlDocument.instance();
        xmlDoc.decodeXml(new String(resp.getResponse()), "osm");
        
        XmlNode rootNode = xmlDoc.getRootNode();
        
        
        return null;
    }
    */
    
    private String getAttrColumnName(DmAttribute attrdef, String method) {
        Column colAnn = attrdef.getMethodAnnotation(method + attrdef.getName(), Column.class);
        String name = colAnn != null && colAnn.name() != null && !colAnn.name().isEmpty() ? colAnn.name() : attrdef.getName();
        return name;
    }

    /*
    public  <T extends OsmElement> void stringToElement(String xmlStr, String rootNodeName, Class<T> clazz) {
        List<T> result = new ArrayList<T>();
        
        XmlDocument xmlDoc = XmlDocument.instance();
        xmlDoc.decodeXml(new String(xmlStr), "osm");
        
        XmlNode rootNode = xmlDoc.getRootNode();
        if(!rootNode.getNodeName().equals("osm")) {
            return;
        }
        
        for(XmlNode node: rootNode.getChildNodes(rootNodeName)) {
            T obj = DmFactory.create(clazz);
            for(String attr: obj.getAttributeNames()) {
                DmAttribute attrMeta = DmFactory.getDmAttributeMetadata(clazz, attr);
                String xmlPropName = getAttrColumnName(attrMeta, "get");
                Object xmlPropValue = node.get(xmlPropName);
                
                Class attrType = attrMeta.getType();
                Object attrObj;
                if(xmlPropValue == null) {
                    attrObj = null;
                } else if(DmObject.class.isAssignableFrom(attrType)) {
                    attrObj = DmFactory.create(attrType);    
                } else if(
                    String.class.isAssignableFrom(attrType) ||
                    Integer.class.isAssignableFrom(attrType) ||
                    Long.class.isAssignableFrom(attrType) ||
                    Double.class.isAssignableFrom(attrType) ||
                    Float.class.isAssignableFrom(attrType) ||
                    Number.class.isAssignableFrom(attrType)
                ) {
                    Constructor<?> ctor;
                    try {
                        ctor = attrType.getConstructor(String.class);
                        attrObj = ctor.newInstance(new Object[] { xmlPropValue });          
                    } catch (Exception ex) {
                        attrObj = null;
                    }            
                } else {
                    attrObj = null;
                }
                obj.setAttr(attr, attrObj);                                
            }
            result.add(obj);
        }
    }
    */
    private <T> String arrayToString(T [] lst) {
        String s = "";
        
        for(T e: lst) {
            s += (s.isEmpty() ? "" : ",") + e;
        }
        
        return s;
    }



    /*
    public static void main(String [ ] args) {
        OsmApiClient cli = new OsmApiClient("osm");
        cli.stringToElement(
        "<osm version=\"0.6\" generator=\"OpenStreetMap server\">\n" +
        "   <user id=\"12023\" display_name=\"jbpbis\" account_created=\"2007-08-16T01:35:56Z\">\n" +
        "     <description></description>\n" +
        "     <contributor-terms agreed=\"false\"/>\n" +
//        "     <img href=\"http://www.gravatar.com/avatar/c8c86cd15f60ecca66ce2b10cb6b9a00.jpg?s=256&d=http%3A%2F%2Fwww.openstreetmap.org%2Fassets%2Fusers%2Fimages%2Flarge-39c3a9dc4e778311af6b70ddcf447b58.png\"/>\n" +
        "     <roles>\n" +
        "     </roles>\n" +
        "     <changesets count=\"1\"/>\n" +
        "     <traces count=\"0\"/>\n" +
        "     <blocks>\n" +
        "       <received count=\"0\" active=\"0\"/>\n" +
        "     </blocks>\n" +
        "   </user>\n" +
        " </osm>", "user",OsmUser.class);
        

    }*/
}

