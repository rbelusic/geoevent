package org.fm.app.geoevent.client;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.fm.app.geoevent.dm.DmGpsLocation;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.app.geoevent.dm.event.DmLocation;
import org.fm.app.geoevent.util.GeoUtil;
import org.fm.dm.DmObject;
import org.fm.dm.DmUtil;

public class GeUtils {

    public static List<DmEvent> recalculateEventDistancesAndSort(
            DmGpsLocation location, List<DmEvent> evlist, Double importantDistance, Double importantDistanceDismiss
    ) {
        // recalculate distances
        List<DmEvent> newEvList = new ArrayList<DmEvent>();

        for (DmEvent ev : evlist) {
            DmLocation evLocation = ev.getLocation(null);
            double distance
                    = evLocation == null ? 0D
                            : GeoUtil.distanceHaversine(
                                    evLocation.getLatitude(0D), evLocation.getLongitude(0D), 0D,
                                    location.getLatitude(0D), location.getLongitude(0D), 0D
                            ) / 1000D;
            ev.setDistance(distance);
            if (distance < importantDistance) {
                ev.setInsAttr("important", Boolean.TRUE);
            } else if (distance > importantDistanceDismiss) {
                ev.setInsAttr("important", Boolean.FALSE);
            } else {
                if (ev.getInsAttr("important", null) == null) {
                    ev.setInsAttr("important", Boolean.FALSE);
                }
            }

            newEvList.add(ev);
        }

        // sort list
        sortEvents(newEvList);

        // return sorted list
        return (newEvList);
    }

    public static boolean checkImportance(
            DmGpsLocation location, DmEvent ev, Double importantDistance, Double importantDistanceDismiss
    ) {
        DmLocation evLocation = ev.getLocation(null);
        double distance
                = evLocation == null ? 0D
                        : GeoUtil.distanceHaversine(
                                evLocation.getLatitude(0D), evLocation.getLongitude(0D), 0D,
                                location.getLatitude(0D), location.getLongitude(0D), 0D
                        ) / 1000D;
        ev.setDistance(distance);
        if (distance < importantDistance) {
            ev.setInsAttr("important", Boolean.TRUE);
        } else if (distance > importantDistanceDismiss) {
            ev.setInsAttr("important", Boolean.FALSE);
        } else {
            if (ev.getInsAttr("important", null) == null) {
                ev.setInsAttr("important", Boolean.FALSE);
            }
        }

        return ev.getInsAttr("important", Boolean.FALSE);
    }

    public static <T extends DmObject> void sortEvents(List<DmEvent> list) {
        DmUtil.listSort(list, new Comparator<DmEvent>() {
            public int compare(DmEvent ev1, DmEvent ev2) {
                Double delta = ev1.getDistance(0D) - ev2.getDistance(0D);
                return delta > 0. ? 1 : (delta == 0. ? 0 : -1);
            }
        });
    }

    public static List<DmEvent> optimizeSortedEventsList(
            List<DmEvent> evlist, int prefferedMaxSize, int maxSizeIfImportants
    ) {
        List<DmEvent> newEvList = new ArrayList<DmEvent>();

        int pos = 0;
        for (DmEvent ev : evlist) {
            if (pos < prefferedMaxSize) {
                newEvList.add(ev);
            } else if (pos < maxSizeIfImportants) {
                if (ev.getInsAttr("important", Boolean.FALSE)) {
                    newEvList.add(ev);
                }
            } else {
                break;
            }
        }

        return newEvList;

    }

}
