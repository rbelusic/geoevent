package org.fm.app.geoevent.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fm.FmException;
import org.fm.app.geoevent.apis.osm.dm.NomPlace;
import org.fm.app.geoevent.application.GeSessionContext;
import org.fm.app.geoevent.dm.DmRole;
import org.fm.app.geoevent.dm.DmSession;
import org.fm.app.geoevent.dm.DmSysInfo;
import org.fm.app.geoevent.dm.DmUser;
import org.fm.app.geoevent.dm.event.DmEvent;
import org.fm.app.geoevent.util.rs.GeObjectMapper;
import org.fm.application.FmApplication;
import org.fm.application.FmContext;
import org.fm.application.FmSessionContext;
import org.fm.dm.DmObject;
import org.fm.http.client.FmHttpBodyParam;
import org.fm.http.client.FmHttpClient;
import org.fm.http.client.FmHttpFileParam;
import org.fm.http.client.FmHttpParam;
import org.fm.http.client.FmHttpParamList;
import org.fm.http.client.FmHttpResponse;
import org.fm.http.client.impl.FmApacheHttpClient;
import org.fm.utils.StringUtil;

public class GeRestClient {
    public final static String GE_CT = "application/json; charset=utf-8";
    public final static String GE_CT_TEXT = "text/html; charset=utf-8";
    public final static String GE_CT_BIN = "application/octet-stream";
    public final static String GE_CT_MULTIPART = "multipart/form-data";
    
    private final FmHttpClient restClient;

    public enum Endpoint {

        SYSTEM_LOGIN, SYSTEM_LOGOUT, SYSTEM_INFO,
        USERS_GET, USERS_GET_ROLES,
        EVENTS_GET, EVENTS_ADD, EVENTS_ADD_PHOTO, EVENTS_FIND,
        GEO_SEARCH, GEO_REVERSE
    }

    public static class EndpointConfig {

        public final String endPointUrl;
        public final String httpMethod;
        public final Class responseClass;
        public final boolean returnsCollection;
        public final int[] httpValidResponseCodes;
        public final String[] httpArgs;
        public final String contentType;

        public static EndpointConfig instance(String url, String method, String[] args, int[] validCodes, Class rspClass, boolean inList) {
            return new EndpointConfig(url, method, args, validCodes, rspClass, inList, GE_CT);
        }

        public static EndpointConfig instance(String url, String method, String[] args, int[] validCodes, Class rspClass, boolean inList, String ctype) {
            return new EndpointConfig(url, method, args, validCodes, rspClass, inList, ctype);
        }

        public boolean isMultipartRequest() {
            return(contentType.startsWith("multipart/form-data"));
        }
        
        private EndpointConfig(String url, String method, String[] args, int[] validCodes, Class rspClass, boolean inList, String ctype) {
            endPointUrl = url;
            httpMethod = method;
            httpArgs = args;
            httpValidResponseCodes = validCodes;
            responseClass = rspClass;
            returnsCollection = inList;
            contentType = ctype;
        }
    }


    public static final Map<Endpoint, EndpointConfig> ENDPOINTS = new HashMap<Endpoint, EndpointConfig>() {
        {
            put(Endpoint.SYSTEM_LOGIN, EndpointConfig.instance("/sys/login", "GET",
                    new String[]{"username", "password"}, new int[]{200}, DmSession.class, false));

            put(Endpoint.SYSTEM_LOGOUT, EndpointConfig.instance("/sys/logout", "GET", null, new int[]{200}, null, false));

            put(Endpoint.SYSTEM_INFO, EndpointConfig.instance("/sys/info", "GET", null, new int[]{200}, DmSysInfo.class, false));

            put(Endpoint.USERS_GET, EndpointConfig.instance("/users/get/[:ids]", "GET", new String[]{"ids"}, new int[]{200}, DmUser.class, true));

            put(Endpoint.USERS_GET_ROLES, EndpointConfig.instance("/users/get/roles", "GET", null, new int[]{200}, DmRole.class, true));

            put(Endpoint.EVENTS_GET, EndpointConfig.instance("/events/get/[:ids]",  "GET", new String[]{"ids"}, new int[]{200}, DmEvent.class, true));

            put(Endpoint.EVENTS_ADD, EndpointConfig.instance("/events/add",         "POST", new String[]{"$events"}, new int[]{201}, DmEvent.class, true, GE_CT));

            put(Endpoint.EVENTS_ADD_PHOTO, EndpointConfig.instance("/events/[:eventId]/photo", "POST", new String[]{"eventId", "@file"}, new int[]{201}, null, false, GE_CT_MULTIPART));

            put(Endpoint.EVENTS_FIND, EndpointConfig.instance("/events/find", "GET",
                    new String[]{"lat", "lon", "distance", "nrows", "tags"}, new int[]{200}, DmEvent.class, true));

            put(Endpoint.GEO_SEARCH, EndpointConfig.instance("/geo/search", "GET",
                    new String[]{
                        "query", "countries", "left", "top", "right", "bottom", "bounded", "address", "tags", "details", "polygon", "limit"}, new int[]{200}, NomPlace.class, true
            ));

            put(Endpoint.GEO_REVERSE, EndpointConfig.instance("/geo/reverse", "GET",
                    new String[]{"lat", "lon", "zoom", "address", "tags", "details", "osmtype", "osmid"}, new int[]{200}, NomPlace.class, false));
        }
    };

    private String serverUrl;

    public <T extends FmHttpClient> GeRestClient(T cl) {
        restClient = cl;
        setServerUrl(
                FmApplication.instance() == null
                        ? "http://localhost:8080"
                        : FmApplication.instance().getApplicationProperty("fm.app.geoevent.client.rest.url")
        );
    }

    public <T extends FmHttpClient> GeRestClient(T cl, String url) {
        restClient = cl;
        setServerUrl(url);
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String url) {
        this.serverUrl = url.endsWith("/") ? url.substring(0, url.length() - 1) : url;
    }

    public GeSessionContext ctx() {
        return (GeSessionContext) FmContext.getSessionBean(FmSessionContext.class);
    }

    public static EndpointConfig getEndpointConfig(Endpoint endpoint) {
        EndpointConfig e = ENDPOINTS.get(endpoint);
        if (e == null) {
            throw new FmException("Unknown REST endpoint");
        }
        return e;
    }

    private Map<String, String> getHeadersMap() {
        return new HashMap<String, String>() {
            {
                if (ctx() != null && ctx().getSessionInfo() != null) {
                    put("X-gevt-token", ctx().getSessionInfo().getId(""));
                }
            }
        };

    }

    // GET
    public <T extends DmObject, I> T get(Endpoint endpoint, Class<T> rcls, Map<String, Object> args) throws Exception {
        EndpointConfig e = getEndpointConfig(endpoint);
        FmHttpParamList paramList = new FmHttpParamList(args);

        String url = getServerUrl() + StringUtil.applyTemplate(e.endPointUrl, args);
        FmHttpResponse response = restClient.get(url, e.contentType, paramList, getHeadersMap());

        for (int code : e.httpValidResponseCodes) {
            if (response.getResponseCode() == code) {
                String data = response.getResponse() == null ? null : new String(response.getResponse());
                if (data == null || data.isEmpty()) {
                    return null;
                }
                GeObjectMapper m = new GeObjectMapper();
                m.setDefaultDeserializationClass(rcls);
                T dm = m.readValue(data, rcls);

                return dm;
            }
        }

        // inv resp code
        throw new FmException("Invalid http response code: " + response.getResponseCode());
    }

    public <T extends DmObject, I> List<T> listGet(Endpoint endpoint, Class<T> rcls, Map<String, Object> args) throws Exception {
        EndpointConfig e = getEndpointConfig(endpoint);
        FmHttpParamList paramList = new FmHttpParamList(args);

        String url = getServerUrl() + StringUtil.applyTemplate(e.endPointUrl, args);
        FmHttpResponse response = restClient.get(url, e.contentType, paramList, getHeadersMap());

        for (int code : e.httpValidResponseCodes) {
            if (response.getResponseCode() == code) {
                String data = response.getResponse() == null ? null : new String(response.getResponse());
                if (data == null || data.isEmpty()) {
                    return null;
                }
                GeObjectMapper m = new GeObjectMapper();
                m.setDefaultDeserializationClass(rcls);
                List<T> dmList = m.readValue(data, List.class);

                return dmList;
            }
        }

        // inv resp code
        throw new FmException("Invalid http response code: " + response.getResponseCode());
    }

    // POST
    private String _post(Endpoint endpoint, Map<String, Object> args) throws Exception {
        GeObjectMapper m = new GeObjectMapper();

        EndpointConfig e = getEndpointConfig(endpoint);
        FmHttpParamList paramList = new FmHttpParamList();
        
        for (Map.Entry<String, Object> arg : args.entrySet()) {
            if (arg.getKey().startsWith("@")) { // file  
                String name = arg.getKey().substring(1);
                Object value = arg.getValue();
                String filename = "data.bin";

                if(value instanceof String) { // file name
                    File file = new File(value.toString());
                    value = file;
                    filename = file.getName();
                }
                
                if(value instanceof byte[]) {                    
                    value = new ByteArrayInputStream((byte[]) value);                
                }

                // process
                if(value instanceof File) {
                    File file = (File) value;
                    if(file.exists()) {
                        paramList.add(
                            new FmHttpFileParam(
                                name, 
                                filename, 
                                URLConnection.guessContentTypeFromName(file.getName()), 
                                file
                            )
                        );                        
                    }
                } else if(value instanceof InputStream) {
                        InputStream is = (InputStream) value;               
                        paramList.add(
                            new FmHttpFileParam(
                                name, 
                                filename, 
                                URLConnection.guessContentTypeFromStream((InputStream) value), is)
                            );                
                }                    
            } else {                
                String name = arg.getKey().startsWith("$") ? arg.getKey().substring(1) : arg.getKey();
                Object value =  arg.getValue() == null ? "" : arg.getValue();                        
                Class valueClass;
                
                Class<?> clazz = value.getClass();
                if (clazz.isArray()) {
                    valueClass = clazz.getComponentType();
                } else {
                    valueClass = clazz;
                }
                String jsonValue = DmObject.class.isAssignableFrom(valueClass)? 
                    m.writeValueAsString(value) : value.toString();

                paramList.add(
                    arg.getKey().startsWith("$")? 
                        new FmHttpBodyParam(name,jsonValue,(DmObject.class.isAssignableFrom(valueClass) ? GE_CT : GE_CT_TEXT)) :
                        new FmHttpParam(name, jsonValue)
                );
            }
        }

        String url = getServerUrl() + StringUtil.applyTemplate(e.endPointUrl, args);        
        FmHttpResponse response = restClient.post(url, e.contentType, paramList, getHeadersMap());

        for (int code : e.httpValidResponseCodes) {
            if (response.getResponseCode() == code) {
                String data = response.getResponse() == null ? null : new String(response.getResponse());
                if (data == null || data.isEmpty()) {
                    return null;
                }
                return data;
            }
        }

        // inv resp code
        throw new FmException("Invalid http response code: " + response.getResponseCode());
    }
    
    public <T extends DmObject, I> T post(Endpoint endpoint, Class<T> rcls, Map<String, Object> args) throws Exception {
        GeObjectMapper m = new GeObjectMapper();
        m.setDefaultDeserializationClass(rcls);
        String data = _post(endpoint, args);
        return data == null ? null : m.readValue(data, rcls);        
    }

    public <T extends DmObject, I extends DmObject> List<T> listPost(Endpoint endpoint, Class<T> rcls, Map<String, Object> args) throws Exception {
        GeObjectMapper m = new GeObjectMapper();
        m.setDefaultDeserializationClass(rcls);
        String data = _post(endpoint, args);
        return data == null ? null : m.readValue(data, List.class);
    }
    

    /* ---------------------------------------------------------------------
     *
     * REST Impl 
     * 
     * 
     * 
     ----------------------------------------------------------------------*/
    /*
     * /sys/*
     */
    public DmSession sysLogin(final String username, final String password) throws Exception {
        DmSession dm = get(Endpoint.SYSTEM_LOGIN, DmSession.class, new HashMap() {
            {
                put("username", username);
                put("password", password);
            }
        });

        ctx().setSessionInfo(dm);
        return dm;
    }

    public void sysLogout() throws Exception {
        ctx().setSessionInfo(null);
        get(Endpoint.SYSTEM_LOGOUT, DmObject.class, new HashMap());
    }

    public DmSysInfo sysInfo() throws Exception {
        return get(Endpoint.SYSTEM_INFO, DmSysInfo.class, new HashMap());
    }

    /*
     * /users/*
     */
    public List<DmUser> usersGet(final List<String> ids) throws Exception {
        return listGet(Endpoint.USERS_GET, DmUser.class, new HashMap() {
            {
                put("ids", ids);
            }
        });
    }

    public List<DmRole> usersGetRoles() throws Exception {
        return listGet(Endpoint.USERS_GET_ROLES, DmRole.class, new HashMap());
    }

    /*
     * /events/*
     */
    public List<DmEvent> eventsGet(final List<String> ids) throws Exception {
        return listGet(Endpoint.EVENTS_GET, DmEvent.class, new HashMap() {
            {
                put("ids", ids);
            }
        });
    }

    public List<DmEvent> eventsFind(final Double lat, final Double lon, final Double distance, final List<String> tags, final Integer nrows) throws Exception {
        return listGet(Endpoint.EVENTS_FIND, DmEvent.class, new HashMap() {
            {
                put("lat", lat);
                put("lon", lon);
                put("distance", distance);
                put("tags", tags);
                put("nrows", nrows);
            }
        });
    }

    public void eventsAdd(final List<DmEvent> events) throws Exception {
        post(Endpoint.EVENTS_ADD, DmObject.class, new HashMap() {
            {
                put("$", events);
            }
        });
    }

    /*
     * /geo/*
     */
    public List<NomPlace> geoSearch(
            final String query,
            final String countries,
            final Double left,
            final Double top,
            final Double right,
            final Double bottom,
            final Boolean bounded,
            final Boolean addressDetails,
            final Boolean extraTags,
            final Boolean nameDetails,
            final Boolean polygon,
            final Integer limit
    ) throws Exception {
        return listGet(Endpoint.GEO_SEARCH, NomPlace.class, new HashMap() {
            {
                if (query != null) {
                    put("query", query);
                }
                if (countries != null) {
                    put("countries", countries);
                }
                if (left != null) {
                    put("left", left);
                }
                if (top != null) {
                    put("top", top);
                }
                if (right != null) {
                    put("right", right);
                }
                if (bottom != null) {
                    put("bottom", bottom);
                }
                if (bounded != null) {
                    put("bounded", bounded);
                }
                if (addressDetails != null) {
                    put("address", addressDetails);
                }
                if (extraTags != null) {
                    put("tags", extraTags);
                }
                if (nameDetails != null) {
                    put("details", nameDetails);
                }
                if (polygon != null) {
                    put("polygon", polygon);
                }
                if (limit != null) {
                    put("limit", limit);
                }
            }
        });

    }

    public NomPlace geoReverse(
            final Double lat,
            final Double lon,
            final Integer zoom,
            final Boolean addressDetails,
            final Boolean extraTags,
            final Boolean nameDetails,
            final String osmType,
            final Long osmId
    ) throws Exception {
        return get(Endpoint.GEO_REVERSE, NomPlace.class, new HashMap() {
            {
                if (lat != null) {
                    put("lat", lat);
                }
                if (lat != null) {
                    put("lon", lon);
                }
                if (zoom != null) {
                    put("zoom", zoom);
                }
                if (addressDetails != null) {
                    put("address", addressDetails);
                }
                if (extraTags != null) {
                    put("tags", extraTags);
                }
                if (nameDetails != null) {
                    put("details", nameDetails);
                }
                if (osmType != null) {
                    put("osmtype", osmType);
                }
                if (osmId != null) {
                    put("osmid", osmId);
                }
            }
        });
    }

    public static void main(String [] args) throws Exception {
        GeRestClient cli = new GeRestClient(new FmApacheHttpClient(),"http://localhost:8080/GeoEvent/api");
        
        DmSession ses = cli.get(GeRestClient.Endpoint.SYSTEM_LOGIN, DmSession.class, new HashMap<String, Object>() {{
            put("username", "admin");
            put("password", "amadeus");
        }});
        
        List<DmUser> usr = cli.listGet(Endpoint.USERS_GET, DmUser.class, new HashMap<String, Object>() {{
            put("ids","1");
        }});
        
        Object resp = cli.post(Endpoint.EVENTS_ADD_PHOTO, null, new HashMap<String, Object>(){{
            put("eventId", "c36dfe9d-1b7d-41d6-b2fe-1e87c6bf373d");
            put("@file","C:\\tmp\\logo.png");
        }});
    }
}
